# Generic App Ui V3 Changelog

# 0.2.2 (2023-01-20)

### Fixes

* Slide-Viewer workspace adjusts for expanding or collapsing the side menu
* ROI's don't reappear after deselection
* Cluster can't be selected by mouse click
* Class color indicator have fixed width and height

# 0.2.0 (2022-12-22)

### Refactor

* Menu overhaul, added analysis menu with sub menus for classes, jobs/roi's and primitives

### Feature

* Highlight annotations in viewer by clicking annotations or selecting roi's in menu
* Annotations in menu are selectable
* Load an show primitives
* Allow input types of rectangle, circle and/or polygon when app is compatible
* Show classes with there colors


# 0.1.0 (2022-11-25)

### Feature

* The Generic App Ui checks if the app is compatible with rectangle as an input, if not shows an error message and it deactivates the annotation draw tools

# 0.0.1 (2022-11-24)

### Refactor

* created Generic App Ui V3 based on Generic App Ui V2
