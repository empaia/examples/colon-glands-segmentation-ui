/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { AppUiStorage } from '../models/app-ui-storage';

@Injectable({
  providedIn: 'root',
})
export class StorageService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation scopeIdAppUiStorageUserGet
   */
  static readonly ScopeIdAppUiStorageUserGetPath = '/{scope_id}/app-ui-storage/user';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAppUiStorageUserGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAppUiStorageUserGet$Response(params: {
    scope_id: string;
  }): Observable<StrictHttpResponse<AppUiStorage>> {

    const rb = new RequestBuilder(this.rootUrl, StorageService.ScopeIdAppUiStorageUserGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppUiStorage>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAppUiStorageUserGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAppUiStorageUserGet(params: {
    scope_id: string;
  }): Observable<AppUiStorage> {

    return this.scopeIdAppUiStorageUserGet$Response(params).pipe(
      map((r: StrictHttpResponse<AppUiStorage>) => r.body as AppUiStorage)
    );
  }

  /**
   * Path part for operation scopeIdAppUiStorageUserPut
   */
  static readonly ScopeIdAppUiStorageUserPutPath = '/{scope_id}/app-ui-storage/user';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAppUiStorageUserPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAppUiStorageUserPut$Response(params: {
    scope_id: string;
    body: AppUiStorage
  }): Observable<StrictHttpResponse<AppUiStorage>> {

    const rb = new RequestBuilder(this.rootUrl, StorageService.ScopeIdAppUiStorageUserPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppUiStorage>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAppUiStorageUserPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAppUiStorageUserPut(params: {
    scope_id: string;
    body: AppUiStorage
  }): Observable<AppUiStorage> {

    return this.scopeIdAppUiStorageUserPut$Response(params).pipe(
      map((r: StrictHttpResponse<AppUiStorage>) => r.body as AppUiStorage)
    );
  }

  /**
   * Path part for operation scopeIdAppUiStorageScopeGet
   */
  static readonly ScopeIdAppUiStorageScopeGetPath = '/{scope_id}/app-ui-storage/scope';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAppUiStorageScopeGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAppUiStorageScopeGet$Response(params: {
    scope_id: string;
  }): Observable<StrictHttpResponse<AppUiStorage>> {

    const rb = new RequestBuilder(this.rootUrl, StorageService.ScopeIdAppUiStorageScopeGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppUiStorage>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAppUiStorageScopeGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdAppUiStorageScopeGet(params: {
    scope_id: string;
  }): Observable<AppUiStorage> {

    return this.scopeIdAppUiStorageScopeGet$Response(params).pipe(
      map((r: StrictHttpResponse<AppUiStorage>) => r.body as AppUiStorage)
    );
  }

  /**
   * Path part for operation scopeIdAppUiStorageScopePut
   */
  static readonly ScopeIdAppUiStorageScopePutPath = '/{scope_id}/app-ui-storage/scope';

  /**
   * .
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdAppUiStorageScopePut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAppUiStorageScopePut$Response(params: {
    scope_id: string;
    body: AppUiStorage
  }): Observable<StrictHttpResponse<AppUiStorage>> {

    const rb = new RequestBuilder(this.rootUrl, StorageService.ScopeIdAppUiStorageScopePutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<AppUiStorage>;
      })
    );
  }

  /**
   * .
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdAppUiStorageScopePut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdAppUiStorageScopePut(params: {
    scope_id: string;
    body: AppUiStorage
  }): Observable<AppUiStorage> {

    return this.scopeIdAppUiStorageScopePut$Response(params).pipe(
      map((r: StrictHttpResponse<AppUiStorage>) => r.body as AppUiStorage)
    );
  }

}
