/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { SlideInfo } from '../models/slide-info';
import { WorkbenchServiceApiV3CustomModelsSlidesSlideList } from '../models/workbench-service-api-v-3-custom-models-slides-slide-list';

@Injectable({
  providedIn: 'root',
})
export class SlidesService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation scopeIdSlidesGet
   */
  static readonly ScopeIdSlidesGetPath = '/{scope_id}/slides';

  /**
   * Get slides of current scope.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesGet$Response(params: {
    scope_id: string;
  }): Observable<StrictHttpResponse<WorkbenchServiceApiV3CustomModelsSlidesSlideList>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesService.ScopeIdSlidesGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<WorkbenchServiceApiV3CustomModelsSlidesSlideList>;
      })
    );
  }

  /**
   * Get slides of current scope.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesGet(params: {
    scope_id: string;
  }): Observable<WorkbenchServiceApiV3CustomModelsSlidesSlideList> {

    return this.scopeIdSlidesGet$Response(params).pipe(
      map((r: StrictHttpResponse<WorkbenchServiceApiV3CustomModelsSlidesSlideList>) => r.body as WorkbenchServiceApiV3CustomModelsSlidesSlideList)
    );
  }

  /**
   * Path part for operation scopeIdSlidesSlideIdInfoGet
   */
  static readonly ScopeIdSlidesSlideIdInfoGetPath = '/{scope_id}/slides/{slide_id}/info';

  /**
   * Get slide info.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdInfoGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdInfoGet$Response(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;
  }): Observable<StrictHttpResponse<SlideInfo>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesService.ScopeIdSlidesSlideIdInfoGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('slide_id', params.slide_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<SlideInfo>;
      })
    );
  }

  /**
   * Get slide info.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdInfoGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdInfoGet(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;
  }): Observable<SlideInfo> {

    return this.scopeIdSlidesSlideIdInfoGet$Response(params).pipe(
      map((r: StrictHttpResponse<SlideInfo>) => r.body as SlideInfo)
    );
  }

  /**
   * Path part for operation scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet
   */
  static readonly ScopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGetPath = '/{scope_id}/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}';

  /**
   * Get slide tile.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Response(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Zoom level
     */
    level: number;

    /**
     * Tile number horizontally
     */
    tile_x: number;

    /**
     * Tile number vertically
     */
    tile_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;

    /**
     * List of requested image channels. By default all channels are returned.
     */
    image_channels?: Array<number>;

    /**
     * Padding color as 24-bit hex-string
     */
    padding_color?: string;

    /**
     * Z coordinate / stack
     */
    'z'?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesService.ScopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('slide_id', params.slide_id, {});
      rb.path('level', params.level, {});
      rb.path('tile_x', params.tile_x, {});
      rb.path('tile_y', params.tile_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
      rb.query('image_channels', params.image_channels, {});
      rb.query('padding_color', params.padding_color, {});
      rb.query('z', params['z'], {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * Get slide tile.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Zoom level
     */
    level: number;

    /**
     * Tile number horizontally
     */
    tile_x: number;

    /**
     * Tile number vertically
     */
    tile_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;

    /**
     * List of requested image channels. By default all channels are returned.
     */
    image_channels?: Array<number>;

    /**
     * Padding color as 24-bit hex-string
     */
    padding_color?: string;

    /**
     * Z coordinate / stack
     */
    'z'?: number;
  }): Observable<Blob> {

    return this.scopeIdSlidesSlideIdTileLevelLevelTileTileXTileYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

  /**
   * Path part for operation scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet
   */
  static readonly ScopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGetPath = '/{scope_id}/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}';

  /**
   * Get slide region.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Response(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Zoom level
     */
    level: number;

    /**
     * Upper left x coordinate
     */
    start_x: number;

    /**
     * Upper left y coordinate
     */
    start_y: number;

    /**
     * Width
     */
    size_x: number;

    /**
     * Height
     */
    size_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;

    /**
     * List of requested image channels. By default all channels are returned.
     */
    image_channels?: Array<number>;

    /**
     * Z coordinate / stack
     */
    'z'?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesService.ScopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('slide_id', params.slide_id, {});
      rb.path('level', params.level, {});
      rb.path('start_x', params.start_x, {});
      rb.path('start_y', params.start_y, {});
      rb.path('size_x', params.size_x, {});
      rb.path('size_y', params.size_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
      rb.query('image_channels', params.image_channels, {});
      rb.query('z', params['z'], {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * Get slide region.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Zoom level
     */
    level: number;

    /**
     * Upper left x coordinate
     */
    start_x: number;

    /**
     * Upper left y coordinate
     */
    start_y: number;

    /**
     * Width
     */
    size_x: number;

    /**
     * Height
     */
    size_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;

    /**
     * List of requested image channels. By default all channels are returned.
     */
    image_channels?: Array<number>;

    /**
     * Z coordinate / stack
     */
    'z'?: number;
  }): Observable<Blob> {

    return this.scopeIdSlidesSlideIdRegionLevelLevelStartStartXStartYSizeSizeXSizeYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

  /**
   * Path part for operation scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet
   */
  static readonly ScopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGetPath = '/{scope_id}/slides/{slide_id}/macro/max_size/{max_x}/{max_y}';

  /**
   * Get slide macro image.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Response(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesService.ScopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('slide_id', params.slide_id, {});
      rb.path('max_x', params.max_x, {});
      rb.path('max_y', params.max_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * Get slide macro image.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<Blob> {

    return this.scopeIdSlidesSlideIdMacroMaxSizeMaxXMaxYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

  /**
   * Path part for operation scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet
   */
  static readonly ScopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGetPath = '/{scope_id}/slides/{slide_id}/label/max_size/{max_x}/{max_y}';

  /**
   * Get slide label image.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Response(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesService.ScopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('slide_id', params.slide_id, {});
      rb.path('max_x', params.max_x, {});
      rb.path('max_y', params.max_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * Get slide label image.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<Blob> {

    return this.scopeIdSlidesSlideIdLabelMaxSizeMaxXMaxYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

  /**
   * Path part for operation scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet
   */
  static readonly ScopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGetPath = '/{scope_id}/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}';

  /**
   * Get slide thumbnail.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<StrictHttpResponse<Blob>> {

    const rb = new RequestBuilder(this.rootUrl, SlidesService.ScopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('slide_id', params.slide_id, {});
      rb.path('max_x', params.max_x, {});
      rb.path('max_y', params.max_y, {});
      rb.query('image_format', params.image_format, {});
      rb.query('image_quality', params.image_quality, {});
    }

    return this.http.request(rb.build({
      responseType: 'blob',
      accept: 'image/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Blob>;
      })
    );
  }

  /**
   * Get slide thumbnail.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet(params: {
    scope_id: string;

    /**
     * Slide ID
     */
    slide_id: string;

    /**
     * Maximum width of image
     */
    max_x: number;

    /**
     * Maximum height of image
     */
    max_y: number;

    /**
     * Image format (e.g. bmp, gif, jpeg, png, tiff). For raw image data choose tiff.
     */
    image_format?: string;

    /**
     * Image quality (Only for specific formats. For Jpeg files compression is always lossy.         For tiff files &#x27;deflate&#x27; compression is used by default. Set to 0 to compress lossy with &#x27;jpeg&#x27;)
     */
    image_quality?: number;
  }): Observable<Blob> {

    return this.scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet$Response(params).pipe(
      map((r: StrictHttpResponse<Blob>) => r.body as Blob)
    );
  }

}
