/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { IdObject } from '../models/id-object';
import { Job } from '../models/job';
import { JobList } from '../models/job-list';
import { JobSlideList } from '../models/job-slide-list';
import { PostJob } from '../models/post-job';

@Injectable({
  providedIn: 'root',
})
export class JobsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation scopeIdJobsGet
   */
  static readonly ScopeIdJobsGetPath = '/{scope_id}/jobs';

  /**
   * Get jobs.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsGet$Response(params: {
    scope_id: string;
  }): Observable<StrictHttpResponse<JobList>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobList>;
      })
    );
  }

  /**
   * Get jobs.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsGet(params: {
    scope_id: string;
  }): Observable<JobList> {

    return this.scopeIdJobsGet$Response(params).pipe(
      map((r: StrictHttpResponse<JobList>) => r.body as JobList)
    );
  }

  /**
   * Path part for operation scopeIdJobsPost
   */
  static readonly ScopeIdJobsPostPath = '/{scope_id}/jobs';

  /**
   * Create job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsPost()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsPost$Response(params: {
    scope_id: string;
    body: PostJob
  }): Observable<StrictHttpResponse<Job>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsPostPath, 'post');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Job>;
      })
    );
  }

  /**
   * Create job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsPost$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsPost(params: {
    scope_id: string;
    body: PostJob
  }): Observable<Job> {

    return this.scopeIdJobsPost$Response(params).pipe(
      map((r: StrictHttpResponse<Job>) => r.body as Job)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdInputsInputKeyPut
   */
  static readonly ScopeIdJobsJobIdInputsInputKeyPutPath = '/{scope_id}/jobs/{job_id}/inputs/{input_key}';

  /**
   * Update input of job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdInputsInputKeyPut()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsJobIdInputsInputKeyPut$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;

    /**
     * Identifier for the input to update
     */
    input_key: string;
    body: IdObject
  }): Observable<StrictHttpResponse<Job>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsJobIdInputsInputKeyPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
      rb.path('input_key', params.input_key, {});
      rb.body(params.body, 'application/json');
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Job>;
      })
    );
  }

  /**
   * Update input of job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdInputsInputKeyPut$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  scopeIdJobsJobIdInputsInputKeyPut(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;

    /**
     * Identifier for the input to update
     */
    input_key: string;
    body: IdObject
  }): Observable<Job> {

    return this.scopeIdJobsJobIdInputsInputKeyPut$Response(params).pipe(
      map((r: StrictHttpResponse<Job>) => r.body as Job)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdInputsInputKeyDelete
   */
  static readonly ScopeIdJobsJobIdInputsInputKeyDeletePath = '/{scope_id}/jobs/{job_id}/inputs/{input_key}';

  /**
   * Remove input from job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdInputsInputKeyDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdInputsInputKeyDelete$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;

    /**
     * Identifier for the input to remove
     */
    input_key: string;
  }): Observable<StrictHttpResponse<Job>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsJobIdInputsInputKeyDeletePath, 'delete');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
      rb.path('input_key', params.input_key, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Job>;
      })
    );
  }

  /**
   * Remove input from job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdInputsInputKeyDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdInputsInputKeyDelete(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;

    /**
     * Identifier for the input to remove
     */
    input_key: string;
  }): Observable<Job> {

    return this.scopeIdJobsJobIdInputsInputKeyDelete$Response(params).pipe(
      map((r: StrictHttpResponse<Job>) => r.body as Job)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdGet
   */
  static readonly ScopeIdJobsJobIdGetPath = '/{scope_id}/jobs/{job_id}';

  /**
   * Get job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdGet$Response(params: {
    scope_id: string;

    /**
     * Id of job
     */
    job_id: string;
  }): Observable<StrictHttpResponse<Job>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsJobIdGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Job>;
      })
    );
  }

  /**
   * Get job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdGet(params: {
    scope_id: string;

    /**
     * Id of job
     */
    job_id: string;
  }): Observable<Job> {

    return this.scopeIdJobsJobIdGet$Response(params).pipe(
      map((r: StrictHttpResponse<Job>) => r.body as Job)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdDelete
   */
  static readonly ScopeIdJobsJobIdDeletePath = '/{scope_id}/jobs/{job_id}';

  /**
   * Remove job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdDelete$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;
  }): Observable<StrictHttpResponse<Job>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsJobIdDeletePath, 'delete');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Job>;
      })
    );
  }

  /**
   * Remove job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdDelete(params: {
    scope_id: string;

    /**
     * Id of job that should be updated
     */
    job_id: string;
  }): Observable<Job> {

    return this.scopeIdJobsJobIdDelete$Response(params).pipe(
      map((r: StrictHttpResponse<Job>) => r.body as Job)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdRunPut
   */
  static readonly ScopeIdJobsJobIdRunPutPath = '/{scope_id}/jobs/{job_id}/run';

  /**
   * Run job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdRunPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdRunPut$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<StrictHttpResponse<Job>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsJobIdRunPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Job>;
      })
    );
  }

  /**
   * Run job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdRunPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdRunPut(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<Job> {

    return this.scopeIdJobsJobIdRunPut$Response(params).pipe(
      map((r: StrictHttpResponse<Job>) => r.body as Job)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdStopPut
   */
  static readonly ScopeIdJobsJobIdStopPutPath = '/{scope_id}/jobs/{job_id}/stop';

  /**
   * Stop job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdStopPut()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdStopPut$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<StrictHttpResponse<boolean>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsJobIdStopPutPath, 'put');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: String((r as HttpResponse<any>).body) === 'true' }) as StrictHttpResponse<boolean>;
      })
    );
  }

  /**
   * Stop job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdStopPut$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdStopPut(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<boolean> {

    return this.scopeIdJobsJobIdStopPut$Response(params).pipe(
      map((r: StrictHttpResponse<boolean>) => r.body as boolean)
    );
  }

  /**
   * Path part for operation scopeIdJobsJobIdSlidesGet
   */
  static readonly ScopeIdJobsJobIdSlidesGetPath = '/{scope_id}/jobs/{job_id}/slides';

  /**
   * Get slides used in a given job.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `scopeIdJobsJobIdSlidesGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdSlidesGet$Response(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<StrictHttpResponse<JobSlideList>> {

    const rb = new RequestBuilder(this.rootUrl, JobsService.ScopeIdJobsJobIdSlidesGetPath, 'get');
    if (params) {
      rb.path('scope_id', params.scope_id, {});
      rb.path('job_id', params.job_id, {});
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<JobSlideList>;
      })
    );
  }

  /**
   * Get slides used in a given job.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `scopeIdJobsJobIdSlidesGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  scopeIdJobsJobIdSlidesGet(params: {
    scope_id: string;

    /**
     * Id of job that should be executed
     */
    job_id: string;
  }): Observable<JobSlideList> {

    return this.scopeIdJobsJobIdSlidesGet$Response(params).pipe(
      map((r: StrictHttpResponse<JobSlideList>) => r.body as JobSlideList)
    );
  }

}
