/* tslint:disable */
/* eslint-disable */

/**
 * An enumeration.
 */
export enum ExaminationState {
  Open = 'OPEN',
  Closed = 'CLOSED'
}
