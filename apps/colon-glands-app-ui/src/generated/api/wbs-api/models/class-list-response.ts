/* tslint:disable */
/* eslint-disable */
import { Class } from './class';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface ClassListResponse {

  /**
   * Count of all items
   */
  item_count: number;

  /**
   * List of items
   */
  items: Array<Class>;
}
