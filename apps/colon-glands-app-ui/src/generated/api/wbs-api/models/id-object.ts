/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface IdObject {

  /**
   * ID (type string) of a single element
   */
  id: string;
}
