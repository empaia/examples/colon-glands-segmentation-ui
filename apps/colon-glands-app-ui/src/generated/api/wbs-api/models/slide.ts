/* tslint:disable */
/* eslint-disable */
import { TagMapping } from './tag-mapping';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface Slide {

  /**
   * Block
   */
  block?: string;

  /**
   * Case ID
   */
  case_id: string;

  /**
   * Timestamp (milliseconds) when the slide was created
   */
  created_at: number;

  /**
   * Flag indicating whether the underlying slide files and mappings have been deleted
   */
  deleted?: boolean;

  /**
   * ID
   */
  id: string;

  /**
   * Local ID provided by AP-LIS
   */
  local_id?: string;

  /**
   * Base URL of Medical Data Service instance that generated empaia_id
   */
  mds_url?: string;

  /**
   * Stain Mapping
   */
  stain?: TagMapping;

  /**
   * Tissue Mapping
   */
  tissue?: TagMapping;

  /**
   * Slide
   */
  type?: 'slide';

  /**
   * Timestamp (milliseconds) when the slide was last updated
   */
  updated_at: number;
}
