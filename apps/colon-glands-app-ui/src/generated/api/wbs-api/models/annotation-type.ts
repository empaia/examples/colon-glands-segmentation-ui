/* tslint:disable */
/* eslint-disable */

/**
 * An enumeration.
 */
export enum AnnotationType {
  Point = 'point',
  Line = 'line',
  Arrow = 'arrow',
  Circle = 'circle',
  Rectangle = 'rectangle',
  Polygon = 'polygon'
}
