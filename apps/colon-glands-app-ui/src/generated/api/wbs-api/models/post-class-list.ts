/* tslint:disable */
/* eslint-disable */
import { PostClass } from './post-class';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface PostClassList {

  /**
   * List of classes
   */
  items: Array<PostClass>;
}
