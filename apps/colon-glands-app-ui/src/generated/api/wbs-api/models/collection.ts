/* tslint:disable */
/* eslint-disable */
import { ArrowAnnotation } from './arrow-annotation';
import { BoolPrimitive } from './bool-primitive';
import { CircleAnnotation } from './circle-annotation';
import { Class } from './class';
import { CollectionItemType } from './collection-item-type';
import { CollectionReferenceType } from './collection-reference-type';
import { DataCreatorType } from './data-creator-type';
import { FloatPrimitive } from './float-primitive';
import { IdObject } from './id-object';
import { IntegerPrimitive } from './integer-primitive';
import { LineAnnotation } from './line-annotation';
import { PointAnnotation } from './point-annotation';
import { PolygonAnnotation } from './polygon-annotation';
import { RectangleAnnotation } from './rectangle-annotation';
import { SlideItem } from './slide-item';
import { StringPrimitive } from './string-primitive';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface Collection {

  /**
   * Creator Id
   */
  creator_id: string;

  /**
   * Creator type
   */
  creator_type: DataCreatorType;

  /**
   * Collection description
   */
  description?: string;

  /**
   * ID of type UUID4 (only needed in post if external Ids enabled)
   */
  id?: string;

  /**
   * Flag to mark a collection as immutable
   */
  is_locked?: boolean;

  /**
   * The number of items in the collection
   */
  item_count?: number;

  /**
   * Ids of items in collection
   */
  item_ids?: Array<string>;

  /**
   * The type of items in the collection
   */
  item_type: CollectionItemType;

  /**
   * Items of the collection
   */
  items?: (Array<PointAnnotation> | Array<LineAnnotation> | Array<ArrowAnnotation> | Array<CircleAnnotation> | Array<RectangleAnnotation> | Array<PolygonAnnotation> | Array<Class> | Array<IntegerPrimitive> | Array<FloatPrimitive> | Array<BoolPrimitive> | Array<StringPrimitive> | Array<SlideItem> | Array<IdObject> | Array<Collection>);

  /**
   * Collection name
   */
  name?: string;

  /**
   * Id of the object referenced by this collection
   */
  reference_id?: string;

  /**
   * Refrence type
   */
  reference_type?: CollectionReferenceType;

  /**
   * Collection type
   */
  type: 'collection';
}
