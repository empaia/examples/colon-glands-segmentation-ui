/* tslint:disable */
/* eslint-disable */
import { AnnotationType } from './annotation-type';
import { Viewport } from './viewport';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface AnnotationViewerQuery {

  /**
   * List of class values. IMPORTANT NOTE: Can be null, if annotations without assigned classes should be included!
   */
  class_values?: Array<string | null>;

  /**
   * List of creator Ids
   */
  creators?: Array<string>;

  /**
   * List of job Ids the annotations must be locked for. IMPORTANT NOTE: Can be a list with null as single value, if annotations not locked in any job should be returned!
   */
  jobs?: (Array<string> | Array<null>);

  /**
   * Resolution range in npp (nanometer per pixel) to filter annotations
   */
  npp_viewing?: Array<number>;

  /**
   * List of reference Ids
   */
  references?: Array<string>;

  /**
   * List of annotation types
   */
  types?: Array<AnnotationType>;

  /**
   * The viewport (only annotations within are returned)
   */
  viewport?: Viewport;
}
