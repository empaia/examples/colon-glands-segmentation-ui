/* tslint:disable */
/* eslint-disable */

/**
 * An enumeration.
 */
export enum PrimitiveReferenceType {
  Annotation = 'annotation',
  Collection = 'collection',
  Wsi = 'wsi'
}
