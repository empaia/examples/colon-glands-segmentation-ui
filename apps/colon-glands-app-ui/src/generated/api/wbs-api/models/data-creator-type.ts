/* tslint:disable */
/* eslint-disable */

/**
 * An enumeration.
 */
export enum DataCreatorType {
  Job = 'job',
  User = 'user',
  Scope = 'scope'
}
