/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface Message {

  /**
   * Message used for untyped responses
   */
  message: string;
}
