/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface AppUiStorage {

  /**
   * Dictionary of key-value-pairs
   */
  content: {
[key: string]: (string | number | number | boolean);
};
}
