/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface Viewport {

  /**
   * Height of viewport (must be > 0)
   */
  height: number;

  /**
   * Width of viewport (must be > 0)
   */
  width: number;

  /**
   * X coordinate of upper left corner of viewport (must be >= 0)
   */
  'x': number;

  /**
   * Y coordinate of upper left corner of viewport (must be >= 0)
   */
  'y': number;
}
