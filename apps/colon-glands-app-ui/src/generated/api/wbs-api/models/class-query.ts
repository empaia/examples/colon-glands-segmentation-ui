/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface ClassQuery {

  /**
   * List of Class Ids (must be of type UUID4)
   */
  classes?: Array<any>;

  /**
   * List of creator Ids
   */
  creators?: Array<string>;

  /**
   * List of job Ids
   */
  jobs?: Array<string>;

  /**
   * List of annotation Ids (UUID type 4)
   */
  references?: Array<string>;
}
