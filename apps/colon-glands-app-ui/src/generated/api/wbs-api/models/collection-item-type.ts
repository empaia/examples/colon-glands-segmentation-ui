/* tslint:disable */
/* eslint-disable */

/**
 * An enumeration.
 */
export enum CollectionItemType {
  Wsi = 'wsi',
  Integer = 'integer',
  Float = 'float',
  Bool = 'bool',
  String = 'string',
  Point = 'point',
  Line = 'line',
  Arrow = 'arrow',
  Circle = 'circle',
  Rectangle = 'rectangle',
  Polygon = 'polygon',
  Class = 'class',
  Collection = 'collection'
}
