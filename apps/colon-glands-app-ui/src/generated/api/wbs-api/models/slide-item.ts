/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface SlideItem {

  /**
   * WSI ID
   */
  id: string;

  /**
   * WSI type
   */
  type: 'wsi';
}
