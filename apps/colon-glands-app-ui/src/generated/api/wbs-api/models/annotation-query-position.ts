/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface AnnotationQueryPosition {

  /**
   * ID of type UUID4
   */
  id: string;

  /**
   * Position of annotation in result of query (starts with 0)
   */
  position: number;
}
