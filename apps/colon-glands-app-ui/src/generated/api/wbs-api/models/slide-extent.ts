/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface SlideExtent {

  /**
   * Extent in horizontal direction
   */
  'x': number;

  /**
   * Extent in vertical direction
   */
  'y': number;

  /**
   * Number of Z-Stack layers
   */
  'z': number;
}
