/* tslint:disable */
/* eslint-disable */

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface UniqueClassValues {

  /**
   * List of unique class values for classes assigned to annotations matching given filter criteria. IMPORTANT NOTE: Can be null, if annotations without assigned classes are returned!
   */
  unique_class_values?: Array<string>;
}
