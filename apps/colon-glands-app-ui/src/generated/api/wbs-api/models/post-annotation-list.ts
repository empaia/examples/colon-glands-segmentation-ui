/* tslint:disable */
/* eslint-disable */
import { PostArrowAnnotation } from './post-arrow-annotation';
import { PostCircleAnnotation } from './post-circle-annotation';
import { PostLineAnnotation } from './post-line-annotation';
import { PostPointAnnotation } from './post-point-annotation';
import { PostPolygonAnnotation } from './post-polygon-annotation';
import { PostRectangleAnnotation } from './post-rectangle-annotation';

/**
 * Abstract Super-class not allowing unknown fields in the **kwargs.
 */
export interface PostAnnotationList {

  /**
   * List of annotations (of same type, e.g. point annotations)
   */
  items: (Array<PostPointAnnotation> | Array<PostLineAnnotation> | Array<PostArrowAnnotation> | Array<PostCircleAnnotation> | Array<PostRectangleAnnotation> | Array<PostPolygonAnnotation>);
}
