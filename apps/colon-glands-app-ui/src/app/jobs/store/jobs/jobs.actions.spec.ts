import * as fromJobs from './jobs.actions';

describe('JobsActions', () => {
  it('should return an action', () => {
    expect(fromJobs.clearJobs().type).toBe('[APP/Jobs] Clear Jobs');
  });
});
