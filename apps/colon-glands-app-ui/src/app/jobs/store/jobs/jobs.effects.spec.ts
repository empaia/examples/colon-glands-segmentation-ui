import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';

import { JobsEffects } from './jobs.effects';

describe('JobsEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<JobsEffects>;
  const createService = createServiceFactory({
    service: JobsEffects,
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      JobsEffects,
      provideMockActions(() => actions$),
      provideMockStore(),
      { provide: 'EXAMINATION_ID', useValue: '' },
      { provide: 'APP_ID', useValue: '' },
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
