import { AnnotationEntity } from 'slide-viewer';
import { Job } from '@api/wbs-api/models/job';
import { JobStatus } from '@api/wbs-api/models/job-status';

export interface JobSelector {
  id: string;
  checked: boolean;
}

export interface JobRoiEntity {
  job: Job;
  roi: AnnotationEntity;
}

export const JOB_POLLING_PERIOD = 5000;

export function isJobRunning(job: Job): boolean {
  return job.status === JobStatus.Assembly
    || job.status === JobStatus.Ready
    || job.status === JobStatus.Scheduled
    || job.status === JobStatus.Running;
}
