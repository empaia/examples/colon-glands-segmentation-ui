import { InputKey, EmpaiaAppDescriptionV3, EadInputTypeAnnotation } from '@core/models/ead.models';
import { EmpaiaAppDescriptionV1 } from '@core/models/ead.models';
import { EadService } from '@core/services/ead.service';
import { Injectable } from '@angular/core';
import { JobsService, DataService } from '@api/wbs-api/services';
import { EXAMINATION_CLOSED_ERROR_STATUS_CODE } from '@menu/models/ui.models';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@nrwl/angular';
import { dispatchActionOnErrorCode, filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { timer } from 'rxjs';
import { catchError, filter, map, retryWhen, switchMap, takeUntil } from 'rxjs/operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { CollectionItemType, DataCreatorType, JobCreatorType, JobMode } from '@api/wbs-api/models';
import { isJobRunning, JobsFeature, JOB_POLLING_PERIOD } from '..';
import * as JobsActions from './jobs.actions';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';



@Injectable()
export class JobsEffects {

  // clear jobs on slide selection
  clearJobsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => JobsActions.clearJobs())
    );
  });

  // create job when annotation was created
  prepareCreateJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationSuccess),
      map(action => action.annotation),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
      ]),
      map(([annotation, scopeId]) => JobsActions.createJob({
        scopeId,
        creatorId: scopeId,
        creatorType: JobCreatorType.Scope,
        annotation,
      }))
    );
  });

  loadJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobs),
      fetch({
        id: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: JobsFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<typeof JobsActions.loadJobs>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService.scopeIdJobsGet({
            scope_id: action.scopeId
          }).pipe(
            // retry to load the selected slide after the token is expired
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(response => {
              const wsiInputKey = this.eadService.getV3WsiInputKey(action.ead, 'standalone') as string;
              return response.items.filter(job => job.inputs[wsiInputKey] === action.slideId && job.mode === JobMode.Standalone);
            }),
            map(jobs => JobsActions.loadJobsSuccess({ jobs }))
          );
        },
        onError: (_action: ReturnType<typeof JobsActions.loadJobs>, error) => {
          return JobsActions.loadJobsFailure({ error });
        }
      })
    );
  });

  jobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.startJobsPolling),
      switchMap(() =>
        timer(0, JOB_POLLING_PERIOD).pipe(
          takeUntil(this.actions$.pipe(ofType(JobsActions.stopJobsPolling))),
          concatLatestFrom(() => [
            this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
            this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
            this.store.select(ScopeSelectors.selectExtendedScope).pipe(
              filterNullish(),
              map(extended => extended.ead as EmpaiaAppDescriptionV3)
            )
          ]),
          map(([, scopeId, slideId, ead]) => JobsActions.loadJobs({ scopeId, slideId, ead }))
        )
      )
    );
  });

  stopJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map(action => action.jobs),
      filter(jobs => !jobs.find(isJobRunning)),
      map(() => JobsActions.stopJobsPolling())
    );
  });

  startJobsPolling$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setWsiJobInput,
        SlidesActions.selectSlide,
      ),
      map(() => JobsActions.startJobsPolling())
    );
  });

  createJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.createJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.createJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService.scopeIdJobsPost({
            scope_id: action.scopeId,
            body: {
              creator_id: action.creatorId,
              creator_type: action.creatorType,
            }
          }).pipe(
            // retry to load the selected slide after the token is expired
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(job => job.id),
            map(jobId => JobsActions.setWsiJobInput({
              scopeId: action.scopeId,
              jobId,
              annotation: action.annotation,
            })),
            catchError(error =>
              dispatchActionOnErrorCode(
                error,
                EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
              )
            )
          );
        },
        onError: (_action: ReturnType<typeof JobsActions.createJob>, error) => {
          return JobsActions.createJobFailure({ error });
        }
      })
    );
  });

  prepareWsiJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiJobInput),
      concatLatestFrom(() => [
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(ScopeSelectors.selectExtendedScope).pipe(filterNullish())
      ]),
      map(([action, slideId, extended]) => JobsActions.setWsiJobInputReady({
        ...action,
        annotationId: action.annotation.id,
        slideId,
        slideInputKey: this.eadService.isV1Ead(extended.ead)
          ? this.eadService.getV1WsiInputKey(extended.ead as EmpaiaAppDescriptionV1) as string
          : this.eadService.getV3TypeInputKey('wsi', extended.ead as EmpaiaAppDescriptionV3, 'standalone')?.inputKey as string,
        annotationInputKey: this.eadService.isV1Ead(extended.ead)
          ? this.eadService.getV1AnnotationsInputKey(extended.ead as EmpaiaAppDescriptionV1) as InputKey
          : this.eadService.getV3TypeInputKey(
            action.annotation.annotationType as EadInputTypeAnnotation,
            extended.ead as EmpaiaAppDescriptionV3,
            'standalone'
          ) as InputKey
      }))
    );
  });

  setWsiJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setWsiJobInputReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setWsiJobInputReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService.scopeIdJobsJobIdInputsInputKeyPut({
            scope_id: action.scopeId,
            job_id: action.jobId,
            input_key: action.slideInputKey,
            body: {
              id: action.slideId
            }
          }).pipe(
            // retry to load the selected slide after the token is expired
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            // map(() => JobsActions.setAnnotationJobInput({ ...action })),
            map(() =>
              action.annotationInputKey.inCollection > 0
                ? JobsActions.setCollectionToAnnotation({
                  ...action,
                  inputId: action.annotationId,
                })
                : JobsActions.setAnnotationJobInput({
                  ...action,
                  inputId: action.annotationId,
                })
            ),
            catchError(error =>
              dispatchActionOnErrorCode(
                error,
                EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
              )
            )
          );
        },
        onError: (action: ReturnType<typeof JobsActions.setWsiJobInputReady>, error) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action, inputId: action.annotationId });
          // return JobsActions.createJobFailure({ error });
        }
      })
    );
  });

  prepareCollectionToAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setCollectionToAnnotation),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setCollectionToAnnotation>,
          _state: JobsFeature.State
        ) => {
          return this.dataService.scopeIdCollectionsPost({
            scope_id: action.scopeId,
            body: {
              item_type: action.collectionType ?? CollectionItemType.Rectangle,
              items: [
                { id: action.inputId }
              ],
              type: 'collection',
              creator_type: DataCreatorType.Scope,
              creator_id: action.scopeId
            }
          }).pipe(
            map(collection => {
              const remain = action.annotationInputKey.inCollection - 1;
              return remain > 0
                ? JobsActions.setCollectionToAnnotation({
                  ...action,
                  inputId: collection?.id as string,
                  collectionType: CollectionItemType.Collection,
                  annotationInputKey: { ...action.annotationInputKey, inCollection: remain }
                })
                : JobsActions.setAnnotationJobInput({
                  ...action,
                  inputId: collection?.id as string
                });
            })
          );
        },
        onError: (action: ReturnType<typeof JobsActions.setCollectionToAnnotation>, error) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
        }
      })
    );
  });

  setAnnotationJobInput$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setAnnotationJobInput),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.setAnnotationJobInput>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService.scopeIdJobsJobIdInputsInputKeyPut({
            scope_id: action.scopeId,
            job_id: action.jobId,
            input_key: action.annotationInputKey.inputKey,
            body: {
              id: action.inputId
            }
          }).pipe(
            // retry to load the selected slide after the token is expired
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(() => JobsActions.runJob({ ...action })),
            catchError(error =>
              dispatchActionOnErrorCode(
                error,
                EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
              )
            )
          );
        },
        onError: (action: ReturnType<typeof JobsActions.setAnnotationJobInput>, error) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
          // return JobsActions.createJobFailure({ error });
        }
      })
    );
  });

  runJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.runJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.runJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService.scopeIdJobsJobIdRunPut({
            scope_id: action.scopeId,
            job_id: action.jobId
          }).pipe(
            // retry to load the selected slide after the token is expired
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(job => JobsActions.createJobSuccess({ job })),
            catchError(error =>
              dispatchActionOnErrorCode(
                error,
                EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
              )
            )
          );
        },
        onError: (action: ReturnType<typeof JobsActions.runJob>, error) => {
          // delete the current job if something goes wrong while setting the inputs
          return JobsActions.setJobInputFailure({ error, ...action });
        }
      })
    );
  });

  deleteJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.deleteJob),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.deleteJob>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService.scopeIdJobsJobIdDelete({
            scope_id: action.scopeId,
            job_id: action.jobId,
          }).pipe(
            // retry to load the selected slide after the token is expired
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(job => job.id),
            map(jobId => JobsActions.deleteJobSuccess({ jobId }))
          );
        },
        onError: (_action: ReturnType<typeof JobsActions.deleteJob>, error) => {
          return JobsActions.deleteJobFailure({ error });
        }
      })
    );
  });

  prepareStopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJob),
      map(action => action.jobId),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([jobId, scopeId]) => JobsActions.stopRunningJobReady({
        scopeId,
        jobId
      }))
    );
  });

  stopRunningJob$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.stopRunningJobReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof JobsActions.stopRunningJobReady>,
          _state: JobsFeature.State
        ) => {
          return this.jobsService.scopeIdJobsJobIdStopPut({
            scope_id: action.scopeId,
            job_id: action.jobId
          }).pipe(
            // retry to load the selected slide after the token is expired
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(done => JobsActions.stopRunningJobSuccess({ done }))
          );
        },
        onError: (_action: ReturnType<typeof JobsActions.stopRunningJobReady>, error) => {
          JobsActions.stopRunningJobFailure({ error });
          return null;
        }
      })
    );
  });

  // stop loading state on extended scope reload
  stopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => JobsActions.stopLoading())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly jobsService: JobsService,
    private readonly dataService: DataService,
    private readonly eadService: EadService,
  ) {}
}
