import { AnnotationEntity } from 'slide-viewer';
import { InputKey, EmpaiaAppDescriptionV3 } from '@core/models/ead.models';
import { HttpErrorResponse } from '@angular/common/http';
import { Job } from '@api/wbs-api/models/job';
import { JobCreatorType } from '@api/wbs-api/models/job-creator-type';
import { createAction, props } from '@ngrx/store';
import { JobSelector } from './jobs.models';
import { CollectionItemType } from '@api/wbs-api/models';

export const loadJobs = createAction(
  '[APP/Jobs] Load Jobs',
  props<{
    scopeId: string,
    slideId: string,
    ead: EmpaiaAppDescriptionV3
  }>()
);

export const loadJobsSuccess = createAction(
  '[APP/Jobs] Load Jobs Success',
  props<{ jobs: Job[] }>()
);

export const loadJobsFailure = createAction(
  '[APP/Jobs] Load Jobs Failure',
  props<{ error: HttpErrorResponse }>()
);

export const startJobsPolling = createAction(
  '[APP/Jobs] Start Jobs Polling'
);

export const stopJobsPolling = createAction(
  '[APP/Jobs] Stop Jobs Polling'
);

export const createJob = createAction(
  '[APP/Jobs] Create Job',
  props<{
    scopeId: string,
    creatorId: string,
    creatorType: JobCreatorType,
    annotation: AnnotationEntity,
  }>()
);

export const setWsiJobInput = createAction(
  '[APP/Jobs] Set Wsi Job Input',
  props<{
    scopeId: string,
    jobId: string,
    annotation: AnnotationEntity,
  }>()
);

export const setWsiJobInputReady = createAction(
  '[APP/Jobs] Set Wsi Job Input Ready',
  props<{
    scopeId: string,
    jobId: string,
    slideInputKey: string,
    slideId: string,
    annotationInputKey: InputKey,
    annotationId: string,
  }>()
);

export const setCollectionToAnnotation = createAction(
  '[APP/Jobs] Set Collection To Annotation',
  props<{
    scopeId: string,
    jobId: string,
    annotationInputKey: InputKey,
    inputId: string,
    collectionType?: CollectionItemType,
  }>()
);

export const setAnnotationJobInput = createAction(
  '[APP/Jobs] Set Annotation Job Input',
  props<{
    scopeId: string,
    jobId: string,
    annotationInputKey: InputKey,
    inputId: string,
  }>()
);

export const runJob = createAction(
  '[App/Jobs] Run Job',
  props<{
    scopeId: string,
    jobId: string,
    inputId: string
  }>()
);

export const createJobSuccess = createAction(
  '[APP/Jobs] Create Job Success',
  props<{ job: Job }>()
);

export const createJobFailure = createAction(
  '[APP/Jobs] Create Job Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setJobInputFailure = createAction(
  '[APP/Jobs] Set Job Input Failure',
  props<{
    error: HttpErrorResponse,
    scopeId: string,
    jobId: string,
    inputId: string,
  }>()
);

export const deleteJob = createAction(
  '[APP/Jobs] Delete Job',
  props<{ scopeId: string, jobId: string }>()
);

export const deleteJobSuccess = createAction(
  '[APP/Jobs] Delete Job Success',
  props<{ jobId: string }>()
);

export const deleteJobFailure = createAction(
  '[APP/Jobs] Delete Job Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setJobSelection = createAction(
  '[APP/Jobs] Set Job Selection',
  props<{ jobSelection: JobSelector }>()
);

export const setJobsSelections = createAction(
  '[APP/Jobs] Set Jobs Selections',
  props<{ jobsSelections: JobSelector[] }>()
);

export const showAllJobs = createAction(
  '[APP/Jobs] Show All Jobs',
);

export const hideAllJobs = createAction(
  '[APP/Jobs] Hide All Jobs',
);

export const clearJobs = createAction(
  '[APP/Jobs] Clear Jobs',
);

export const stopRunningJob = createAction(
  '[Jobs] Stop Running Job',
  props<{ jobId: string }>()
);

export const stopRunningJobReady = createAction(
  '[Jobs] Stop Running Job Ready',
  props<{ scopeId: string, jobId: string }>()
);

export const stopRunningJobSuccess = createAction(
  '[Jobs] Stop Running Job Success',
  props<{ done: boolean }>()
);

export const stopRunningJobFailure = createAction(
  '[Jobs] Stop Running Job Failure',
  props<{ error: HttpErrorResponse }>()
);

export const stopLoading = createAction(
  '[Jobs] Stop Loading',
);
