import { AnnotationEntity } from 'slide-viewer';
import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectJobsFeatureState,
} from '../jobs-feature.state';
import { jobAdapter, jobSelectorAdapter } from './jobs.reducer';
import * as CollectionsSelectors from '@collections/store/collections/collections.selectors';
import * as AnnotationsSelectors from '@annotations/store/annotations/annotations.selectors';
import { JobRoiEntity } from './jobs.models';

const {
  selectIds,
  selectAll,
} = jobAdapter.getSelectors();

export const selectJobState = createSelector(
  selectJobsFeatureState,
  (state: ModuleState) => state.jobs
);

export const selectJobsLoaded = createSelector(
  selectJobState,
  (state) => state.loaded
);

export const selectAllJobs = createSelector(
  selectJobState,
  (state) => selectAll(state)
);

export const selectAllJobIds = createSelector(
  selectJobState,
  (state) => selectIds(state) as string[]
);

export const selectAllSelectedState = createSelector(
  selectJobState,
  (state) => state.allSelected
);

export const selectAllJobsSelections = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectAll(state.selectedJobs)
);

export const selectJobSelectionEntities = createSelector(
  selectJobState,
  (state) => jobSelectorAdapter.getSelectors().selectEntities(state.selectedJobs)
);

export const selectAllCheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !!j.checked).map(j => j.id)
);

export const selectAllUncheckedJobIds = createSelector(
  selectAllJobsSelections,
  (jobsSelections) => jobsSelections.filter(j => !j.checked).map(j => j.id)
);

export const selectAllJobsWithRois = createSelector(
  selectAllJobs,
  CollectionsSelectors.selectCollectionEntities,
  AnnotationsSelectors.selectAnnotationEntities,
  (jobs, collectionEntities, annotationEntities) => {
    const jobRoiEntities: JobRoiEntity[] = [];

    jobs.forEach(job => {
      const inputs = Object.keys(job.inputs);
      const collection = collectionEntities[job.inputs[inputs[0]]];
      const jobRoiEntity: JobRoiEntity = {
        job,
        roi: annotationEntities[collection?.items?.[0]?.id ?? job.inputs[inputs[0]]] as AnnotationEntity
      };
      if (jobRoiEntity.roi) {
        jobRoiEntities.push(jobRoiEntity);
      }
    });

    return jobRoiEntities;
  }
);
