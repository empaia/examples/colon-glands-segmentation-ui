import { EffectsModule } from '@ngrx/effects';
import { JOBS_MODULE_FEATURE_KEY, reducers } from '@jobs/store/jobs-feature.state';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobsEffects } from '@jobs/store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      JOBS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      JobsEffects
    ])
  ]
})
export class JobsModule { }
