import { HttpErrorResponse } from '@angular/common/http';
import { ExtendedScope } from '@api/wbs-api/models/extended-scope';
import { createAction, props } from '@ngrx/store';

export const setScope = createAction(
  '[APP/Scope] Set Scope',
  props<{ scopeId: string }>()
);

export const loadExtendedScope = createAction(
  '[APP/Scope] Load Extended Scope',
  props<{ scopeId: string }>()
);

export const loadExtendedScopeSuccess = createAction(
  '[APP/Scope] Load Extended Scope Success',
  props<{ extendedScope: ExtendedScope }>()
);

export const loadExtendedScopeFailure = createAction(
  '[APP/Scope] Load Extended Scope Failure',
  props<{ error: HttpErrorResponse }>()
);

export const checkEadInputCompatibilitySuccess = createAction(
  '[App/Scope] Check Ead Input Compatibility Success',
);

export const checkEadInputCompatibilityFailure = createAction(
  '[App/Scope] Check Ead Input Compatibility Failure',
  props<{ error: object }>()
);
