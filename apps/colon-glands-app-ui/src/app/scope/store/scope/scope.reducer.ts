import { ExtendedScope } from '@api/wbs-api/models/extended-scope';
import { createReducer, on } from '@ngrx/store';
import * as ScopeActions from './scope.actions';


export const SCOPE_FEATURE_KEY = 'scope';

export interface State {
  scopeId: string | undefined;
  extendedScope: ExtendedScope | undefined;
}

export const initialState: State = {
  scopeId: undefined,
  extendedScope: undefined,
};

export const reducer = createReducer(
  initialState,
  on(ScopeActions.setScope, (state, { scopeId }): State => ({
    ...state,
    scopeId,
  })),
  on(ScopeActions.loadExtendedScopeSuccess, (state, { extendedScope }): State => ({
    ...state,
    extendedScope,
  }))
);
