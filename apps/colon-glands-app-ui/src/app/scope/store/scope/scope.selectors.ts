import { ExaminationState } from '@api/wbs-api/models';
import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectScopeFeatureState,
} from '../scope-feature.state';

export const selectScopeState = createSelector(
  selectScopeFeatureState,
  (state: ModuleState) => state.scope
);

export const selectScopeId = createSelector(
  selectScopeState,
  (state) => state.scopeId
);

export const selectExtendedScope = createSelector(
  selectScopeState,
  (state) => state.extendedScope
);

export const selectExaminationState = createSelector(
  selectExtendedScope,
  (scope) => scope?.examination_state === ExaminationState.Closed
);
