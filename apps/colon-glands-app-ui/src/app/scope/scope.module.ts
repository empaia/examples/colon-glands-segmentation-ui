import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SCOPE_MODULE_FEATURE_KEY,reducers } from '@scope/store/scope-feature.state';
import { ScopeEffects } from '@scope/store';
import { MaterialModule } from '@material/material.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    StoreModule.forFeature(
      SCOPE_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      ScopeEffects
    ])
  ]
})
export class ScopeModule { }
