import { ClassesSelectors, ClassColorsSelectors } from '@classes/store';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { ClassesContainerComponent } from './classes-container.component';
import { MaterialModule } from '@material/material.module';
import { ClassesLabelComponent } from '@analysis/components/classes-label/classes-label.component';
import { ClassesComponent } from '@analysis/components/classes/classes.component';

describe('ClassesContainerComponent', () => {
  let spectator: Spectator<ClassesContainerComponent>;
  const createComponent = createComponentFactory({
    component: ClassesContainerComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      MockComponents(
        ClassesLabelComponent,
        ClassesComponent,
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: ClassesSelectors.selectAllClasses,
            value: []
          },
          {
            selector: ClassesSelectors.selectClassSelectionEntity,
            value: {}
          },
          {
            selector: ClassColorsSelectors.selectAllClassColors,
            value: {}
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
