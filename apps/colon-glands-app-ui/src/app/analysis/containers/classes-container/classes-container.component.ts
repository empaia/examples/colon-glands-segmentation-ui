import { Dictionary } from '@ngrx/entity';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ClassColorEntity, ClassEntity, ClassesSelectors, ClassColorsSelectors, ClassSelector, ClassesActions } from '@classes/store';

@Component({
  selector: 'app-classes-container',
  templateUrl: './classes-container.component.html',
  styleUrls: ['./classes-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClassesContainerComponent {
  public classEntities$: Observable<ClassEntity[]>;
  public classColorDictonary$: Observable<Dictionary<ClassColorEntity>>;
  public classSelections$: Observable<Dictionary<ClassSelector>>;

  constructor(private store: Store) {
    this.classEntities$ = this.store.select(ClassesSelectors.selectAllClasses);
    this.classColorDictonary$ = this.store.select(ClassColorsSelectors.selectAllClassColors);
    this.classSelections$ = this.store.select(ClassesSelectors.selectClassSelectionEntity);
  }

  public onClassSelectionChanged(classSelection: ClassSelector): void {
    this.store.dispatch(ClassesActions.setClassSelection({ classSelection }));
  }
}
