import { MenuSelectors } from '@menu/store';
import { provideMockStore } from '@ngrx/store/testing';
import { MenuItemLayoutContainerComponent } from '@shared/containers/menu-item-layout-container/menu-item-layout-container.component';
import { MockComponents } from 'ng-mocks';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { AnalysisContainerComponent } from './analysis-container.component';
import { AnalysisLabelComponent } from '@analysis/components/analysis-label/analysis-label.component';
import { ClassesContainerComponent } from '../classes-container/classes-container.component';
import { RegionsOfInterestContainerComponent } from '../regions-of-interest-container/regions-of-interest-container.component';
import { ResultsContainerComponent } from '../results-container/results-container.component';

describe('AnalysisContainerComponent', () => {
  let spectator: Spectator<AnalysisContainerComponent>;
  const createComponent = createComponentFactory({
    component: AnalysisContainerComponent,
    declarations: [
      MockComponents(
        MenuItemLayoutContainerComponent,
        AnalysisLabelComponent,
        ClassesContainerComponent,
        RegionsOfInterestContainerComponent,
        ResultsContainerComponent,
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: MenuSelectors.selectAnalysisMenu,
            value: {}
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
