import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { XaiLayerActions, XaiLayersSelectors } from '@analysis/store';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MatSliderChange } from '@angular/material/slider';


@Component({
  selector: 'app-xai-container',
  templateUrl: './xai-container.component.html',
  styleUrls: ['./xai-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class XaiComponent {
  public alpha$:  Observable<number>;
  public showXaiLayer$:  Observable<boolean>;

  constructor(private store: Store) {
    this.alpha$ = this.store.select(XaiLayersSelectors.selectAlphaState);
    this.showXaiLayer$ = this.store.select(XaiLayersSelectors.selectShowXaiLayerState);
  }

  public onAlphaSliderChange(event : MatSliderChange): void {
    this.store.dispatch(XaiLayerActions.setAlpha({ alpha: event.value as number } ));
  }

  public onCheckboxChange(change: MatCheckboxChange): void {
    this.store.dispatch(XaiLayerActions.setShowXaiLayer({ showXaiLayer: change.checked }));
  }
}
