import { AnnotationsActions, AnnotationsSelectors, AnnotationsViewerActions } from '@annotations/store';
import { AnnotationEntity } from 'slide-viewer';
import { Dictionary } from '@ngrx/entity';
import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { JobRoiEntity, JobSelector, JobsSelectors, JobsActions } from '@jobs/store';

@Component({
  selector: 'app-regions-of-interest-container',
  templateUrl: './regions-of-interest-container.component.html',
  styleUrls: ['./regions-of-interest-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegionsOfInterestContainerComponent {
  public jobRoiEntities$: Observable<JobRoiEntity[]>;
  public jobsSelections$: Observable<Dictionary<JobSelector>>;
  public selectedAnnotation$: Observable<string | undefined>;

  constructor(private store: Store) {
    this.jobRoiEntities$ = this.store.select(JobsSelectors.selectAllJobsWithRois);
    this.jobsSelections$ = this.store.select(JobsSelectors.selectJobSelectionEntities);
    this.selectedAnnotation$ = this.store.select(AnnotationsSelectors.selectSelectedAnnotationId);
  }

  public onZoomToAnnotation(focus: AnnotationEntity): void {
    this.store.dispatch(AnnotationsViewerActions.zoomToAnnotation({ focus }));
  }

  public onJobSelectionChanged(jobSelection: JobSelector): void {
    this.store.dispatch(JobsActions.setJobSelection({ jobSelection }));
  }

  public onRoiClicked(selected: string): void {
    this.store.dispatch(AnnotationsActions.selectAnnotation({ selected }));
  }
}
