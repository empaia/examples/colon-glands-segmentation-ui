import { JobsSelectors } from '@jobs/store';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';
import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { RegionsOfInterestContainerComponent } from './regions-of-interest-container.component';
import { RegionsOfInterestLabelComponent } from '@analysis/components/regions-of-interest-label/regions-of-interest-label.component';
import { RegionsOfInterestComponent } from '@analysis/components/regions-of-interest/regions-of-interest.component';

describe('RegionsOfInterestContainerComponent', () => {
  let spectator: Spectator<RegionsOfInterestContainerComponent>;
  const createComponent = createComponentFactory({
    component: RegionsOfInterestContainerComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        RegionsOfInterestLabelComponent,
        RegionsOfInterestComponent,
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: JobsSelectors.selectAllJobsWithRois,
            value: []
          },
          {
            selector: JobsSelectors.selectJobSelectionEntities,
            value: {}
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
