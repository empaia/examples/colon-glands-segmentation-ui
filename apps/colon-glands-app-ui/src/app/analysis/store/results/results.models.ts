import { AnnotationEntity } from 'slide-viewer';
import { SlideEntity } from '@slides/store';
import { BoolPrimitive, FloatPrimitive, IntegerPrimitive, StringPrimitive } from '@api/wbs-api/models';

export type PrimitiveEntity = IntegerPrimitive | FloatPrimitive | BoolPrimitive | StringPrimitive | AggregatedPrimitives;

export type ResultEntity = SlideEntity | AnnotationEntity;

export interface ReferenceEntity {
  id: string;
  type: 'wsi' | 'annotation';
}

export interface AggregatedPrimitives {
  id: string;
  area: number;
  title: string;
  scores: PredictionScore[];
}

export interface PredictionScore {
  label: string;
  value: number;
}
