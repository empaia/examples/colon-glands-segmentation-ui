import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import * as ResultsActions from './results.actions';
import { AggregatedPrimitives, PrimitiveEntity, ReferenceEntity } from './results.models';


export const RESULTS_FEATURE_KEY = 'results';

export interface State extends EntityState<PrimitiveEntity> {
  primitives: AggregatedPrimitives[] | null;
  loaded: boolean;
  reference?: ReferenceEntity | undefined;
  error?: HttpErrorResponse | undefined;
}

export const primitiveAdapter = createEntityAdapter<PrimitiveEntity>();

export const initialState: State = primitiveAdapter.getInitialState({
  primitives: null,
  loaded: true,
  reference: undefined,
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(ResultsActions.loadPrimitives, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(ResultsActions.loadPrimitivesSuccess, (state, { primitives }): State =>
    primitiveAdapter.setAll(primitives, {
      ...state,
      loaded: true,
    })
  ),
  on(ResultsActions.loadPrimitivesFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    error,
  })),
  on(ResultsActions.setPrimitivesReference, (state, { reference }): State => ({
    ...state,
    reference,
  })),
  on(ResultsActions.clearPrimitives, (state): State =>
    primitiveAdapter.removeAll({
      ...state,
      ...initialState
    })
  ),
);
