import * as  fromResults from './results.actions';

describe('loadResults', () => {
  it('should return an action', () => {
    expect(fromResults.clearPrimitives().type).toBe('[App/Results] Clear Primitives');
  });
});
