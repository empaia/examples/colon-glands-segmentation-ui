import { DataService } from '@api/wbs-api/services';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SpectatorService, createServiceFactory, mockProvider } from '@ngneat/spectator';
import { ResultsEffects } from './results.effects';
import { Observable } from 'rxjs';

describe('ResultsEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<ResultsEffects>;
  const createService = createServiceFactory({
    service: ResultsEffects,
    imports: [
      HttpClientTestingModule
    ],
    providers: [
      ResultsEffects,
      provideMockStore(),
      provideMockActions(() => actions$),
      mockProvider(DataService),
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
