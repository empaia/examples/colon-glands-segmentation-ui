import { createReducer, on } from '@ngrx/store';
import * as Actions from './xai-layer.actions';

export const XAI_UI_FEATURE_KEY = 'xai_ui';


export interface State {
    alpha: number;
    showXaiLayer: boolean;
}

export const initialState: State = {
    alpha: 0.7,
    showXaiLayer: false
};

export const reducer = createReducer(
    initialState,
    on(Actions.setShowXaiLayer, (state, { showXaiLayer }): State => ({
        ...state, showXaiLayer: showXaiLayer
    })),
    on(Actions.setAlpha, (state, { alpha }): State => ({
        ...state, alpha: alpha
    }))
);
