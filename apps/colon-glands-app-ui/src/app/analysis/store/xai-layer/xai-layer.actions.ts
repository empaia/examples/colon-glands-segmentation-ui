import { createAction, props } from '@ngrx/store';

export const setAlpha = createAction(
    '[XAI-UI] Set Alpha',
    props<{ alpha: number }>()
);

export const setShowXaiLayer = createAction(
    '[XAI-UI] Set Show XAI Layer',
    props<{ showXaiLayer: boolean }>()
);
