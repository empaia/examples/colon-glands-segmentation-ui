import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectAnalysisFeatureState,
} from '../analysis-feature.state';

export const selectAlphaState = createSelector(
  selectAnalysisFeatureState,
  (state: ModuleState) => state.xai_ui.alpha
);

export const selectShowXaiLayerState = createSelector(
  selectAnalysisFeatureState,
  (state: ModuleState) => state.xai_ui.showXaiLayer
);