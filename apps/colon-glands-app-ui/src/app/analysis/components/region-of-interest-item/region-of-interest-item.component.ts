import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { JobRoiEntity, JobSelector } from '@jobs/store';
import { AnnotationEntity } from 'slide-viewer';

@Component({
  selector: 'app-region-of-interest-item',
  templateUrl: './region-of-interest-item.component.html',
  styleUrls: ['./region-of-interest-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegionOfInterestItemComponent implements OnChanges {
  @Input() public jobRoiEntity!: JobRoiEntity;
  @Input() public selected!: boolean;
  @Input() public selectedRoi!: boolean;

  @Output() public changeSelection = new EventEmitter<JobSelector>();
  @Output() public roiClicked = new EventEmitter<string>();
  @Output() public zoomToAnnotation = new EventEmitter<AnnotationEntity>();

  public roiTooltip = '';

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.jobRoiEntity) {
      this.roiTooltip = this.roiToString(this.jobRoiEntity.roi);
    }
  }

  public onCheckboxChange(change: MatCheckboxChange): void {
    const jobSelector: JobSelector = {
      id: this.jobRoiEntity.job.id,
      checked: change.checked
    };
    this.changeSelection.emit(jobSelector);
  }

  public onRoiClicked(id: string): void {
    if (this.selected) {
      this.roiClicked.emit(id);
    }
  }

  private roiToString(roi: AnnotationEntity): string {
    return `
    Id: ${roi.id}
    ${roi.name ? 'Name: ' + roi.name : ''}
    ${roi.description ? 'Description: ' + roi.description : ''}
    `;
  }
}
