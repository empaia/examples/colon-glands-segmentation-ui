import { AnnotationType } from 'slide-viewer';
import { JobCreatorType, JobStatus } from '@api/wbs-api/models';
import { MockComponents } from 'ng-mocks';
import { JobStatusComponent } from '@shared/components/job-status/job-status.component';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { RegionOfInterestItemComponent } from './region-of-interest-item.component';
import { MaterialModule } from '@material/material.module';

describe('RegionOfInterestItemComponent', () => {
  let spectator: Spectator<RegionOfInterestItemComponent>;
  const createComponent = createComponentFactory({
    component: RegionOfInterestItemComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      MockComponents(
        JobStatusComponent
      )
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      jobRoiEntity: {
        job: {
          id: 'Test-Id',
          app_id: 'Test-App-Id',
          created_at: 0,
          creator_id: 'Test-Creator-Id',
          creator_type: JobCreatorType.Scope,
          inputs: {},
          outputs: {},
          status: JobStatus.Completed
        },
        roi: {
          id: 'Test-Annotation-Id',
          annotationType: AnnotationType.RECTANGLE
        }
      }
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
