import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { ClassItemComponent } from './class-item.component';
import { MaterialModule } from '@material/material.module';

describe('ClassItemComponent', () => {
  let spectator: Spectator<ClassItemComponent>;
  const createComponent = createComponentFactory({
    component: ClassItemComponent,
    imports: [
      MaterialModule
    ],
  });

  beforeEach(() => spectator = createComponent({
    props: {
      classItem: {
        id: 'Test-Id',
        name: 'Test'
      }
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
