import { MatCheckboxChange } from '@angular/material/checkbox';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';
import { ClassColorEntity, ClassEntity, ClassSelector } from '@classes/store';

@Component({
  selector: 'app-class-item',
  templateUrl: './class-item.component.html',
  styleUrls: ['./class-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClassItemComponent implements OnChanges {
  @Input() public classItem!: ClassEntity;
  @Input() public classColorEntity!: ClassColorEntity;
  @Input() public selected!: boolean;

  @Output() public changeSelection = new EventEmitter<ClassSelector>();

  public classItemTooltip = '';

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.classItem) {
      this.classItemTooltip = this.convertClassItemToString();
    }
  }

  public onCheckboxChange(change: MatCheckboxChange): void {
    const classSelector: ClassSelector = {
      id: this.classItem.id,
      checked: change.checked
    };
    this.changeSelection.emit(classSelector);
  }

  private convertClassItemToString(): string {
    return `
    Class Value: ${this.classItem.id}
    Class Name: ${this.classItem.name}
    ${this.classItem.description ? 'Description: ' + this.classItem.description : ''}
    `;
  }
}
