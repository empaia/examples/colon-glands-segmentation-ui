import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { RegionsOfInterestLabelComponent } from './regions-of-interest-label.component';

describe('RegionsOfInterestLabelComponent', () => {
  let spectator: Spectator<RegionsOfInterestLabelComponent>;
  const createComponent = createComponentFactory({
    component: RegionsOfInterestLabelComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
