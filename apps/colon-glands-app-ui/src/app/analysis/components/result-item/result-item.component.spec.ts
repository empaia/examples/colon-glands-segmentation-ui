import { DataCreatorType } from '@api/wbs-api/models';
import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { ResultItemComponent } from './result-item.component';

describe('ResultItemComponent', () => {
  let spectator: Spectator<ResultItemComponent>;
  const createComponent = createComponentFactory({
    component: ResultItemComponent,
    imports: [
      MaterialModule,
    ],
    declarations: []
  });

  beforeEach(() => spectator = createComponent({
    props: {
      primitiveEntity: {
        name: 'Test',
        type: 'string',
        value: 'Test',
        creator_id: 'Test-Creator-Id',
        creator_type: DataCreatorType.Scope
      }
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
