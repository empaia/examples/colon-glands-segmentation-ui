import { PrimitiveEntity } from '@analysis/store';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultsComponent {
  @Input() public primitiveEntities!: PrimitiveEntity[];
  @Input() public primitivesLoaded = true;
}
