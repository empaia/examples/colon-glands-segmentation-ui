import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { MockComponents } from 'ng-mocks';
import { ResultsComponent } from '@analysis/components/results/results.component';
import { ResultItemComponent } from '@analysis/components/result-item/result-item.component';
import { MaterialModule } from '@material/material.module';

describe('ResultsComponent', () => {
  let spectator: Spectator<ResultsComponent>;
  const createComponent = createComponentFactory({
    component: ResultsComponent,
    imports: [
      MaterialModule
    ],
    declarations: [
      MockComponents(
        ResultItemComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
