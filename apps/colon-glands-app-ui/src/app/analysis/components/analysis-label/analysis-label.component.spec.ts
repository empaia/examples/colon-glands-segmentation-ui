import { MenuLabelComponent } from '@shared/components/menu-label/menu-label.component';
import { MockComponents } from 'ng-mocks';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { AnalysisLabelComponent } from './analysis-label.component';

describe('AnalysisLabelComponent', () => {
  let spectator: Spectator<AnalysisLabelComponent>;
  const createComponent = createComponentFactory({
    component: AnalysisLabelComponent,
    declarations: [
      MockComponents(
        MenuLabelComponent
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
