import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-analysis-label',
  templateUrl: './analysis-label.component.html',
  styleUrls: ['./analysis-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnalysisLabelComponent {
  @Input() selected!: boolean;
  @Input() contentLoaded!: boolean;
}
