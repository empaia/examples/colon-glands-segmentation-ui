import { MockComponents } from 'ng-mocks';
import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { ClassesComponent } from './classes.component';
import { ClassItemComponent } from '../class-item/class-item.component';

describe('ClassesComponent', () => {
  let spectator: Spectator<ClassesComponent>;
  const createComponent = createComponentFactory({
    component: ClassesComponent,
    declarations: [
      MockComponents(
        ClassItemComponent
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
