import { Dictionary } from '@ngrx/entity';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ClassColorEntity, ClassEntity, ClassSelector } from '@classes/store';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClassesComponent {
  @Input() public classEntities!: ClassEntity[];
  @Input() public classColorDictonary!: Dictionary<ClassColorEntity>;
  @Input() public classSelections!: Dictionary<ClassSelector>;

  @Output() public changeClassSelection = new EventEmitter<ClassSelector>();
}
