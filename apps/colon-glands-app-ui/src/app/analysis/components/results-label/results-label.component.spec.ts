import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { ResultsLabelComponent } from './results-label.component';
import { MaterialModule } from '@material/material.module';

describe('ResultsLabelComponent', () => {
  let spectator: Spectator<ResultsLabelComponent>;
  const createComponent = createComponentFactory({
    component: ResultsLabelComponent,
    imports: [
      MaterialModule
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
