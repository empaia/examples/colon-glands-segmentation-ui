import { SlideEntity } from "@slides/store";

export interface HeadlineInfo {
  id: string;
  type: 'wsi' | 'point' | 'line' | 'arrow' | 'circle' | 'rectangle' | 'polygon';
  name?: string;
  description?: string;
}

export function headlineInfoToString(headlineInfo: HeadlineInfo): string {
  return `
  Id: ${headlineInfo.id}
  Type: ${headlineInfo.type}
  ${headlineInfo.name ? 'Name: ' + headlineInfo.name : ''}
  ${headlineInfo.description ? 'Description: ' + headlineInfo.description : ''}
  `;
}

export function isWsiInstance(object: object): object is SlideEntity {
  return 'dataView' in object;
}

export const SLIDE_NAME = 'SLIDE';
