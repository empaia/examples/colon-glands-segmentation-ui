import { Spectator, createComponentFactory } from '@ngneat/spectator/jest';
import { ClassesLabelComponent } from './classes-label.component';

describe('ClassesLabelComponent', () => {
  let spectator: Spectator<ClassesLabelComponent>;
  const createComponent = createComponentFactory({
    component: ClassesLabelComponent,
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
