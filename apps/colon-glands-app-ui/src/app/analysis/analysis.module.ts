import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MaterialModule } from '@material/material.module';
import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultItemComponent } from './components/result-item/result-item.component';
import { ResultsComponent } from './components/results/results.component';
import { ResultsLabelComponent } from './components/results-label/results-label.component';
import { ResultsContainerComponent } from './containers/results-container/results-container.component';
import { ANALYSIS_MODULE_FEATURE_KEY, reducers } from '@analysis/store/analysis-feature.state';
import { AnalysisContainerComponent } from './containers/analysis-container/analysis-container.component';
import { ClassesContainerComponent } from './containers/classes-container/classes-container.component';
import { RegionsOfInterestContainerComponent } from './containers/regions-of-interest-container/regions-of-interest-container.component';
import { AnalysisLabelComponent } from './components/analysis-label/analysis-label.component';
import { ClassesLabelComponent } from './components/classes-label/classes-label.component';
import { ClassesComponent } from './components/classes/classes.component';
import { ClassItemComponent } from './components/class-item/class-item.component';
import { RegionsOfInterestLabelComponent } from './components/regions-of-interest-label/regions-of-interest-label.component';
import { RegionsOfInterestComponent } from './components/regions-of-interest/regions-of-interest.component';
import { RegionOfInterestItemComponent } from './components/region-of-interest-item/region-of-interest-item.component';
import { ResultsEffects } from '@analysis/store/results/results.effects';
import { XaiComponent } from './containers/xai-container/xai-container.component';



@NgModule({
  declarations: [
    ResultItemComponent,
    ResultsComponent,
    ResultsLabelComponent,
    ResultsContainerComponent,
    AnalysisContainerComponent,
    ClassesContainerComponent,
    RegionsOfInterestContainerComponent,
    AnalysisLabelComponent,
    ClassesLabelComponent,
    ClassesComponent,
    ClassItemComponent,
    RegionsOfInterestLabelComponent,
    RegionsOfInterestComponent,
    RegionOfInterestItemComponent,
    XaiComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    StoreModule.forFeature(
      ANALYSIS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      ResultsEffects
    ])
  ],
  exports: [
    AnalysisContainerComponent
  ]
})
export class AnalysisModule { }
