import { SlidesModule } from '@slides/slides.module';
import { EffectsModule } from '@ngrx/effects';
import { MENU_MODULE_FEATURE_KEY, reducers } from '@menu/store/menu-feature.state';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from './../shared/shared.module';
import { MaterialModule } from '@material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './containers/menu/menu.component';
import { MenuEffects } from '@menu/store';
import { LetModule } from '@ngrx/component';
import { AnalysisModule } from '@analysis/analysis.module';



@NgModule({
  declarations: [
    MenuComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    SlidesModule,
    AnalysisModule,
    StoreModule.forFeature(
      MENU_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      MenuEffects
    ]),
    LetModule,
  ],
  exports: [
    MenuComponent
  ]
})
export class MenuModule { }
