import { SlidesSelectors } from '@slides/store';
import { MaterialModule } from '@material/material.module';
import { LetModule } from '@ngrx/component';
import { SlidesContainerComponent } from '@slides/containers/slides-container/slides-container.component';
import { MockComponents } from 'ng-mocks';
import { provideMockStore } from '@ngrx/store/testing';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { MenuComponent } from './menu.component';
import { MenuSelectors } from '@menu/store';
import { AnalysisContainerComponent } from '@analysis/containers/analysis-container/analysis-container.component';
import { SlidesLabelComponent } from '@slides/components/slides-label/slides-label.component';
import { MenuButtonComponent } from '@shared/components/menu-button/menu-button.component';

describe('MenuComponent', () => {
  let spectator: Spectator<MenuComponent>;
  const createComponent = createComponentFactory({
    component: MenuComponent,
    imports: [
      LetModule,
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        SlidesContainerComponent,
        SlidesLabelComponent,
        AnalysisContainerComponent,
        MenuButtonComponent,
      )
    ],
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: MenuSelectors.selectSlidesMenu,
            value: {}
          },
          {
            selector: MenuSelectors.selectAnalysisMenu,
            value: {}
          },
          {
            selector: MenuSelectors.selectMenuVisibilityState,
            value: false
          },
          {
            selector: MenuSelectors.selectMenuSize,
            value: 0
          },
          {
            selector: SlidesSelectors.selectSelectedSlide,
            value: undefined
          },
          {
            selector: SlidesSelectors.selectSlidesLoaded,
            value: true
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
