import { MatSidenav } from '@angular/material/sidenav';
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { MenuEntity, MenuType } from '@menu/models/menu.models';
import { MenuActions, MenuSelectors } from '@menu/store';
import { Store } from '@ngrx/store';
import { MenuButtonEventModel } from '@shared/components/menu-button/menu-button.models';
import { SlideEntity, SlidesSelectors } from '@slides/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent {
  @ViewChild('drawer') private menuDrawer!: MatSidenav;
  public slidesMenu$: Observable<MenuEntity>;
  public analysisMenu$: Observable<MenuEntity>;
  public showMenu$: Observable<boolean>;
  public menuSize$: Observable<number>;

  public slideSelected$: Observable<SlideEntity | undefined>;
  public slidesLoaded$: Observable<boolean>;

  constructor(private store: Store) {
    this.slidesMenu$ = this.store.select(MenuSelectors.selectSlidesMenu);
    this.analysisMenu$ = this.store.select(MenuSelectors.selectAnalysisMenu);
    this.showMenu$ = this.store.select(MenuSelectors.selectMenuVisibilityState);
    this.menuSize$ = this.store.select(MenuSelectors.selectMenuSize);

    this.slideSelected$ = this.store.select(SlidesSelectors.selectSelectedSlide);
    this.slidesLoaded$ = this.store.select(SlidesSelectors.selectSlidesLoaded);
  }

  public toggle(): void {
    this.menuDrawer.toggle();
    this.store.dispatch(MenuActions.toggleMenu());
  }

  public railMenuItemButton(selected: boolean, event: MenuButtonEventModel): void {
    if (this.menuDrawer && !this.menuDrawer.opened || this.menuDrawer && selected) {
      this.toggle();
    }
    this.store.dispatch(MenuActions.openMenu({ id: event.value }));
  }

  public onLabelClicked(event: MouseEvent, id: MenuType): void {
    event.stopImmediatePropagation();
    this.store.dispatch(MenuActions.openMenu({ id }));
  }
}
