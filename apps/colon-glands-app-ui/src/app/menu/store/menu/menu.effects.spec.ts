import { provideMockStore } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { MenuEffects } from './menu.effects';

describe('MenuEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<MenuEffects>;
  const createEffect = createServiceFactory({
    service: MenuEffects,
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      MenuEffects,
      provideMockActions(() => actions$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createEffect());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
