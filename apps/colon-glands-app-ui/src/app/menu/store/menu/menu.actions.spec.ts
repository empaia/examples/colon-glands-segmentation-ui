import * as fromMenu from './menu.actions';

describe('loadMenus', () => {
  it('should return an action', () => {
    expect(fromMenu.openMenu.type).toBe('[APP/Menu] Open Menu');
  });
});
