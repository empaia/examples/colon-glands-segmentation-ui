import { WorkspaceSizeService } from '@menu/services/workspace-size.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as MenuActions from './menu.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import { map } from 'rxjs/operators';



@Injectable()
export class MenuEffects {

  // open slide menu when slides are loading
  openSlideMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      map(() => MenuActions.openMenu({ id: 'slides' }))
    );
  });

  setViewerToolbarPosition$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.changeMenuWorkspaceSize),
      map(action => action.domRect),
      map(domRect => this.workspaceSizeService.setSlideViewerToolbarPosition(domRect)),
    );
  }, { dispatch: false });

  setViewerOverviewPosition$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MenuActions.changeMenuWorkspaceSize),
      map(action => action.domRect),
      map(domRect => this.workspaceSizeService.setSlideViewerOverviewPosition(domRect))
    );
  }, { dispatch: false });

  // open results menu on slide selection
  openResultsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.selectSlide),
      map(() => MenuActions.openMenu({ id: 'analysis' }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly workspaceSizeService: WorkspaceSizeService,
  ) {}
}
