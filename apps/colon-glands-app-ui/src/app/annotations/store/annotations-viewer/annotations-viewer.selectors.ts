import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectAnnotationsFeatureState,
} from '../annotations-feature.state';
import { annotationAdapter } from './annotations-viewer.reducer';

const {
  selectAll,
  selectEntities,
} = annotationAdapter.getSelectors();

export const selectAnnotationsViewerState = createSelector(
  selectAnnotationsFeatureState,
  (state: ModuleState) => state.annotationsViewer
);

export const selectAllAnnotationViewerIds = createSelector(
  selectAnnotationsViewerState,
  (state) => state.annotationIds
);

export const selectAnnotationsViewerCentroids = createSelector(
  selectAnnotationsViewerState,
  (state) => state.centroids
);

export const selectAllViewerAnnotations = createSelector(
  selectAnnotationsViewerState,
  (state) => selectAll(state)
);

export const selectAnnotationsViewerEntities = createSelector(
  selectAnnotationsViewerState,
  (state) => selectEntities(state)
);

export const selectAnnotationsViewerLoaded = createSelector(
  selectAnnotationsViewerState,
  (state) => state.loaded
);

export const selectAnnotationTypeFilters = createSelector(
  selectAnnotationsViewerState,
  (state) => state.annotationTypes
);

export const selectAnnotationViewerClearState = createSelector(
  selectAnnotationsViewerState,
  (state) => state.clear
);

export const selectFocusedViewerAnnotation = createSelector(
  selectAnnotationsViewerState,
  (state) => state.focus
);

export const selectFocusedViewerAnnotationId = createSelector(
  selectFocusedViewerAnnotation,
  (annotation) => annotation?.id
);

export const selectHiddenAnnotationViewerIds = createSelector(
  selectAnnotationsViewerState,
  (state) => state.hidden
);

export const selectRemovedAnnotationViewerIds = createSelector(
  selectAnnotationsViewerState,
  (state) => state.remove
);
