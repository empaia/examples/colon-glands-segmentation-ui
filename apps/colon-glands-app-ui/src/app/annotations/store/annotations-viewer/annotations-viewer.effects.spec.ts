import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';

import { AnnotationsViewerEffects } from './annotations-viewer.effects';

describe('AnnotationsViewerEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<AnnotationsViewerEffects>;
  const createService = createServiceFactory({
    service: AnnotationsViewerEffects,
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      AnnotationsViewerEffects,
      provideMockActions(() => actions$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
