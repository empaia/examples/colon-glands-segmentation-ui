import { Injectable } from '@angular/core';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import { DataService } from '@api/wbs-api/services';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@nrwl/angular';
import { catchError, debounceTime, filter, map } from 'rxjs/operators';
import { AnnotationsViewerFeature, ANNOTATION_QUERY_LIMIT } from '..';
import * as AnnotationsViewerActions from './annotations-viewer.actions';
import * as AnnotationsUiActions from '@annotations/store/annotations-ui/annotations-ui.actions';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as AnnotationsUiSelectors from '@annotations/store/annotations-ui/annotations-ui.selectors';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import * as ClassesActions from '@classes/store/classes/classes.actions';
import * as ClassesSelectors from '@classes/store/classes/classes.selectors';
import { dispatchActionOnErrorCode, filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { retryWhen } from 'rxjs/operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { AnnotationEntity } from 'slide-viewer';
import { AnnotationApiOutput } from '@annotations/services/annotation-types';
import { EXAMINATION_CLOSED_ERROR_STATUS_CODE } from '@menu/models/ui.models';



@Injectable()
export class AnnotationsViewerEffects {

  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => AnnotationsViewerActions.clearAnnotations())
    );
  });

  // load annotation ids when the job list was loaded
  startLoadingAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map(action => action.jobs.map(job => job.id)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds).pipe(filterNullish()),
        this.store.select(ClassesSelectors.selectAllCheckedClasses)
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds]) => !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds, classValues]) =>
        classValues.length > 0
          ? AnnotationsViewerActions.loadAnnotationIds({
            scopeId,
            slideId,
            jobIds,
            currentView,
            classValues
          })
          : AnnotationsViewerActions.clearAnnotations()
      )
    );
  });

  // load new annotation ids when the viewport has changed
  viewportChanged$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewportReady),
      map(action => action.currentView),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
        this.store.select(ClassesSelectors.selectAllCheckedClasses)
      ]),
      filter(([_currentView, _scopeId, _slideId, jobIds]) => !!jobIds.length),
      map(([currentView, scopeId, slideId, jobIds, classValues]) =>
        classValues.length > 0
          ? AnnotationsViewerActions.loadAnnotationIds({
            scopeId,
            slideId,
            jobIds,
            currentView,
            classValues
          })
          : AnnotationsViewerActions.clearAnnotations()
      )
    );
  });

  // remove focused annotation after the viewport changed
  removeFocusAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewport),
      map(() => AnnotationsViewerActions.zoomToAnnotation({ focus: undefined }))
    );
  });

  // reload annotations when job/classes gets checked or unchecked
  reloadAnnotationIdsOnJobCheck$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.showAllJobs,
        ClassesActions.setClassSelection,
        ClassesActions.setClassesSelections,
      ),
      debounceTime(1000),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
        this.store.select(ClassesSelectors.selectAllCheckedClasses),
        this.store.select(ClassesSelectors.selectAllClasses),
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds]) => !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds, selectedClasses]) =>
        selectedClasses.length > 0
          ? AnnotationsViewerActions.loadAnnotationIds({
            scopeId,
            slideId,
            currentView,
            jobIds,
            classValues: selectedClasses
          })
          : AnnotationsViewerActions.clearAnnotations()
      )
    );
  });

  loadAnnotationIds$ = createEffect(() => {
    return this, this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotationIds),
      fetch({
        id: (
          action: ReturnType<typeof AnnotationsViewerActions.loadAnnotationIds>,
          _state: AnnotationsViewerFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<typeof AnnotationsViewerActions.loadAnnotationIds>,
          _state: AnnotationsViewerFeature.State
        ) => {
          return this.dataService.scopeIdAnnotationsQueryViewerPut({
            scope_id: action.scopeId,
            body: {
              references: [
                action.slideId
              ],
              jobs: action.jobIds.length ? action.jobIds : [null],
              viewport: action.currentView.viewport,
              npp_viewing: this.viewportConverter.calculateNppViewRange(action.currentView.nppCurrent, action.currentView.nppBase),
              class_values: action.classValues,
              types: action.annotationTypes?.map(this.annotationConverter.convertAnnotationTypeToApi)
            }
          }).pipe(
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(results => AnnotationsViewerActions.loadAnnotationIdsSuccess({
              annotationIds: results.annotations,
              centroids: results.low_npp_centroids,
            }))
          );
        },
        onError: (_action: ReturnType<typeof AnnotationsViewerActions.loadAnnotationIds>, error) => {
          return AnnotationsViewerActions.loadAnnotationIdsFailure({ error });
        }
      })
    );
  });

  prepareLoadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotations),
      map(action => action.annotationIds),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
      ]),
      filter(([annotationIds, _scopeId, jobIds]) =>
        !!annotationIds.length
        && !!jobIds.length
      ),
      map(([annotationIds, scopeId, jobIds]) =>
        AnnotationsViewerActions.loadAnnotationsReady({
          scopeId,
          annotationIds,
          jobIds,
          withClasses: true,
          skip: 0,
        })
      )
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadAnnotationsReady),
      fetch({
        id: (
          action: ReturnType<typeof AnnotationsViewerActions.loadAnnotationsReady>,
          _state: AnnotationsViewerFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<typeof AnnotationsViewerActions.loadAnnotationsReady>,
          _state: AnnotationsViewerFeature.State
        ) => {
          return this.dataService.scopeIdAnnotationsQueryPut({
            scope_id: action.scopeId,
            with_classes: action.withClasses,
            skip: action.skip,
            limit: ANNOTATION_QUERY_LIMIT,
            body: {
              annotations: action.annotationIds,
              jobs: action.jobIds?.length ? action.jobIds : [null]
            }
          }).pipe(
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(result => result.items.map(a => this.annotationConverter.fromApiType(a) as AnnotationEntity)),
            map(annotations => AnnotationsViewerActions.loadAnnotationsSuccess({ annotations }))
          );
        },
        onError: (_action: ReturnType<typeof AnnotationsViewerActions.loadAnnotationsReady>, error) => {
          return AnnotationsViewerActions.loadAnnotationsFailure({ error });
        }
      })
    );
  });

  prepareCreateAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotation),
      map(action => action.annotation),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish())
      ]),
      map(([annotation, scopeId, slideId]) => AnnotationsViewerActions.createAnnotationReady({
        annotation,
        scopeId,
        slideId,
      }))
    );
  });

  createAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationReady),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof AnnotationsViewerActions.createAnnotationReady>,
          _state: AnnotationsViewerFeature.State
        ) => {
          return this.dataService.scopeIdAnnotationsPost({
            scope_id: action.scopeId,
            is_roi: true,
            body: this.annotationConverter.toApiPostType(
              action.annotation,
              action.slideId,
              action.scopeId,
            )
          }).pipe(
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(result => this.annotationConverter.fromApiType(result as AnnotationApiOutput) as AnnotationEntity),
            map(annotation => AnnotationsViewerActions.createAnnotationSuccess({ annotation })),
            catchError(error =>
              dispatchActionOnErrorCode(
                error,
                EXAMINATION_CLOSED_ERROR_STATUS_CODE,
                ScopeActions.loadExtendedScope({ scopeId: action.scopeId })
              )
            )
          );
        },
        onError: (_action: ReturnType<typeof AnnotationsViewerActions.createAnnotationReady>, error) => {
          return AnnotationsViewerActions.createAnnotationFailure({ error });
        }
      })
    );
  });

  // delete the current annotation when something goes wrong at setting the job inputs
  /* removeAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.setJobInputFailure),
      map(action => AnnotationsViewerActions.deleteAnnotation({ ...action }))
    );
  }); */

  deleteAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.deleteAnnotation),
      pessimisticUpdate({
        run: (
          action: ReturnType<typeof AnnotationsViewerActions.deleteAnnotation>,
          _state: AnnotationsViewerFeature.State
        ) => {
          return this.dataService.scopeIdAnnotationsAnnotationIdDelete({
            scope_id: action.scopeId,
            annotation_id: action.annotationId
          }).pipe(
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(result => result.id),
            map(annotationId => AnnotationsViewerActions.deleteAnnotationSuccess({ annotationId }))
          );
        },
        onError: (_action: ReturnType<typeof AnnotationsViewerActions.deleteAnnotation>, error) => {
          return AnnotationsViewerActions.deleteAnnotationFailure({ error });
        }
      })
    );
  });

  // add focused annotation to slide-viewer cache
  addAnnotationOnFocus$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.zoomToAnnotation),
      map(action => action.focus),
      filterNullish(),
      map(annotation => AnnotationsViewerActions.addAnnotation({ annotation }))
    );
  });

  prepareLoadHiddenAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.hideAllJobs,
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllUncheckedJobIds).pipe(filterNullish()),
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds]) => !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds]) =>
        AnnotationsViewerActions.loadHiddenAnnotationIds({
          scopeId,
          slideId,
          currentView,
          jobIds
        })
      )
    );
  });

  loadHiddenAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.loadHiddenAnnotationIds),
      fetch({
        run: (
          action: ReturnType<typeof AnnotationsViewerActions.loadHiddenAnnotationIds>,
          _state: AnnotationsViewerFeature.State
        ) => {
          return this.dataService.scopeIdAnnotationsQueryViewerPut({
            scope_id: action.scopeId,
            body: {
              references: [
                action.slideId
              ],
              jobs: action.jobIds.length ? action.jobIds : [null],
              viewport: action.currentView.viewport,
              npp_viewing: this.viewportConverter.calculateNppViewRange(action.currentView.nppCurrent, action.currentView.nppBase),
              class_values: action.classValues,
              types: action.annotationTypes?.map(this.annotationConverter.convertAnnotationTypeToApi)
            }
          }).pipe(
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(results => results.annotations),
            map(hidden => AnnotationsViewerActions.loadHiddenAnnotationIdsSuccess({ hidden }))
          );
        },
        onError: (_action: ReturnType<typeof AnnotationsViewerActions.loadHiddenAnnotationIds>, error) => {
          return AnnotationsViewerActions.loadHiddenAnnotationIdsFailure({ error });
        }
      })
    );
  });

  // clear centroids on hide or result selection when no job is checked
  clearCentroids$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.hideAllJobs,
      ),
      concatLatestFrom(() =>
        this.store.select(JobsSelectors.selectAllCheckedJobIds)
      ),
      filter(([, jobIds]) => !jobIds.length),
      map(() => AnnotationsViewerActions.clearCentroids())
    );
  });

  // stop loading state when loading extended scope
  stopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => AnnotationsViewerActions.stopLoading())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: DataService,
    private readonly annotationConverter: AnnotationConversionService,
    private readonly viewportConverter: ViewportConversionService,
  ) { }
}
