import { HttpErrorResponse } from '@angular/common/http';
import { AnnotationEntity, AnnotationType } from 'slide-viewer';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as AnnotationsViewerActions from './annotations-viewer.actions';


export const ANNOTATIONS_VIEWER_FEATURE_KEY = 'annotationsViewer';

export interface State extends EntityState<AnnotationEntity> {
  annotationIds: string[];
  centroids: number[][];
  remove: string[];
  loaded: boolean;
  clear: boolean;
  hidden: string[];
  annotationTypes?: AnnotationType[];
  focus?: AnnotationEntity | undefined;
  error?: HttpErrorResponse | null;
}

export const annotationAdapter = createEntityAdapter<AnnotationEntity>();

export const initialState: State = annotationAdapter.getInitialState({
  annotationIds: [],
  centroids: [],
  remove: [],
  loaded: true,
  clear: false,
  hidden: [],
  annotationTypes: undefined,
  focus: undefined,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(AnnotationsViewerActions.loadAnnotationIds, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      loaded: false,
      clear: false,
    })
  ),
  on(AnnotationsViewerActions.loadAnnotationIdsSuccess, (state, { annotationIds, centroids }): State => ({
    ...state,
    centroids,
    remove: state.annotationIds.filter(id => !annotationIds.includes(id)),
    loaded: true,
    clear: false,
    annotationIds,
  })),
  on(AnnotationsViewerActions.loadAnnotationIdsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    clear: false,
    error,
  })),
  on(AnnotationsViewerActions.loadAnnotations, (state): State => ({
    ...state,
    loaded: false,
    clear: false,
  })),
  on(AnnotationsViewerActions.loadAnnotationsSuccess, (state, { annotations }): State =>
    annotationAdapter.setAll(annotations, {
      ...state,
      loaded: true,
      clear: false,
    })
  ),
  on(AnnotationsViewerActions.loadAnnotationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    clear: false,
    error,
  })),
  on(AnnotationsViewerActions.clearAnnotations, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      ...initialState,
      clear: true,
    })
  ),
  on(AnnotationsViewerActions.createAnnotation, (state): State => ({
    ...state,
    loaded: false,
    clear: false,
  })),
  on(AnnotationsViewerActions.createAnnotationSuccess, (state, { annotation }): State =>
    annotationAdapter.upsertOne(annotation, {
      ...state,
      loaded: true,
      clear: false,
      annotationIds: state.annotationIds.concat(annotation.id as string),
    })
  ),
  on(AnnotationsViewerActions.createAnnotationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    clear: false,
    error,
  })),
  on(AnnotationsViewerActions.setAnnotationTypeFilter, (state, { annotationTypes }): State => ({
    ...state,
    clear: false,
    annotationTypes,
  })),
  on(AnnotationsViewerActions.deleteAnnotationSuccess, (state, { annotationId }): State =>
    annotationAdapter.removeOne(annotationId, {
      ...state,
      annotationIds: [...state.annotationIds]
        .splice(0, state.annotationIds.indexOf(annotationId))
        .concat(
          [...state.annotationIds]
            .splice(state.annotationIds.indexOf(annotationId), state.annotationIds.length - 1)
        ),
      remove: [annotationId],
    })
  ),
  on(AnnotationsViewerActions.deleteAnnotationFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(AnnotationsViewerActions.zoomToAnnotation, (state, { focus }): State => ({
    ...state,
    focus,
  })),
  on(AnnotationsViewerActions.addAnnotation, (state, { annotation }): State =>
    annotationAdapter.upsertOne(annotation, {
      ...state,
      annotationIds: [...state.annotationIds, annotation.id]
    })
  ),
  on(AnnotationsViewerActions.loadHiddenAnnotationIds, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      loaded: false,
      clear: false,
    })
  ),
  on(AnnotationsViewerActions.loadHiddenAnnotationIdsSuccess, (state, { hidden }): State => ({
    ...state,
    hidden,
    loaded: true,
  })),
  on(AnnotationsViewerActions.loadHiddenAnnotationIdsFailure, (state, { error }): State => ({
    ...state,
    hidden: [],
    loaded: true,
    error,
  })),
  on(AnnotationsViewerActions.clearCentroids, (state): State => ({
    ...state,
    centroids: []
  })),
  on(AnnotationsViewerActions.stopLoading, (state): State => ({
    ...state,
    loaded: true,
    clear: false,
  }))
);
