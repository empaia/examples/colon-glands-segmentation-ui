import * as fromAnnotations from './annotations.actions';

describe('loadAnnotations', () => {
  it('should return an action', () => {
    expect(fromAnnotations.loadInputAnnotations({
      scopeId: 'SCOPE-ID',
      withClasses: false,
      skip: 0,
      limit: 0,
      slideId: 'SLIDE-ID',
      jobIds: ['JOB-ID']
    }).type).toBe('[APP/Annotations] Load Input Annotations');
  });
});
