import { HttpErrorResponse } from '@angular/common/http';
import { AnnotationEntity } from 'slide-viewer';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as AnnotationsActions from './annotations.actions';


export const ANNOTATIONS_FEATURE_KEY = 'annotations';

export interface State extends EntityState<AnnotationEntity> {
  loaded: boolean;
  selected?: string | undefined;
  error?: HttpErrorResponse | null;
}

export const annotationAdapter = createEntityAdapter<AnnotationEntity>();

export const initialState: State = annotationAdapter.getInitialState({
  loaded: true,
  selected: undefined,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(AnnotationsActions.loadInputAnnotations, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(AnnotationsActions.loadInputAnnotationsSuccess, (state, { annotations }): State =>
    annotationAdapter.upsertMany(annotations, {
      ...state,
      loaded: true,
    })
  ),
  on(AnnotationsActions.loadInputAnnotationsFailure, (state, { error }): State => ({
    ...state,
    error,
    loaded: true,
  })),
  on(AnnotationsActions.clearAnnotations, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      ...initialState,
    })
  ),
  on(AnnotationsActions.selectAnnotation, (state, { selected }): State => ({
    ...state,
    selected,
  })),
  on(AnnotationsActions.addAnnotation, (state, { annotation }): State =>
    annotationAdapter.addOne(annotation, {
      ...state,
    })
  ),
);
