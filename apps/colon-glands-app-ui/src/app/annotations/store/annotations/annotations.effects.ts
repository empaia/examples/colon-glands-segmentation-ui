import { Injectable } from '@angular/core';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { DataService } from '@api/wbs-api/services';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@nrwl/angular';
import { filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { filter, map, retryWhen } from 'rxjs/operators';
import { AnnotationEntity } from 'slide-viewer';
import { AnnotationsFeature, INPUT_ANNOTATION_LIMIT } from '..';
import * as AnnotationsActions from './annotations.actions';
import * as AnnotationsViewerActions from '@annotations/store/annotations-viewer/annotations-viewer.actions';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import { requestNewToken } from 'vendor-app-communication-interface';




@Injectable()
export class AnnotationsEffects {

  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => AnnotationsActions.clearAnnotations())
    );
  });

  // load annotations when job list was fetched
  loadAnnotationsAfterJobs$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map(action => action.jobs.map(j => j.id)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
      ]),
      filter(([jobIds]) => !!jobIds && !!jobIds.length),
      map(([jobIds, scopeId, slideId]) =>
        AnnotationsActions.loadInputAnnotations({
          scopeId,
          slideId,
          jobIds,
          withClasses: true,
          skip: 0,
          limit: INPUT_ANNOTATION_LIMIT,
        })
      )
    );
  });

  // add annotation after creation
  addAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsViewerActions.createAnnotationSuccess),
      map(action => action.annotation),
      map(annotation => AnnotationsActions.addAnnotation({ annotation }))
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsActions.loadInputAnnotations),
      fetch({
        run: (
          action: ReturnType<typeof AnnotationsActions.loadInputAnnotations>,
          _state: AnnotationsFeature.State,
        ) => {
          return this.dataService.scopeIdAnnotationsQueryPut({
            scope_id: action.scopeId,
            with_classes: action.withClasses,
            skip: action.skip,
            limit: action.limit,
            body: {
              creators: [
                action.scopeId
              ],
              references: [
                action.slideId
              ],
              jobs: action.jobIds?.length ? action.jobIds : [null],
            }
          }).pipe(
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(result => result.items.map(a => this.annotationConversion.fromApiType(a) as AnnotationEntity)),
            map(annotations => AnnotationsActions.loadInputAnnotationsSuccess({ annotations }))
          );
        },
        onError: (_action: ReturnType<typeof AnnotationsActions.loadInputAnnotations>, error) => {
          return AnnotationsActions.loadInputAnnotationsFailure({ error });
        }
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: DataService,
    private readonly annotationConversion: AnnotationConversionService,
  ) {}
}
