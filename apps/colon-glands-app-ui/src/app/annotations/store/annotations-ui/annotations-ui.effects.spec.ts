import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';

import { AnnotationsUiEffects } from './annotations-ui.effects';

describe('AnnotationsUiEffects', () => {
  let action$: Observable<unknown>;
  let spectator: SpectatorService<AnnotationsUiEffects>;
  const createService = createServiceFactory({
    service: AnnotationsUiEffects,
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      AnnotationsUiEffects,
      provideMockActions(() => action$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
