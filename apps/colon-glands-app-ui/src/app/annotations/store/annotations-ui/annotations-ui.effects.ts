import { fetch } from '@nrwl/angular';
import { Injectable } from '@angular/core';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { filter, map } from 'rxjs/operators';
import * as AnnotationsUiActions from './annotations-ui.actions';
import * as AnnotationsUiFeature from './annotations-ui.reducer';
import * as ResultsActions from '@analysis/store/results/results.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import { ExaminationState } from '@api/wbs-api/models';
import { AnnotationEntity, DrawType, InteractionType } from 'slide-viewer';
import { EadService } from '@core/services/ead.service';
import { EmpaiaAppDescriptionV3 } from '@core/models/ead.models';
import { DataService } from '@api/wbs-api/services';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { Store } from '@ngrx/store';
import { filterNullish } from '@shared/helper/rxjs-operators';



@Injectable()
export class AnnotationsUiEffects {

  convertViewport$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewport),
      map(action => action.currentView),
      map(currentView => {
        return {
          viewport: this.viewportConverter.convertFromApi(currentView.extent),
          nppCurrent: currentView.nppCurrent,
          nppBase: currentView.nppBase,
        };
      }),
      map(currentView => AnnotationsUiActions.setViewportReady({ currentView }))
    );
  });

  // set allowed interaction types to move and rectangle
  setAllowedInteractionTypes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlides),
      map(() => AnnotationsUiActions.setAllowedInteractionTypes({
        allowedDrawTypes: [InteractionType.Move]
      }))
    );
  });

  // set active interaction to move on examination close
  setInteractionToMoveOnClose$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope.examination_state),
      filter(examinationState => examinationState === ExaminationState.Closed),
      map(() => AnnotationsUiActions.setInteractionType({ interactionType: InteractionType.Move }))
    );
  });

  // set interaction type move and compatible types when examination
  // is open
  setDrawTypesOnOpenExamination$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      map(action => action.extendedScope),
      map(extendedScope => {
        if (extendedScope.examination_state === ExaminationState.Open) {
          const types = this.eadService.getV3AnnotationInputTypes(extendedScope.ead as EmpaiaAppDescriptionV3, 'standalone');
          return AnnotationsUiActions.setAllowedInteractionTypes({
            allowedDrawTypes: [
              InteractionType.Move,
              // if polygon is an allowed input add alternative polygon as draw mode
              ...(types.includes('polygon') ? (types as DrawType[]).concat(DrawType.PolygonAlternative) : types as DrawType[])
            ]
          });
        } else {
          return AnnotationsUiActions.setAllowedInteractionTypes({
            allowedDrawTypes: [InteractionType.Move]
          });
        }
      })
    );
  });

  // deactivate annotation draw tools if app is not compatible
  disableDrawToolsOnIncompatibility$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.checkEadInputCompatibilityFailure),
      map(() => AnnotationsUiActions.setAllowedInteractionTypes({
        allowedDrawTypes: [InteractionType.Move]
      }))
    );
  });

  // load annotation when the set reference is of
  // type annotation
  loadOnReferenceSet$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ResultsActions.setPrimitivesReference),
      map(action => action.reference),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())
      ),
      map(([reference, scopeId]) =>
        reference?.type === 'annotation'
          ? AnnotationsUiActions.loadReferenceAnnotation({
            scopeId,
            annotationId: reference.id
          })
          : AnnotationsUiActions.clearReferenceAnnotation()
      )
    );
  });

  loadReferenceAnnotation$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.loadReferenceAnnotation),
      fetch({
        run: (
          action: ReturnType<typeof AnnotationsUiActions.loadReferenceAnnotation>,
          _state: AnnotationsUiFeature.State
        ) => {
          return this.dataService.scopeIdAnnotationsAnnotationIdGet({
            scope_id: action.scopeId,
            annotation_id: action.annotationId
          }).pipe(
            map(annotation => this.annotationConverter.fromApiType(annotation) as AnnotationEntity),
            map(referenceAnnotation => AnnotationsUiActions.loadReferenceAnnotationSuccess({ referenceAnnotation }))
          );
        },
        onError: (_action: ReturnType<typeof AnnotationsUiActions.loadReferenceAnnotation>, error) => {
          return AnnotationsUiActions.loadReferenceAnnotationFailure({ error });
        }
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly viewportConverter: ViewportConversionService,
    private readonly eadService: EadService,
    private readonly dataService: DataService,
    private readonly annotationConverter: AnnotationConversionService,
    private readonly store: Store,
  ) {}
}
