import { AnnotationEntity, ToolbarInteractionType } from 'slide-viewer';
import { CurrentView } from './annotations-ui.models';
import { createReducer, on } from '@ngrx/store';
import * as AnnotationsUiActions from './annotations-ui.actions';


export const ANNOTATIONS_UI_FEATURE_KEY = 'annotationsUi';

export interface State {
  currentView?: CurrentView;
  interactionType?: ToolbarInteractionType;
  allowedDrawTypes?: ToolbarInteractionType[];
  referenceAnnotation?: AnnotationEntity | undefined;
}

export const initialState: State = {
  currentView: undefined,
  interactionType: undefined,
  allowedDrawTypes: undefined,
  referenceAnnotation: undefined,
};

export const reducer = createReducer(
  initialState,
  on(AnnotationsUiActions.setViewportReady, (state, { currentView }): State => ({
    ...state,
    currentView,
  })),
  on(AnnotationsUiActions.setInteractionType, (state, { interactionType }): State => ({
    ...state,
    interactionType,
  })),
  on(AnnotationsUiActions.setAllowedInteractionTypes, (state, { allowedDrawTypes }): State => ({
    ...state,
    allowedDrawTypes,
  })),
  on(AnnotationsUiActions.loadReferenceAnnotationSuccess, (state, { referenceAnnotation }): State => ({
    ...state,
    referenceAnnotation,
  })),
  on(AnnotationsUiActions.clearReferenceAnnotation, (state): State => ({
    ...state,
    referenceAnnotation: undefined,
  })),
);
