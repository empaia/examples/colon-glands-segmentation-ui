import * as fromAnnotationsUi from './annotations-ui.actions';

describe('loadAnnotationsUis', () => {
  it('should return an action', () => {
    expect(fromAnnotationsUi.setViewport({
      currentView: {
        extent: [],
        zoom: 0,
        nppCurrent: 0,
        nppMin: 0,
        nppMax: 0,
        nppBase: 0,
      }
    }).type).toBe('[APP/Annotation UI] Set Viewport');
  });
});
