import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromAnnotationsUi from '@annotations/store/annotations-ui/annotations-ui.reducer';
import * as fromAnnotationsViewer from '@annotations/store/annotations-viewer/annotations-viewer.reducer';
import * as fromAnnotations from '@annotations/store/annotations/annotations.reducer';

export const ANNOTATIONS_MODULE_FEATURE_KEY = 'annotationsModuleFeature';

export const selectAnnotationsFeatureState = createFeatureSelector<State>(
  ANNOTATIONS_MODULE_FEATURE_KEY
);

export interface State {
  [fromAnnotationsUi.ANNOTATIONS_UI_FEATURE_KEY]: fromAnnotationsUi.State;
  [fromAnnotationsViewer.ANNOTATIONS_VIEWER_FEATURE_KEY]: fromAnnotationsViewer.State;
  [fromAnnotations.ANNOTATIONS_FEATURE_KEY]: fromAnnotations.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromAnnotationsUi.ANNOTATIONS_UI_FEATURE_KEY]: fromAnnotationsUi.reducer,
    [fromAnnotationsViewer.ANNOTATIONS_VIEWER_FEATURE_KEY]: fromAnnotationsViewer.reducer,
    [fromAnnotations.ANNOTATIONS_FEATURE_KEY]: fromAnnotations.reducer,
  })(state, action);
}
