import { ApiConfiguration } from '@api/wbs-api/api-configuration';
import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WbsUrlService {
  private _wbsUrl!: string;
  private _wbsUrlExtension = `/${this.apiVersion}/scopes`;

  constructor(
    private apiConfig: ApiConfiguration,
    @Inject('API_VERSION') private apiVersion: string,
  ) {}

  public get wbsUrl() {
    return this._wbsUrl;
  }

  public set wbsUrl(val: string) {
    this._wbsUrl = val;
    this.setRootUrl();
  }

  public get wbsUrlExtension() {
    return this._wbsUrlExtension;
  }

  public get wbsUrlComplete() {
    return this._wbsUrl + this._wbsUrlExtension;
  }

  private setRootUrl(): void {
    this.apiConfig.rootUrl = this.wbsUrlComplete;
  }
}
