import { WbsUrl } from 'vendor-app-communication-interface';
import { createAction, props } from '@ngrx/store';

export const setWbsUrl = createAction(
  '[APP/WbsUrl] Set Wbs Url',
  props<{ wbsUrl: WbsUrl }>()
);
