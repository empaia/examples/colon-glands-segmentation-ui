import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectWbsUrlFeatureState,
} from '../wbs-url-feature.state';

export const selectWbsUrlState = createSelector(
  selectWbsUrlFeatureState,
  (state: ModuleState) => state.wbsUrl
);

export const selectWbsUrl = createSelector(
  selectWbsUrlState,
  (state) => state.wbsUrl
);
