import { map } from 'rxjs/operators';
import { WbsUrlService } from '@wbs-url/services/wbs-url.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as WbsUrlActions from './wbs-url.actions';


@Injectable()
export class WbsUrlEffects {

  setWbsUrl$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(WbsUrlActions.setWbsUrl),
      map(action => action.wbsUrl),
      map(wbsUrl => this.wbsUrlService.wbsUrl = wbsUrl.url)
    );
  }, { dispatch: false });

  constructor(
    private readonly actions$: Actions,
    private readonly wbsUrlService: WbsUrlService,
  ) {}
}
