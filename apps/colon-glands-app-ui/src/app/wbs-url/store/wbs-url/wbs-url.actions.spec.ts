import * as fromWbsUrl from './wbs-url.actions';

describe('loadWbsUrls', () => {
  it('should return an action', () => {
    expect(fromWbsUrl.setWbsUrl({
      wbsUrl: { url: 'test-wbs-url', type: 'wbsUrl' }
    }).type).toBe('[APP/WbsUrl] Set Wbs Url');
  });
});
