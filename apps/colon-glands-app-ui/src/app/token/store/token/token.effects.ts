import { requestNewToken } from 'vendor-app-communication-interface';
import { map } from 'rxjs/operators';
import { AccessTokenService } from '@token/services/access-token.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as TokenActions from './token.actions';



@Injectable()
export class TokenEffects {

  setAccessToken$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.setAccessToken),
      map(action => action.token),
      map(token => this.accessTokenService.accessToken = token.value)
    );
  }, { dispatch: false });

  requestNewToke$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TokenActions.requestNewToken),
      map(() => requestNewToken())
    );
  }, { dispatch: false });

  constructor(
    private readonly actions$: Actions,
    private readonly accessTokenService: AccessTokenService,
  ) {}
}
