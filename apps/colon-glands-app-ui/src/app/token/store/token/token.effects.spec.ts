import { provideMockActions } from '@ngrx/effects/testing';
import { AccessTokenService } from '@token/services/access-token.service';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';
import { Observable } from 'rxjs';

import { TokenEffects } from './token.effects';

describe('TokenEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<TokenEffects>;
  const createService = createServiceFactory({
    service: TokenEffects,
    providers: [
      TokenEffects,
      AccessTokenService,
      provideMockActions(() => actions$)
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
