import { Token } from 'vendor-app-communication-interface';
import { createReducer, on } from '@ngrx/store';
import * as TokenActions from './token.actions';


export const TOKEN_FEATURE_KEY = 'token';

export interface State {
  token: Token | undefined;
}

export const initialState: State = {
  token: undefined,
};

export const reducer = createReducer(
  initialState,
  on(TokenActions.setAccessToken, (state, { token }): State => ({
    ...state,
    token,
  }))
);
