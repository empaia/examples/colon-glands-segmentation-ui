import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromRectangleExplanations from '@xai/store/rectangle-explanations/rectangle-explanations.reducer';

export const EXPLANATIONS_MODULE_FEATURE_KEY = 'explanationsModuleFeature';

export const selectExplanationFeatureState = createFeatureSelector<State>(
  EXPLANATIONS_MODULE_FEATURE_KEY
);

export interface State {
  [fromRectangleExplanations.RECTANGLE_EXPLANATIONS_VIEWER_FEATURE_KEY]: fromRectangleExplanations.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromRectangleExplanations.RECTANGLE_EXPLANATIONS_VIEWER_FEATURE_KEY]: fromRectangleExplanations.reducer,
  })(state, action);
}
