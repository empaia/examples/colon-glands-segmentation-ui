import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectExplanationFeatureState,
} from '../explanations.state';
import { annotationAdapter } from './rectangle-explanations.reducer';

const {
  selectAll,
  selectEntities,
} = annotationAdapter.getSelectors();

export const selectAnnotationsViewerState = createSelector(
  selectExplanationFeatureState,
  (state: ModuleState) => state.rectangleExplanationsViewer
);

export const selectAllAnnotationViewerIds = createSelector(
  selectAnnotationsViewerState,
  (state) => state.annotationIds
);

// export const selectAnnotationsViewerCentroids = createSelector(
//   selectAnnotationsViewerState,
//   (state) => state.centroids
// );

export const selectAllViewerAnnotations = createSelector(
  selectAnnotationsViewerState,
  (state) => selectAll(state)
);

export const selectAnnotationsViewerEntities = createSelector(
  selectAnnotationsViewerState,
  (state) => selectEntities(state)
);

export const selectAnnotationsViewerLoaded = createSelector(
  selectAnnotationsViewerState,
  (state) => state.loaded
);

export const selectAnnotationTypeFilters = createSelector(
  selectAnnotationsViewerState,
  (state) => state.annotationTypes
);

export const selectAnnotationViewerClearState = createSelector(
  selectAnnotationsViewerState,
  (state) => state.clear
);

export const selectFocusedViewerAnnotation = createSelector(
  selectAnnotationsViewerState,
  (state) => state.focus
);

export const selectFocusedViewerAnnotationId = createSelector(
  selectFocusedViewerAnnotation,
  (annotation) => annotation?.id
);

export const selectHiddenAnnotationViewerIds = createSelector(
  selectAnnotationsViewerState,
  (state) => state.hidden
);

export const selectRemovedAnnotationViewerIds = createSelector(
  selectAnnotationsViewerState,
  (state) => state.remove
);