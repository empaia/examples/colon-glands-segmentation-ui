import { HttpErrorResponse } from '@angular/common/http';
import { AnnotationEntity, AnnotationType } from 'slide-viewer';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as Actions from './rectangle-explanations.actions';


export const RECTANGLE_EXPLANATIONS_VIEWER_FEATURE_KEY = 'rectangleExplanationsViewer';

export interface State extends EntityState<AnnotationEntity> {
  annotationIds: string[];
  centroids: number[][];
  remove: string[];
  loaded: boolean;
  clear: boolean;
  hidden: string[];
  annotationTypes?: AnnotationType[];
  focus?: AnnotationEntity | undefined;
  error?: HttpErrorResponse | null;
}

export const annotationAdapter = createEntityAdapter<AnnotationEntity>();

export const initialState: State = annotationAdapter.getInitialState({
  annotationIds: [],
  centroids: [],
  remove: [],
  loaded: true,
  clear: false,
  hidden: [],
  annotationTypes: undefined,
  focus: undefined,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(Actions.loadAnnotationIds, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      loaded: false,
      clear: false,
    })
  ),
  on(Actions.loadAnnotationIdsSuccess, (state, { annotationIds, centroids }): State => ({
    ...state,
    centroids,
    remove: state.annotationIds.filter(id => !annotationIds.includes(id)),
    loaded: true,
    clear: false,
    annotationIds,
  })),
  on(Actions.loadAnnotationIdsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    clear: false,
    error,
  })),
  on(Actions.loadAnnotations, (state): State => ({
    ...state,
    loaded: false,
    clear: false,
  })),
  on(Actions.loadAnnotationsSuccess, (state, { annotations }): State =>
    annotationAdapter.setAll(annotations, {
      ...state,
      loaded: true,
      clear: false,
    })
  ),
  on(Actions.loadAnnotationsFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    clear: false,
    error,
  })),
  on(Actions.clearAnnotations, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      ...initialState,
      clear: true,
    })
  ),
  on(Actions.createAnnotation, (state): State => ({
    ...state,
    loaded: false,
    clear: false,
  })),
  on(Actions.createAnnotationSuccess, (state, { annotation }): State =>
    annotationAdapter.upsertOne(annotation, {
      ...state,
      loaded: true,
      clear: false,
      annotationIds: state.annotationIds.concat(annotation.id as string),
    })
  ),
  on(Actions.createAnnotationFailure, (state, { error }): State => ({
    ...state,
    loaded: true,
    clear: false,
    error,
  })),
  on(Actions.setAnnotationTypeFilter, (state, { annotationTypes }): State => ({
    ...state,
    clear: false,
    annotationTypes,
  })),
  on(Actions.deleteAnnotationSuccess, (state, { annotationId }): State =>
    annotationAdapter.removeOne(annotationId, {
      ...state,
      annotationIds: [...state.annotationIds]
        .splice(0, state.annotationIds.indexOf(annotationId))
        .concat(
          [...state.annotationIds]
            .splice(state.annotationIds.indexOf(annotationId), state.annotationIds.length - 1)
        ),
      remove: [annotationId],
    })
  ),
  on(Actions.deleteAnnotationFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(Actions.zoomToAnnotation, (state, { focus }): State => ({
    ...state,
    focus,
  })),
  on(Actions.addAnnotation, (state, { annotation }): State =>
    annotationAdapter.upsertOne(annotation, {
      ...state,
      annotationIds: [...state.annotationIds, annotation.id]
    })
  ),
  on(Actions.loadHiddenAnnotationIds, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      loaded: false,
      clear: false,
    })
  ),
  on(Actions.loadHiddenAnnotationIdsSuccess, (state, { hidden }): State => ({
    ...state,
    hidden,
    loaded: true,
  })),
  on(Actions.loadHiddenAnnotationIdsFailure, (state, { error }): State => ({
    ...state,
    hidden: [],
    loaded: true,
    error,
  })),
  on(Actions.clearCentroids, (state): State => ({
    ...state,
    centroids: []
  })),
  on(Actions.stopLoading, (state): State => ({
    ...state,
    loaded: true,
    clear: false,
  }))
);
