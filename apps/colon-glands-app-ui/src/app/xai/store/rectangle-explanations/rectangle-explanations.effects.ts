import { Injectable } from '@angular/core';
import { AnnotationConversionService } from '@annotations/services/annotation-conversion.service';
import { ViewportConversionService } from '@annotations/services/viewport-conversion.service';
import { DataService } from '@api/wbs-api/services';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch, pessimisticUpdate } from '@nrwl/angular';
import { catchError, debounceTime, filter, map, switchMap } from 'rxjs/operators';
import { RectangleExplanationsFeature, ANNOTATION_QUERY_LIMIT } from '../index';
import * as RectangleExplanationsActions from './rectangle-explanations.actions';
import * as AnnotationsUiActions from '@annotations/store/annotations-ui/annotations-ui.actions';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as AnnotationsUiSelectors from '@annotations/store/annotations-ui/annotations-ui.selectors';
import * as JobsSelectors from '@jobs/store/jobs/jobs.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import * as SlidesSelectors from '@slides/store/slides/slides.selectors';
import { selectShowXaiLayerState } from '@analysis/store/xai-layer/xai-layer.selectors';
import { setShowXaiLayer } from '@analysis/store/xai-layer/xai-layer.actions';
import { dispatchActionOnErrorCode, filterNullish, retryOnAction } from '@shared/helper/rxjs-operators';
import { retryWhen } from 'rxjs/operators';
import { requestNewToken } from 'vendor-app-communication-interface';
import { RectangleExplanationEntity } from 'slide-viewer';
import { AnnotationApiOutput } from '@annotations/services/annotation-types';
import { EXAMINATION_CLOSED_ERROR_STATUS_CODE } from '@menu/models/ui.models';
import { forkJoin, of } from 'rxjs';



@Injectable()
export class RectangleExplanationViewerEffects {

  // clear annotations on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => RectangleExplanationsActions.clearAnnotations())
    );
  });

  // load annotation ids when the job list was loaded
  startLoadingAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      map(action => action.jobs.map(job => job.id)),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds).pipe(filterNullish()),
        this.store.select(selectShowXaiLayerState),
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds, isXaiLayerShown]) => isXaiLayerShown && !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds]) =>
        RectangleExplanationsActions.loadAnnotationIds({
          scopeId,
          slideId,
          jobIds,
          currentView,
        })
      )
    );
  });

  // load new annotation ids when the viewport has changed
  viewportChanged$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AnnotationsUiActions.setViewportReady),
      map(action => action.currentView),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
        this.store.select(selectShowXaiLayerState)
      ]),
      filter(([_currentView, _scopeId, _slideId, jobIds, isXaiLayerShown]) => isXaiLayerShown && !!jobIds.length),
      map(([currentView, scopeId, slideId, jobIds]) =>
        RectangleExplanationsActions.loadAnnotationIds({
          scopeId,
          slideId,
          jobIds,
          currentView,
        })
      )
    );
  });


  // reload explanation when job/show explanation gets checked or unchecked
  reloadAnnotationIdsOnJobCheck$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.showAllJobs,
        setShowXaiLayer
      ),
      debounceTime(1000),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
        this.store.select(selectShowXaiLayerState),
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds, isXaiLayerShown]) => isXaiLayerShown && !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds]) =>
        RectangleExplanationsActions.loadAnnotationIds({
          scopeId,
          slideId,
          currentView,
          jobIds,
          classValues: ['org.empaia.dai.colon_glands_attention.v3.0.classes.explanation']
        })
      )
    );
  });

  loadAnnotationIds$ = createEffect(() => {
    return this, this.actions$.pipe(
      ofType(RectangleExplanationsActions.loadAnnotationIds),
      fetch({
        id: (
          action: ReturnType<typeof RectangleExplanationsActions.loadAnnotationIds>,
          _state: RectangleExplanationsFeature.State
        ) => {
          return action.type;
        },
        run: (
          action: ReturnType<typeof RectangleExplanationsActions.loadAnnotationIds>,
          _state: RectangleExplanationsFeature.State
        ) => {
          return this.dataService.scopeIdAnnotationsQueryViewerPut({
            scope_id: action.scopeId,
            body: {
              references: [
                action.slideId
              ],
              jobs: action.jobIds.length ? action.jobIds : [null],
              viewport: action.currentView.viewport,
              npp_viewing: this.viewportConverter.calculateNppViewRange(action.currentView.nppCurrent, action.currentView.nppBase),
              class_values: ['org.empaia.dai.colon_glands_attention.v3.0.classes.explanation'],
              types: action.annotationTypes?.map(this.annotationConverter.convertAnnotationTypeToApi)
            }
          }).pipe(
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(results => {
              return RectangleExplanationsActions.loadAnnotationIdsSuccess({
                annotationIds: results.annotations,
                centroids: results.low_npp_centroids,
              })
            })
          );
        },
        onError: (_action: ReturnType<typeof RectangleExplanationsActions.loadAnnotationIds>, error) => {
          return RectangleExplanationsActions.loadAnnotationIdsFailure({ error });
        }
      })
    );
  });

  prepareLoadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RectangleExplanationsActions.loadAnnotations),
      map(action => action.annotationIds),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllCheckedJobIds),
      ]),
      filter(([annotationIds, _scopeId, jobIds]) =>
        !!annotationIds.length
        && !!jobIds.length
      ),
      map(([annotationIds, scopeId, jobIds]) =>
        RectangleExplanationsActions.loadAnnotationsReady({
          scopeId,
          annotationIds,
          jobIds,
          withClasses: true,
          skip: 0,
        })
      )
    );
  });

  loadAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RectangleExplanationsActions.loadAnnotationsReady),
      switchMap((action: ReturnType<typeof RectangleExplanationsActions.loadAnnotationsReady>) => {
        const fetchAnnotations$ = this.dataService.scopeIdAnnotationsQueryPut({
          scope_id: action.scopeId,
          with_classes: action.withClasses,
          skip: action.skip,
          limit: ANNOTATION_QUERY_LIMIT,
          body: {
            annotations: action.annotationIds,
            jobs: action.jobIds?.length ? action.jobIds : [null]
          }
        }).pipe(
          retryWhen(errors =>
            retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
          ),
          map(result => result.items.map(a => this.annotationConverter.fromApiType(a) as RectangleExplanationEntity)),
          // catchError(error => of(RectangleExplanationsActions.loadAnnotationsFailure({ error })))
        );

        const fetchFloats$ = this.dataService.scopeIdPrimitivesQueryPut(
          {
            scope_id: action.scopeId,
            skip: action.skip,
            limit: ANNOTATION_QUERY_LIMIT,
            body: {
              references: action.annotationIds,
              jobs: action.jobIds?.length ? action.jobIds : []
            }
          }).pipe(
          // map(floats => floats.items.map(el=> el.value as number)),
          // catchError(error => of(RectangleExplanationsActions.loadAnnotationsFailure({ error })))
        );

        return forkJoin([fetchAnnotations$, fetchFloats$]).pipe(
          map(([annotations, floats]) => {
            for (const annotation of annotations) {
              const id = annotation.id;
              const correspondingFloat = floats.items.find(el => el.reference_id === id);


              if (correspondingFloat)
                annotation.importance = correspondingFloat.value as number
            }
            return RectangleExplanationsActions.loadAnnotationsSuccess({ annotations });
          })
        );
      })
    );
  });

  // add focused annotation to slide-viewer cache
  addAnnotationOnFocus$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RectangleExplanationsActions.zoomToAnnotation),
      map(action => action.focus),
      filterNullish(),
      map(annotation => RectangleExplanationsActions.addAnnotation({ annotation }))
    );
  });

  // stop loading state when loading extended scope
  stopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => RectangleExplanationsActions.stopLoading())
    );
  });

  prepareLoadHiddenAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        JobsActions.setJobSelection,
        JobsActions.setJobsSelections,
        JobsActions.hideAllJobs,
      ),
      concatLatestFrom(() => [
        this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish()),
        this.store.select(SlidesSelectors.selectSelectedSlideId).pipe(filterNullish()),
        this.store.select(AnnotationsUiSelectors.selectCurrentView).pipe(filterNullish()),
        this.store.select(JobsSelectors.selectAllUncheckedJobIds).pipe(filterNullish()),
      ]),
      filter(([, _scopeId, _slideId, _currentView, jobIds]) => !!jobIds.length),
      map(([, scopeId, slideId, currentView, jobIds]) =>
        RectangleExplanationsActions.loadHiddenAnnotationIds({
          scopeId,
          slideId,
          currentView,
          jobIds
        })
      )
    );
  });

  loadHiddenAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RectangleExplanationsActions.loadHiddenAnnotationIds),
      fetch({
        run: (
          action: ReturnType<typeof RectangleExplanationsActions.loadHiddenAnnotationIds>,
          _state: RectangleExplanationsFeature.State
        ) => {
          return this.dataService.scopeIdAnnotationsQueryViewerPut({
            scope_id: action.scopeId,
            body: {
              references: [
                action.slideId
              ],
              jobs: action.jobIds.length ? action.jobIds : [null],
              viewport: action.currentView.viewport,
              npp_viewing: this.viewportConverter.calculateNppViewRange(action.currentView.nppCurrent, action.currentView.nppBase),
              class_values: ['org.empaia.dai.colon_glands_attention.v3.0.classes.explanation'],
              types: action.annotationTypes?.map(this.annotationConverter.convertAnnotationTypeToApi)
            }
          }).pipe(
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(results => results.annotations),
            map(hidden => RectangleExplanationsActions.loadHiddenAnnotationIdsSuccess({ hidden }))
          );
        },
        onError: (_action: ReturnType<typeof RectangleExplanationsActions.loadHiddenAnnotationIds>, error) => {
          return RectangleExplanationsActions.loadHiddenAnnotationIdsFailure({ error });
        }
      })
    );
  });


  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly dataService: DataService,
    private readonly annotationConverter: AnnotationConversionService,
    private readonly viewportConverter: ViewportConversionService,
  ) { }
}
