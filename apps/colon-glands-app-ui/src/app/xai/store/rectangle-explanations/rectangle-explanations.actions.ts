import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { CurrentView } from '@annotations/store/annotations-ui/annotations-ui.models';
import { Annotation, RectangleExplanationEntity, AnnotationType } from 'slide-viewer';

export const loadAnnotationIds = createAction(
  '[APP/Rectangle-Explanations] Load Annotation Ids',
  props<{
    scopeId: string,
    slideId: string,
    currentView: CurrentView,
    jobIds: string[],
    classValues?: string[] | [null],
    annotationTypes?: AnnotationType[],
  }>()
);

export const loadAnnotationIdsSuccess = createAction(
  '[APP/Rectangle-Explanations] Load Annotation Ids Success',
  props<{
    annotationIds: string[],
    centroids: number[][],
  }>()
);

export const loadAnnotationIdsFailure = createAction(
  '[APP/Rectangle-Explanations] Load Annotation Ids Failure',
  props<{ error: HttpErrorResponse }>()
);

export const loadAnnotations = createAction(
  '[APP/Rectangle-Explanations] Load Annotations',
  props<{ annotationIds: string[] }>()
);

export const loadAnnotationsReady = createAction(
  '[APP/Rectangle-Explanations] Load Annotations Ready',
  props<{
    withClasses: boolean,
    scopeId: string,
    annotationIds: string[],
    jobIds?: string[],
    skip: number,
  }>()
);

export const loadAnnotationsSuccess = createAction(
  '[APP/Rectangle-Explanations] Load Annotations Success',
  props<{ annotations: RectangleExplanationEntity[] }>()
);

export const loadAnnotationsFailure = createAction(
  '[APP/Rectangle-Explanations] Load Annotations Failure',
  props<{ error: HttpErrorResponse }>()
);

export const clearAnnotations = createAction(
  '[APP/Rectangle-Explanations] Clear Annotations'
);

export const createAnnotation = createAction(
  '[APP/Rectangle-Explanations] Create Annotation',
  props<{ annotation: Annotation }>()
);

export const createAnnotationReady = createAction(
  '[APP/Rectangle-Explanations] Create Annotation Ready',
  props<{
    annotation: Annotation,
    scopeId: string,
    slideId: string,
  }>()
);

export const createAnnotationSuccess = createAction(
  '[APP/Rectangle-Explanations] Create Annotation Success',
  props<{ annotation: RectangleExplanationEntity }>()
);

export const createAnnotationFailure = createAction(
  '[APP/Rectangle-Explanations] Create Annotation Failure',
  props<{ error: HttpErrorResponse }>()
);

export const setAnnotationTypeFilter = createAction(
  '[APP/Rectangle-Explanations] Set Annotation Type Filter',
  props<{ annotationTypes: AnnotationType[] | undefined }>()
);

export const deleteAnnotation = createAction(
  '[APP/Rectangle-Explanations] Delete Annotation',
  props<{ scopeId: string, annotationId: string }>()
);

export const deleteAnnotationSuccess = createAction(
  '[APP/Rectangle-Explanations] Delete Annotation Success',
  props<{ annotationId: string }>()
);

export const deleteAnnotationFailure = createAction(
  '[APP/Rectangle-Explanations] Delete Annotation Failure',
  props<{ error: HttpErrorResponse }>()
);

export const zoomToAnnotation = createAction(
  '[APP/Rectangle-Explanations] Zoom To Annotation',
  props<{ focus?: RectangleExplanationEntity | undefined }>()
);

export const addAnnotation = createAction(
  '[APP/Rectangle-Explanations] Add Annotation',
  props<{ annotation: RectangleExplanationEntity }>()
);

export const loadHiddenAnnotationIds = createAction(
  '[APP/Rectangle-Explanations] Load Hidden Annotation Ids',
  props<{
    scopeId: string,
    slideId: string,
    currentView: CurrentView,
    jobIds: string[],
    classValues?: string[],
    annotationTypes?: AnnotationType[],
  }>()
);

export const loadHiddenAnnotationIdsSuccess = createAction(
  '[APP/Rectangle-Explanations] Load Hidden Annotation Ids Success',
  props<{ hidden: string[] }>()
);

export const loadHiddenAnnotationIdsFailure = createAction(
  '[APP/Rectangle-Explanations] Load Hidden Annotation Ids Failure',
  props<{ error: HttpErrorResponse }>()
);

export const clearCentroids = createAction(
  '[APP/Rectangle-Explanations] Clear Centroids',
);

export const stopLoading = createAction(
  '[APP/Rectangle-Explanations] Stop Loading',
);
