import { EffectsModule } from '@ngrx/effects';
import { EXPLANATIONS_MODULE_FEATURE_KEY, reducers } from '@xai/store/explanations.state';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RectangleExplanationViewerEffects } from '@xai/store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(
      EXPLANATIONS_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      RectangleExplanationViewerEffects
    ])
  ]
})
export class XaiModule { }
