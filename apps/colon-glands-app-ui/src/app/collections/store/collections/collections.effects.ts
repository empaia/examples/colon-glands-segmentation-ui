import { filterNullish } from '@shared/helper/rxjs-operators';
import { Store } from '@ngrx/store';
import { fetch } from '@nrwl/angular';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { CollectionsFeature } from '..';
import * as CollectionsActions from './collections.actions';
import * as JobsActions from '@jobs/store/jobs/jobs.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { map } from 'rxjs/operators';
import { DataService } from '@api/wbs-api/services';



@Injectable()
export class CollectionsEffects {

  // clear collections on slide selection
  clearAnnotationsOnSlideSelection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        SlidesActions.selectSlide,
        SlidesActions.clearSlides,
      ),
      map(() => CollectionsActions.clearCollections())
    );
  });

  // Load everytime the job list was loaded
  startLoadingCollections$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(JobsActions.loadJobsSuccess),
      concatLatestFrom(() => this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())),
      map(([, scopeId]) => scopeId),
      map(scopeId => CollectionsActions.loadCollections({ scopeId }))
    );
  });

  loadCollections$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(CollectionsActions.loadCollections),
      fetch({
        run: (
          action: ReturnType<typeof CollectionsActions.loadCollections>,
          _state: CollectionsFeature.State
        ) => {
          return this.dataServie.scopeIdCollectionsQueryPut({
            scope_id: action.scopeId,
            body: {
              creators: [
                action.scopeId
              ]
            }
          }).pipe(
            map(collectionList => collectionList.items),
            map(collections => CollectionsActions.loadCollectionsSuccess({ collections }))
          );
        },
        onError: (_action: ReturnType<typeof CollectionsActions.loadCollections>, error) => {
          return CollectionsActions.loadCollectionFailure({ error });
        }
      })
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly dataServie: DataService,
    private readonly store: Store,
  ) {}
}
