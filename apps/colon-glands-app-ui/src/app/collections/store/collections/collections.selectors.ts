import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectCollectionsModuleState,
} from '../collections-feature.state';
import { collectionsAdapter } from './collections.reducer';

const {
  selectEntities
} = collectionsAdapter.getSelectors();

export const selectCollectionsState = createSelector(
  selectCollectionsModuleState,
  (state: ModuleState) => state.collections
);

export const selectCollectionEntities = createSelector(
  selectCollectionsState,
  selectEntities
);
