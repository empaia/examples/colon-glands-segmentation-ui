import { HttpErrorResponse } from '@angular/common/http';
import { Collection } from '@api/wbs-api/models';
import { createAction, props } from '@ngrx/store';

export const loadCollections = createAction(
  '[APP/Collections] Load Collections',
  props<{
    scopeId: string
  }>()
);

export const loadCollectionsSuccess = createAction(
  '[APP/Collections] Load Collections Success',
  props<{ collections: Collection[] }>()
);

export const loadCollectionFailure = createAction(
  '[APP/Collections] Load Collections Failure',
  props<{ error: HttpErrorResponse }>()
);

export const clearCollections = createAction(
  '[APP/Collections] Clear Collections',
);
