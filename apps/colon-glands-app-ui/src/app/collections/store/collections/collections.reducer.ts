import { HttpErrorResponse } from '@angular/common/http';
import { Collection } from '@api/wbs-api/models';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as CollectionsActions from './collections.actions';


export const COLLECTIONS_FEATURE_STATE = 'collections';

export interface State extends EntityState<Collection> {
  loaded: boolean,
  error?: HttpErrorResponse | null;
}

export const collectionsAdapter = createEntityAdapter<Collection>();

export const initialState: State = collectionsAdapter.getInitialState({
  loaded: true,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(CollectionsActions.loadCollections, (state): State => ({
    ...state,
    loaded: false,
  })),
  on(CollectionsActions.loadCollectionsSuccess, (state, { collections }): State =>
    collectionsAdapter.setAll(collections, {
      ...state,
      loaded: true,
    })
  ),
  on(CollectionsActions.loadCollectionFailure, (state, { error }): State => ({
    ...state,
    error,
  })),
  on(CollectionsActions.clearCollections, (state): State => ({
    ...state,
    ...initialState,
  }))
);
