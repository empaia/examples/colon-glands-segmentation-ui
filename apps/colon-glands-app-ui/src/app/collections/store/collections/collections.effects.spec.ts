import { DataService } from '@api/wbs-api/services';
import { provideMockStore } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService, mockProvider } from '@ngneat/spectator/jest';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CollectionsEffects } from './collections.effects';

describe('CollectionsEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<CollectionsEffects>;
  const createEffect = createServiceFactory({
    service: CollectionsEffects,
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      CollectionsEffects,
      provideMockActions(() => actions$),
      provideMockStore(),
      mockProvider(DataService)
    ]
  });

  beforeEach(() => spectator = createEffect());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
