import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromCollections from '@collections/store/collections/collections.reducer';

export const COLLECTIONS_MODULE_FEATURE_KEY = 'collectionsModuleFeature';

export const selectCollectionsModuleState = createFeatureSelector<State>(
  COLLECTIONS_MODULE_FEATURE_KEY
);

export interface State {
  [fromCollections.COLLECTIONS_FEATURE_STATE]: fromCollections.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromCollections.COLLECTIONS_FEATURE_STATE]: fromCollections.reducer,
  })(state, action);
}
