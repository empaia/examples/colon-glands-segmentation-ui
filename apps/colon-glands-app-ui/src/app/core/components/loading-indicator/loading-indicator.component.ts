import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

type Position = 'before' | 'after';

@Component({
  selector: 'app-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingIndicatorComponent {
  @Input() public loaded!: boolean;
  @Input() public message!: string;
  @Input() public position: Position = 'after';
}
