import { MaterialModule } from '@material/material.module';
import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { LoadingIndicatorComponent } from './loading-indicator.component';

describe('LoadingIndicatorComponent', () => {
  let spectator: Spectator<LoadingIndicatorComponent>;
  const createComponent = createComponentFactory({
    component: LoadingIndicatorComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create the app', () => {
    expect(spectator.component).toBeDefined();
  });
});
