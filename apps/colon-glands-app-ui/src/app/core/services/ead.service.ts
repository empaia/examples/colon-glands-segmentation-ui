/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import {
  EadInputType,
  EadInputTypeAnnotation,
  EmpaiaAppDescriptionV1,
  EmpaiaAppDescriptionV3,
  EmpaiaAppDescriptionV3Modes,
  InputKey,
  annotationTypes,
  isCollection,
} from '@core/models/ead.models';

@Injectable({
  providedIn: 'root'
})
export class EadService {
  public isV1Ead(ead: object): ead is EmpaiaAppDescriptionV1 {
    return 'inputs' in ead;
  }

  public isV3Ead(ead: object): ead is EmpaiaAppDescriptionV3 {
    return 'io' in ead;
  }

  public getV1Inputs(ead: EmpaiaAppDescriptionV1): string[] {
    return Object.keys(ead.inputs);
  }

  public getV1WsiInputKey(ead: EmpaiaAppDescriptionV1): string | undefined {
    return this.searchForType('wsi', ead.inputs);
  }

  public getV1AnnotationsInputKey(ead: EmpaiaAppDescriptionV1): InputKey | undefined {
    return this.searchForTypeWithCollection('rectangle', ead.inputs);
  }

  public getV3Inputs(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Modes): string[] {
    return ead.modes[mode]?.inputs as string[];
  }

  public getV3Outputs(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Modes): string[] {
    return ead.modes[mode]?.outputs as string[];
  }

  public getV3WsiInputKey(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Modes): string | undefined {
    return this.getV3TypeInputKey('wsi', ead, mode)?.inputKey;
  }

  public getV3AnnotationsInputKey(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Modes): InputKey | undefined {
    return this.getV3TypeInputKey('rectangle', ead, mode);
  }

  public getV3TypeInputKey(type: EadInputType, ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Modes): InputKey | undefined {
    const keys = this.getV3Inputs(ead, mode);

    for (const key of keys) {
      const result = this.searchForTypeWithCollectionAndkey(type, key, ead.io);
      if (result) {
        return result;
      }
    }

    return undefined;
  }

  public getV3AnnotationInputTypes(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Modes): EadInputTypeAnnotation[] {
    const types = annotationTypes.filter(type => this.getV3TypeInputKey(type, ead, mode));

    return types;
  }

  public isV3CompatibleWithInputType(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Modes, type: EadInputType): boolean {
    return !!this.getV3TypeInputKey(type, ead, mode);
  }

  public hasAnyV3CompatibleInputType(ead: EmpaiaAppDescriptionV3, mode: EmpaiaAppDescriptionV3Modes, types: EadInputType[]): boolean {
    return types.some(type => this.isV3CompatibleWithInputType(ead, mode, type));
  }

  private searchForType(type: EadInputType, inputs?: object): string | undefined {
    if (inputs) {
      for (const [key ,value] of Object.entries(inputs)) {
        if (value['type'] === type) {
          return key;
        }
      }
    }

    return undefined;
  }

  private searchForTypeWithCollection(type: EadInputType, inputs?: object): InputKey | undefined {
    if (inputs) {
      for (const [key, value] of Object.entries(inputs)) {
        if (value['type'] === type) {
          return { inputKey: key, inCollection: 0 };
        } else if (isCollection(value['type'])) {
          const found = this.searchForTypeWithCollection(type, value);
          if (found) {
            found.inputKey = key;
            found.inCollection += 1;
            return found;
          }
        }
      }
    }

    return undefined;
  }

  private searchForTypeWithCollectionAndkey(type: EadInputType, key: string, inputs?: any): InputKey | undefined {
    if (inputs && inputs[key]) {
      const input = inputs[key];
      if (input['type'] === type) {
        return { inputKey: key, inCollection: 0 };
      } else if (isCollection(input['type'])) {
        const found = this.searchForTypeWithCollection(type, input);
        if (found) {
          found.inputKey = key;
          found.inCollection += 1;
          return found;
        }
      }
    }

    return undefined;
  }
}
