import { Component, NgZone, OnInit } from '@angular/core';
import { AnnotationsActions, AnnotationsSelectors, AnnotationsUiActions, AnnotationsUiSelectors, AnnotationsViewerActions, AnnotationsViewerSelectors } from '@annotations/store';
import { RectangleExplanationsActions, RectangleExplanationsSelectors } from '@xai/store';
import { AnnotationClassColor, ClassColorsActions, ClassesSelectors } from '@classes/store';
import { MenuActions, MenuSelectors } from '@menu/store';
import { Store } from '@ngrx/store';
import { ScopeActions, ScopeSelectors } from '@scope/store';
import { SlidesSelectors } from '@slides/store';
import { TokenActions } from '@token/store';
import { WbsUrlActions } from '@wbs-url/store';
import { asyncScheduler } from 'rxjs';
import { observeOn } from 'rxjs/operators';
import { Annotation, AnnotationHighlightConfig, CurrentView, HighlightType, UiConfig } from 'slide-viewer';
import { addScopeListener, addTokenListener, addWbsUrlListener, Scope, Token, WbsUrl } from 'vendor-app-communication-interface';
import { XaiLayersSelectors } from '@analysis/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public slide$ = this.store.select(SlidesSelectors.selectSelectedSlideImageView);
  public selectedSlideId$ = this.store.select(SlidesSelectors.selectSelectedSlideId);
  public annotationIds$ = this.store.select(AnnotationsViewerSelectors.selectAllAnnotationViewerIds);
  public annotations$ = this.store.select(AnnotationsViewerSelectors.selectAllViewerAnnotations);
  public annotationLoaded$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationsViewerLoaded).pipe(
    observeOn(asyncScheduler)
  );
  public selectedAnnotationId$ = this.store.select(AnnotationsSelectors.selectSelectedAnnotationId);
  public centroids$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationsViewerCentroids);
  public allowedInteractionTypes$ = this.store.select(AnnotationsUiSelectors.selectAllowedInteractionTypes);
  public selectedInteractionType$ = this.store.select(AnnotationsUiSelectors.selectInteractionType);
  public focusAnnotation$ = this.store.select(AnnotationsViewerSelectors.selectFocusedViewerAnnotationId);
  public hiddenAnnotationIds$ = this.store.select(AnnotationsViewerSelectors.selectHiddenAnnotationViewerIds);
  public removeAnnotations$ = this.store.select(AnnotationsViewerSelectors.selectRemovedAnnotationViewerIds);
  public clearAnnotations$ = this.store.select(AnnotationsViewerSelectors.selectAnnotationViewerClearState);
  public allClassEntities$ = this.store.select(ClassesSelectors.selectAllClasses);
  public examinationState$ = this.store.select(ScopeSelectors.selectExaminationState);
  public menuSize$ = this.store.select(MenuSelectors.selectMenuSize);

  public xaiAlpha$ = this.store.select(XaiLayersSelectors.selectAlphaState);
  public showXaiLayer$ = this.store.select(XaiLayersSelectors.selectShowXaiLayerState);
  private showXaiLayer = false;

  public rectangleExplanationIds$ = this.store.select(RectangleExplanationsSelectors.selectAllAnnotationViewerIds);
  public rectangleExplanations$ = this.store.select(RectangleExplanationsSelectors.selectAllViewerAnnotations);
  // public rectangleExplanationLoaded$ = this.store.select(RectangleExplanationsSelectors.selectAnnotationsViewerLoaded).pipe(
  //   observeOn(asyncScheduler)
  // );
  // public selectedRectangleExplanationId$ = this.store.select(AnnotationsSelectors.selectSelectedAnnotationId);
  public hiddenRectangleExplanationIds$ = this.store.select(RectangleExplanationsSelectors.selectHiddenAnnotationViewerIds);
  public removeRectangleExplanations$ = this.store.select(RectangleExplanationsSelectors.selectRemovedAnnotationViewerIds);
  public clearRectangleExplanations$ = this.store.select(RectangleExplanationsSelectors.selectAnnotationViewerClearState);


  public uiConfig: UiConfig = {
    showAnnotationBar: true,
    autoSetAnnotationTitle: false,
    renderHideNavigationButton: true,
  };

  public annotationHighlightConfig: AnnotationHighlightConfig = {
    highlightType: HighlightType.CLICK
  };

  public readonly CLUSTER_DISTANCE = 60;

  constructor(
    private store: Store,
    private ngZone: NgZone
  ) {
  }

  public ngOnInit(): void {
    addTokenListener((token: Token) => {
      this.ngZone.run(() => {
        this.store.dispatch(TokenActions.setAccessToken({ token }));
      });
    });

    addScopeListener((scope: Scope) => {
      this.ngZone.run(() => {
        this.store.dispatch(ScopeActions.setScope({ scopeId: scope.id }));
      });
    });

    addWbsUrlListener((wbsUrl: WbsUrl) => {
      this.ngZone.run(() => {
        this.store.dispatch(WbsUrlActions.setWbsUrl({ wbsUrl }));
      });
    });
  }

  public toggleMenuVisibility(): void {
    this.store.dispatch(MenuActions.toggleMenu());
  }

  public requestAnnotations(annotationIds: string[]): void {
    this.store.dispatch(AnnotationsViewerActions.loadAnnotations({ annotationIds }));
  }

  public onViewerMoveEnd(currentView: CurrentView): void {
    this.store.dispatch(AnnotationsUiActions.setViewport({ currentView }));
  }

  public onAnnotationCreated(annotation: Annotation): void {
    this.store.dispatch(AnnotationsViewerActions.createAnnotation({ annotation }));
  }

  public onAnnotationClassColorsChanged(annotationClassColors: AnnotationClassColor): void {
    this.store.dispatch(ClassColorsActions.storeClassColorMapping({ colorMapping: annotationClassColors }));
  }

  public onAnnotationClicked(ids: string[]): void {
    this.store.dispatch(AnnotationsActions.selectAnnotation({ selected: ids[0] }));
  }

  public requestRectangleExplanations(annotationIds: string[]): void {
    this.store.dispatch(RectangleExplanationsActions.loadAnnotations({ annotationIds }));
  }
}
