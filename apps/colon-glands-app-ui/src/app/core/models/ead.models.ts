export interface EmpaiaAppDescriptionCore {
  $shema: string;
  name: string;
  name_short: string;
  namespace: string;
  description: string;
}

export interface EmpaiaAppDescriptionV1 extends EmpaiaAppDescriptionCore {
  inputs: object;
  outputs: object;
}

export interface EmpaiaAppDescriptionV3 extends EmpaiaAppDescriptionCore {
  io: object;
  modes: Modes;
}

export interface Modes {
  standalone?: ModeIOs;
  preprocessing?: ModeIOs;
  postprocessing?: ModeIOContainer;
}

export interface ModeIOs {
  inputs: string[];
  outputs: string[];
}

export interface ModeIOContainer extends ModeIOs {
  containerized: boolean;
}

export enum EmpaiaAppDescriptionVersion {
  VERSION_1,
  VERSION_3,
}

export interface InputKey {
  inputKey: string;
  inCollection: number;
}

export type EmpaiaAppDescriptionV3Modes = 'standalone' | 'preprocessing' | 'postprocessing';

export type EadInputTypeWsi = 'wsi';
export type EadInputTypeCollection = 'collection';
export type EadInputTypeAnnotation = 'rectangle' | 'polygon' | 'circle'; // TODO: add more later

export type EadInputType = EadInputTypeWsi | EadInputTypeCollection | EadInputTypeAnnotation;

export function isWsi(object: string): object is EadInputTypeWsi {
  return object === 'wsi';
}

export function isCollection(object: string): object is EadInputTypeCollection {
  return object === 'collection';
}

export const annotationTypes: EadInputTypeAnnotation[] = ['rectangle', 'polygon', 'circle'];
export function isAnnotation(object: string): object is EadInputTypeAnnotation {
  return annotationTypes.includes(object as EadInputTypeAnnotation);
}

export function getEmpaiaAppDescriptionVersion(ead: object): EmpaiaAppDescriptionVersion {
  return 'io' in ead && 'modes' in ead ? EmpaiaAppDescriptionVersion.VERSION_3 : EmpaiaAppDescriptionVersion.VERSION_1;
}
