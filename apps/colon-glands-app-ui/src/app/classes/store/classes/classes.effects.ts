import { Injectable } from '@angular/core';
import { DataService } from '@api/wbs-api/services';
import { ClassNameConversionService } from '@classes/services/class-name-conversion.service';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@nrwl/angular';
import { filterNullish } from '@shared/helper/rxjs-operators';
import { filter, map } from 'rxjs/operators';
import { ClassesFeature } from '..';
import * as ClassesActions from './classes.actions';
import * as ScopeActions from '@scope/store/scope/scope.actions';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';



@Injectable()
export class ClassesEffects {

  prepareLoadClassNamespaces$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScopeSuccess),
      concatLatestFrom(() => this.store.select(ScopeSelectors.selectScopeId).pipe(filterNullish())),
      map(([, scopeId]) => scopeId),
      map(scopeId => ClassesActions.loadClassNamespaces({ scopeId }))
    );
  });

  loadClassNamespaces$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.loadClassNamespaces),
      fetch({
        run: (
          action: ReturnType<typeof ClassesActions.loadClassNamespaces>,
          _state: ClassesFeature.State
        ) => {
          return this.dataService.scopeIdClassNamespacesGet({
            scope_id: action.scopeId
          }).pipe(
            map(response => this.classNameConverter.getClasses(response)),
            map(classes => ClassesActions.loadClassNamespacesSuccess({ classes }))
          );
        },
        onError: (
          _action: ReturnType<typeof ClassesActions.loadClassNamespaces>,
          error
        ) => {
          return ClassesActions.loadClassNamespacesFailure({ error });
        }
      })
    );
  });

  clearClassesEad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScopeActions.loadExtendedScope),
      map(() => ClassesActions.clearClassesEAD())
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly dataService: DataService,
    private readonly classNameConverter: ClassNameConversionService,
    private readonly store: Store,
  ) { }
}
