import { Observable } from 'rxjs';

import { ClassesEffects } from '@classes/store';
import { createServiceFactory, mockProvider, SpectatorService } from '@ngneat/spectator';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DataService } from '@api/wbs-api/services';
import { ClassNameConversionService } from '@classes/services/class-name-conversion.service';

describe('ClassesEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<ClassesEffects>;
  const createService = createServiceFactory({
    service: ClassesEffects,
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      ClassesEffects,
      provideMockActions(() => actions$),
      provideMockStore(),
      mockProvider(DataService),
      mockProvider(ClassNameConversionService),
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
