import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectClassesFeatureState,
} from '../classes-feature.state';
import { classesAdapter, classSelectorAdapter } from './classes.reducer';

const {
  selectAll
} = classesAdapter.getSelectors();

export const selectClassesState = createSelector(
  selectClassesFeatureState,
  (state: ModuleState) => state.classes
);

export const selectAllClasses = createSelector(
  selectClassesState,
  selectAll,
);

export const selectAllCheckedClasses = createSelector(
  selectClassesState,
  (state) => classSelectorAdapter.getSelectors().selectAll(state.selectedClasses).filter(c => c.checked && c.id !== 'org.empaia.dai.colon_glands_attention.v3.0.classes.explanation').map(c => c.id)
);

export const selectClassSelectionEntity = createSelector(
  selectClassesState,
  (state) => classSelectorAdapter.getSelectors().selectEntities(state.selectedClasses)
);
