import { ClassEntity, ClassSelector } from './classes.models';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as ClassesActions from './classes.actions';
import { upsertManyWithoutUpdate } from '@shared/helper/ngrx-operators';


export const CLASSES_FEATURE_KEY = 'classes';

export interface State extends EntityState<ClassEntity> {
  selectedClasses: EntityState<ClassSelector>;
  allSelected: boolean;
  loaded: boolean;
}

export const classesAdapter = createEntityAdapter<ClassEntity>();
export const classSelectorAdapter = createEntityAdapter<ClassSelector>();

export const initialState: State = classesAdapter.getInitialState({
  selectedClasses: classSelectorAdapter.getInitialState(),
  allSelected: true,
  loaded: true,
});

export const reducer = createReducer(
  initialState,
  on(ClassesActions.loadClassNamespacesSuccess, (state, { classes }): State =>
    classesAdapter.setAll(classes, {
      ...state,
      loaded: true,
      selectedClasses: upsertManyWithoutUpdate(classSelectorAdapter, convertClassesToClassSelectors(classes, true), state.selectedClasses)
    })
  ),
  on(ClassesActions.clearClassesEAD, (state) =>
    classesAdapter.removeAll({
      ...state,
      selectedClasses: classSelectorAdapter.removeAll({
        ...state.selectedClasses
      })
    })
  ),
  on(ClassesActions.setClassSelection, (state, { classSelection }): State => ({
    ...state,
    selectedClasses: classSelectorAdapter.upsertOne(classSelection, {
      ...state.selectedClasses
    })
  })),
  on(ClassesActions.setClassesSelections, (state, { classesSelections }): State => ({
    ...state,
    selectedClasses: classSelectorAdapter.upsertMany(classesSelections, {
      ...state.selectedClasses
    })
  })),
);

function convertClassToClassSelector(classEntity: ClassEntity, checked: boolean): ClassSelector {
  return { id: classEntity.id, checked };
}

function convertClassesToClassSelectors(classEntities: ClassEntity[], checked: boolean): ClassSelector[] {
  return classEntities.map(c => convertClassToClassSelector(c, checked));
}
