import * as fromClasses from './classes.actions';

describe('setClassEAD', () => {
  it('should return an action', () => {
    expect(fromClasses.setClassesEAD({
      classes: []
    }).type).toBe('[Classes] Set Classes EAD');
  });
});
