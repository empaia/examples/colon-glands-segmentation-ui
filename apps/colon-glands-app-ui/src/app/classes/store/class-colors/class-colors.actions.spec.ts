import * as fromClassColors from './class-colors.actions';

describe('setClassColors', () => {
  it('should return an action', () => {
    expect(fromClassColors.storeClassColorMapping({
      colorMapping: {}
    }).type).toBe('[Class-Colors] Store Class Color Mapping');
  });
});
