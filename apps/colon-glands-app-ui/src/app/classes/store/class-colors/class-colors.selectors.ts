import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectClassesFeatureState,
} from '../classes-feature.state';
import { classColorsAdapter } from './class-colors.reducer';

const {
  selectEntities
} = classColorsAdapter.getSelectors();

export const selectClassColorState = createSelector(
  selectClassesFeatureState,
  (state: ModuleState) => state.classColors
);

export const selectAllClassColors = createSelector(
  selectClassColorState,
  selectEntities
);
