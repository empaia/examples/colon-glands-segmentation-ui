export interface ClassColorEntity {
  id: string;
  color: string;
}

export interface AnnotationClassColor {
  [key: string]: string;
}
