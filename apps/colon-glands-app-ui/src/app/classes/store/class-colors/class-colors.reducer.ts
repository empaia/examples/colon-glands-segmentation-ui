import { ClassColorEntity } from './class-colors.models';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as ClassColorsActions from './class-colors.actions';


export const CLASS_COLORS_FEATURE_KEY = 'classColors';

export type State = EntityState<ClassColorEntity>;

export const classColorsAdapter = createEntityAdapter<ClassColorEntity>();

export const initialState: State = classColorsAdapter.getInitialState();

export const reducer = createReducer(
  initialState,
  on(ClassColorsActions.storeClassColorMapping, (state, { colorMapping }): State =>
    classColorsAdapter.setAll(Object.entries(colorMapping).map(entry => ({ id: entry[0], color: entry[1] })), {
      ...state
    })
  ),
);
