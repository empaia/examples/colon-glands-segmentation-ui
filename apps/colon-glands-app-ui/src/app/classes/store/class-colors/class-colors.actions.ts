import { createAction, props } from '@ngrx/store';
import { AnnotationClassColor } from './class-colors.models';

export const storeClassColorMapping = createAction(
  '[Class-Colors] Store Class Color Mapping',
  props<{ colorMapping: AnnotationClassColor }>()
);
