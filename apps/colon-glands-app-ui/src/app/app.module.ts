import { CollectionsModule } from '@collections/collections.module';
import { AuthenticationInterceptor } from '@core/services/interceptors/authentication.interceptor';
import { CoreModule } from '@core/core.module';
import { JobsModule } from '@jobs/jobs.module';
import { SlidesModule } from '@slides/slides.module';
import { AnnotationsModule } from '@annotations/annotations.module';
import { ClassesModule } from '@classes/classes.module';
import { MenuModule } from '@menu/menu.module';
import { SharedModule } from './shared/shared.module';
import { ScopeModule } from '@scope/scope.module';
import { TokenModule } from '@token/token.module';
import { WbsUrlModule } from '@wbs-url/wbs-url.module';
import { MaterialModule } from '@material/material.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../environments/environment';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { ApiModule as WbsApiModule } from '@api/wbs-api/api.module';
import { AppComponent } from '@core/containers/app.component';
import { AnalysisModule } from '@analysis/analysis.module';
import { XaiModule } from '@xai/xai.module';

// The app now by it's own with wbs api version to use
const API_VERSION = 'v3';

export const metaReducers: MetaReducer<object>[] = !environment.production
  ? [storeFreeze]
  : [];

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot({},
      {
        metaReducers,
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
          strictActionWithinNgZone: true
        }
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument({
      name: "Generic-App-Ui Store"
    }) : [],
    AnalysisModule,
    AnnotationsModule,
    ClassesModule,
    CollectionsModule,
    CoreModule,
    JobsModule,
    MaterialModule,
    MenuModule,
    ScopeModule,
    SharedModule,
    SlidesModule,
    TokenModule,
    WbsApiModule,
    WbsUrlModule,
    XaiModule,
  ],
  providers: [
    { provide: 'API_VERSION', useValue: API_VERSION },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
