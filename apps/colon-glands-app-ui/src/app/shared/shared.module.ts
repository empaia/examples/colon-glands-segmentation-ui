import { ErrorSnackbarComponent } from './components/error-snackbar/error-snackbar.component';
import { MaterialModule } from '@material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DictParserPipe } from './pipes/dict-parser.pipe';
import { LocalSafeUrlPipe } from './pipes/local-safe-url.pipe';
import { StringSlicerPipe } from './pipes/string-slicer.pipe';
import { CopyToClipboardDirective } from './directives/copy-to-clipboard.directive';
import { CloseButtonComponent } from './components/close-button/close-button.component';
import { IconCheckboxComponent } from './components/icon-checkbox/icon-checkbox.component';
import { JobStatusComponent } from './components/job-status/job-status.component';
import { LoadingComponent } from './components/loading/loading.component';
import { MenuButtonComponent } from './components/menu-button/menu-button.component';
import { MenuLabelComponent } from './components/menu-label/menu-label.component';
import { MissingSelectionErrorComponent } from './components/missing-selection-error/missing-selection-error.component';
import { MenuItemLayoutContainerComponent } from './containers/menu-item-layout-container/menu-item-layout-container.component';

const COMPONENTS = [
  CloseButtonComponent,
  IconCheckboxComponent,
  JobStatusComponent,
  LoadingComponent,
  MenuButtonComponent,
  MenuLabelComponent,
  MissingSelectionErrorComponent,
  MenuItemLayoutContainerComponent,
  ErrorSnackbarComponent
];

const PIPES = [
  DictParserPipe,
  LocalSafeUrlPipe,
  StringSlicerPipe,
];

const DIRECTIVES = [
  CopyToClipboardDirective,
];

@NgModule({
  declarations: [
    COMPONENTS,
    PIPES,
    DIRECTIVES,
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    COMPONENTS,
    PIPES,
    DIRECTIVES,
  ]
})
export class SharedModule { }
