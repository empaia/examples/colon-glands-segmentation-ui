import { Observable, of, OperatorFunction, pipe, throwError, UnaryFunction } from 'rxjs';
import { concatMap, delayWhen, filter, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { FORBIDDEN_ERROR_STATUS_CODE, VALIDATION_ERROR_STATUS_CODE } from '@menu/models/ui.models';
import { Actions, ofType } from '@ngrx/effects';
import { ActionCreator } from '@ngrx/store';

// filter nullish values and infer type
export function filterNullish<T>(): UnaryFunction<Observable<T | null | undefined>, Observable<T>> {
  return pipe(
    filter(x => x != null) as OperatorFunction<T | null | undefined, T>
  );
}

export function retryOnAction(
  errors: Observable<unknown>,
  actions: Actions,
  action: ActionCreator,
  callback: () => void
): Observable<unknown> {
  return errors.pipe(
    // check if error is of status code 401 (token expired) or
    // 403 (forbidden, no token is set)
    // otherwise rethrow the error to handel it in
    // a different ErrorHandler
    concatMap(error =>
      (
        error instanceof HttpErrorResponse
        && error.status === VALIDATION_ERROR_STATUS_CODE
        || error instanceof HttpErrorResponse
        && error.status === FORBIDDEN_ERROR_STATUS_CODE
      )
        ? of(null)
        : throwError(error)
    ),
    map(() => callback()),
    delayWhen(() => actions.pipe(
      ofType(action)
    ))
  );
}

export function dispatchActionOnErrorCode(
  error: unknown,
  errorCode: number,
  action: unknown,
): Observable<ActionCreator> {
  return error instanceof HttpErrorResponse && error.status === errorCode
    ? of(action as ActionCreator)
    : throwError(error);
}

export function compareDistinct<T>(prev: T[], curr:T[]): boolean {
  return prev.length === curr.length && prev.every((val, index) => val === curr[index]);
}
