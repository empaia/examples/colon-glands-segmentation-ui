import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { MissingSelectionErrorComponent } from './missing-selection-error.component';

describe('MissingSelectionErrorComponent', () => {
  let spectator: Spectator<MissingSelectionErrorComponent>;
  const createComponent = createComponentFactory({
    component: MissingSelectionErrorComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
