import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { SelectionError } from '@menu/models/ui.models';
import { MenuType } from '@menu/models/menu.models';

@Component({
  selector: 'app-missing-selection-error',
  templateUrl: './missing-selection-error.component.html',
  styleUrls: ['./missing-selection-error.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MissingSelectionErrorComponent {
  @Input() missingSelectionType!: SelectionError;
  @Output() errorClicked = new EventEmitter<MenuType>();
}
