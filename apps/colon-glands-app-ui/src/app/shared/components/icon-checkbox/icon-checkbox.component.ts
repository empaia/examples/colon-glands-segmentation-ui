import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-icon-checkbox',
  templateUrl: './icon-checkbox.component.html',
  styleUrls: ['./icon-checkbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconCheckboxComponent {
  @Input() selected!: boolean;
  @Input() selectedIcon = 'visibility';
  @Input() selectedTooltip = 'Click to unselect all results';
  @Input() unselectedIcon = 'visibility_off';
  @Input() unselectedTooltip = 'Click to select all results';
  @Input() iconCheckboxClass!: string;
  @Input() iconCheckboxStyle!: string;
  @Input() disabled = false;

  @Output() selectionChanged = new EventEmitter<boolean>();
}
