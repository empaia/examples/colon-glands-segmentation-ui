import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { IconCheckboxComponent } from './icon-checkbox.component';

describe('IconCheckboxComponent', () => {
  let spectator: Spectator<IconCheckboxComponent>;
  const createComponent = createComponentFactory({
    component: IconCheckboxComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
