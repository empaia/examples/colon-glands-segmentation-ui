import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-close-button',
  templateUrl: './close-button.component.html',
  styleUrls: ['./close-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseButtonComponent {
  @Input() public disabled!: boolean;
  @Input() public icon?: string;

  @Output() public clickedClose = new EventEmitter<void>();

  public onCloseClick(event: MouseEvent): void {
    this.clickedClose.emit();
    event.stopImmediatePropagation();
  }
}
