import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { CloseButtonComponent } from './close-button.component';

describe('CloseButtonComponent', () => {
  let spectator: Spectator<CloseButtonComponent>;
  const createComponent = createComponentFactory({
    component: CloseButtonComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
