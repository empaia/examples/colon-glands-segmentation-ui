import { MaterialModule } from '@material/material.module';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { JobStatusComponent } from './job-status.component';

describe('JobStatusComponent', () => {
  let spectator: Spectator<JobStatusComponent>;
  const createComponent = createComponentFactory({
    component: JobStatusComponent,
    imports: [
      MaterialModule,
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
