import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-menu-label',
  templateUrl: './menu-label.component.html',
  styleUrls: ['./menu-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuLabelComponent {
  @Input() selected!: boolean;
  @Input() loaded = true;
  @Input() enableMenuLabelTopShadow = false;

  getClass(): string {
    let cssClass = this.selected ? 'mat-primary-bg' : 'mat-button-bg';
    if (this.enableMenuLabelTopShadow) {
      cssClass += ' menu-label-top-shadow';
    }
    return cssClass;
  }
}
