import { MatSnackBar } from '@angular/material/snack-bar';
import { Directive, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[appCopyToClipboard]'
})
export class CopyToClipboardDirective<T> {
  public key: string | undefined;

  @Input('appCopyToClipboard')
  public content!: T;

  @Output()
  public copied = new EventEmitter<string>();

  constructor(private snackBar: MatSnackBar) { }

  @HostListener('click', ['$event'])
  public onClick(event: MouseEvent): void {
    if (this.key && this.content) {
      event.preventDefault();

      if (this.key === 'i') {
        // user requests id of selected item
        const id = this.content['id'] ?? (this.content['item'].id ?? undefined);
        if (id) {
          navigator.clipboard.writeText(id).then().catch(e => console.error(e));
          this.notify('ID');
        }
      } else if (this.key === 'j') {
        // user requests selected item as json string
        navigator.clipboard.writeText(
          JSON.stringify(this.content, null, 2)
        ).then().catch(e => console.error(e));
        this.notify('JSON-Object');
      }

      event.stopPropagation();
      event.stopImmediatePropagation();
    }
  }

  private notify(text: string): void {
    this.snackBar.open(`${text} was copied to clipboard`, 'Ok', {
      duration: 500
    });
  }

  @HostListener('document:keydown', ['$event'])
  public keyDown(event: KeyboardEvent): void {
    // filter alt key because user could press alt + tab
    // in this case alt would be stored in 'this.key'
    // and only be removed when another key was pressed
    // because the browser is not focused anymore keyup
    // of alt will never be registrated
    this.key = event.key !== 'Alt' ? event.key : undefined;
  }

  @HostListener('document:keyup', ['$event'])
  public keyUp(_event: KeyboardEvent): void {
    this.key = undefined;
  }
}
