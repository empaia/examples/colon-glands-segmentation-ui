import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringSlicer'
})
export class StringSlicerPipe implements PipeTransform {

  transform(value: string, maxStringLength = 20, completeWord = false, trailing = '...'): string {
    if (value !== undefined && value !== null) {
      if (completeWord) {
        maxStringLength = value.substring(0, maxStringLength).lastIndexOf(' ');
      }
      return value.length > maxStringLength ? value.substring(0, maxStringLength) + trailing : value;
    }
    return '';
  }

}
