import { provideMockStore } from '@ngrx/store/testing';
import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { MenuItemLayoutContainerComponent } from './menu-item-layout-container.component';
import { MenuSelectors } from '@menu/store';

describe('MenuItemLayoutContainerComponent', () => {
  let spectator: Spectator<MenuItemLayoutContainerComponent>;
  const createComponent = createComponentFactory({
    component: MenuItemLayoutContainerComponent,
    providers: [
      provideMockStore({
        selectors: [
          {
            selector: MenuSelectors.selectMenuVisibilityState,
            value: true
          }
        ]
      })
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
