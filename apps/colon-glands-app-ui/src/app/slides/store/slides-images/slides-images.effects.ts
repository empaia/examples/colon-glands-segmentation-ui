import { Injectable } from '@angular/core';
import { SlidesService } from '@api/wbs-api/services';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { fetch } from '@nrwl/angular';
import { retryOnAction } from '@shared/helper/rxjs-operators';
import { concatMap, map, mergeMap, retryWhen } from 'rxjs/operators';
import { MAX_SLIDE_THUMBNAIL_HEIGHT, MAX_SLIDE_THUMBNAIL_WIDTH, SlideImage, SlideImageFormat, SlideImageStatus, SlidesImagesFeature, SLIDE_THUMBNAIL_QUALITY } from '..';
import * as SlidesImagesActions from './slides-images.actions';
import * as SlidesActions from '@slides/store/slides/slides.actions';
import * as TokenActions from '@token/store/token/token.actions';
import * as SlidesImagesSelectors from './slides-images.selectors';
import * as ScopeSelectors from '@scope/store/scope/scope.selectors';
import { requestNewToken } from 'vendor-app-communication-interface';



@Injectable()
export class SlidesImagesEffects {

  // prepare clear slide image. Revoke all stored urls to set the
  // allocated memory free
  prepareClearImages$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.prepareClearImages),
      concatLatestFrom(() => this.store.select(SlidesImagesSelectors.selectAllSlidesImages)),
      map(([_action, images]) => {
        images.forEach(img => {
          if (img.label) {
            URL.revokeObjectURL(img.label);
          }
          if (img.thumbnail) {
            URL.revokeObjectURL(img.thumbnail);
          }
        });
        return SlidesImagesActions.clearAllImages();
      }),
    );
  });

  // start loading labels and thumbnails, if slides are loaded
  startLoadingImages$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesActions.loadSlidesSuccess),
      map(action => action.slides.map(slide => slide.id)),
      map(slideIds =>
        SlidesImagesActions.loadManySlidesLabelsAndThumbnails({
          slideIds,
          maxWidth: MAX_SLIDE_THUMBNAIL_WIDTH,
          maxHeight: MAX_SLIDE_THUMBNAIL_HEIGHT,
          imageFormat: SlideImageFormat.JPEG,
          imageQuality: SLIDE_THUMBNAIL_QUALITY
        })
      )
    );
  });

  loadSlideThumbnail$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadSlideThumbnail),
      fetch({
        run: (
          action: ReturnType<typeof SlidesImagesActions.loadSlideThumbnail>,
          _state: SlidesImagesFeature.State
        ) => {
          return this.slidesService.scopeIdSlidesSlideIdThumbnailMaxSizeMaxXMaxYGet({
            scope_id: action.scopeId,
            slide_id: action.slideId,
            max_x: action.maxWidth,
            max_y: action.maxHeight,
            image_format: action.imageFormat,
            image_quality: action.imageQuality,
          }).pipe(
            // retry to load the selected slide after the token is expired
            retryWhen(errors =>
              retryOnAction(errors, this.actions$, TokenActions.setAccessToken, requestNewToken)
            ),
            map(img => URL.createObjectURL(img)),
            map(img => {
              const slideImage: SlideImage = {
                id: action.slideId,
                thumbnailStatus: SlideImageStatus.LOADED,
                thumbnail: img,
              };
              return slideImage;
            }),
            map(slideImage => SlidesImagesActions.loadSlideThumbnailSuccess({ slideImage }))
          );
        },
        onError: (action: ReturnType<typeof SlidesImagesActions.loadSlideThumbnail>, error) => {
          return SlidesImagesActions.loadSlideThumbnailFailure({ slideId: action.slideId, error });
        }
      })
    );
  });

  loadManySlidesLabels$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesLabels),
      mergeMap(action =>
        action.slideIds.filter(slideId => !!slideId).map(slideId => SlidesImagesActions.loadSlideLabel({
          slideId,
          scopeId: action.scopeId,
          maxWidth: action.maxWidth,
          maxHeight: action.maxHeight,
          imageFormat: action.imageFormat,
          imageQuality: action.imageQuality
        }))
      )
    );
  });

  loadManySlidesThumbnails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesThumbnails),
      mergeMap(action =>
        action.slideIds.filter(slideId => !!slideId).map(slideId => SlidesImagesActions.loadSlideThumbnail({
          slideId,
          scopeId: action.scopeId,
          maxWidth: action.maxWidth,
          maxHeight: action.maxHeight,
          imageFormat: action.imageFormat,
          imageQuality: action.imageQuality
        }))
      )
    );
  });

  loadManySlidesLabelsAndThumbnails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(SlidesImagesActions.loadManySlidesLabelsAndThumbnails),
      concatLatestFrom(() =>
        this.store.select(ScopeSelectors.selectScopeId)
      ),
      concatMap(([action, scopeId]) => [
        SlidesImagesActions.loadManySlidesLabels({
          ...action,
          scopeId: scopeId as string,
        }),
        SlidesImagesActions.loadManySlidesThumbnails({
          ...action,
          scopeId: scopeId as string,
        })
      ])
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
    private readonly slidesService: SlidesService,
  ) {}
}
