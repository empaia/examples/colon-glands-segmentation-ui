import { provideMockStore } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator/jest';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { SlidesImagesEffects } from './slides-images.effects';

describe('SlidesImagesEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<SlidesImagesEffects>;
  const createEffect = createServiceFactory({
    service: SlidesImagesEffects,
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      SlidesImagesEffects,
      provideMockActions(() => actions$),
      provideMockStore()
    ],
  });

  beforeEach(() => spectator = createEffect());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
