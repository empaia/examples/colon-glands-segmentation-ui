import { HttpErrorResponse } from '@angular/common/http';
import { SlideImage, SlideImageStatus } from '@slides/store/slides-images/slides-images.models';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as SlidesImagesActions from './slides-images.actions';


export const SLIDES_IMAGES_FEATURE_KEY = 'slidesImages';

export interface State extends EntityState<SlideImage> {
  error?: HttpErrorResponse | null;
}

export const slidesImagesAdapter = createEntityAdapter<SlideImage>();

export const initialState: State = slidesImagesAdapter.getInitialState({
  error: undefined,
});

export const reducer = createReducer(
  initialState,
  on(SlidesImagesActions.loadSlideLabel, (state, { slideId }): State =>
    slidesImagesAdapter.upsertOne({ id: slideId, labelStatus: SlideImageStatus.LOADING }, {
      ...state
    })
  ),
  on(SlidesImagesActions.loadSlideLabelSuccess, (state, { slideImage }): State =>
    slidesImagesAdapter.upsertOne(slideImage, {
      ...state
    })
  ),
  on(SlidesImagesActions.loadSlideLabelFailure, (state, { slideId, error }): State =>
    slidesImagesAdapter.upsertOne({ id: slideId, labelStatus: SlideImageStatus.ERROR }, {
      ...state,
      error
    })
  ),
  on(SlidesImagesActions.loadSlideThumbnail, (state, { slideId }): State =>
    slidesImagesAdapter.upsertOne({ id: slideId, thumbnailStatus: SlideImageStatus.LOADING }, {
      ...state
    })
  ),
  on(SlidesImagesActions.loadSlideThumbnailSuccess, (state, { slideImage }): State =>
    slidesImagesAdapter.upsertOne(slideImage, {
      ...state
    })
  ),
  on(SlidesImagesActions.loadSlideThumbnailFailure, (state, { slideId, error }): State =>
    slidesImagesAdapter.upsertOne({ id: slideId, thumbnailStatus: SlideImageStatus.ERROR }, {
      ...state,
      error
    })
  ),
  on(SlidesImagesActions.clearAllImages, (state): State =>
    slidesImagesAdapter.removeAll({
      ...state
    })
  ),
);
