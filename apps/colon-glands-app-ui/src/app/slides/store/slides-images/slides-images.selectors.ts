import { createSelector } from '@ngrx/store';
import {
  State as ModuleState,
  selectSlideFeatureState,
} from '../slides-feature.state';
import { slidesImagesAdapter } from './slides-images.reducer';

const {
  selectAll,
  selectEntities,
} = slidesImagesAdapter.getSelectors();

export const selectSlidesImagesState = createSelector(
  selectSlideFeatureState,
  (state: ModuleState) => state.slidesImages
);

export const selectAllSlidesImages = createSelector(
  selectSlidesImagesState,
  (state) => selectAll(state)
);

export const selectSlidesImagesEntities = createSelector(
  selectSlidesImagesState,
  (state) => selectEntities(state)
);

export const selectSlidesImagesError = createSelector(
  selectSlidesImagesState,
  (state) => state.error
);
