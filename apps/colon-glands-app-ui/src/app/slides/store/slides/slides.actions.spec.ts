import * as SlidesActions from './slides.actions';

describe('Slides Actions', () => {
  it('should return an action', () => {
    expect(SlidesActions.loadSlides({
      scopeId: 'SCOPE-ID'
    }).type).toBe('[APP/Slides] Load Slides');
  });
});
