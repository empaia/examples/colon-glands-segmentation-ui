import { provideMockStore } from '@ngrx/store/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createServiceFactory, SpectatorService, mockProvider } from '@ngneat/spectator/jest';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { SlidesEffects } from './slides.effects';
import { SlideConversionService } from '@slides/services/slide-conversion.service';

describe('SlidesEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<SlidesEffects>;
  const createEffect = createServiceFactory({
    service: SlidesEffects,
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      SlidesEffects,
      provideMockActions(() => actions$),
      provideMockStore(),
      mockProvider(SlideConversionService)
    ]
  });

  beforeEach(() => spectator = createEffect());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
