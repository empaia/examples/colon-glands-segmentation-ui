import { Slide as SlideView } from '@api/wbs-api/models/slide';
import { Slide } from 'slide-viewer';

export interface SlideEntity {
  id: string;
  disabled: boolean;
  dataView: SlideView;
  imageView?: Slide;
}
