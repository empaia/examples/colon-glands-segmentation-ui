import { MenuLabelComponent } from '@shared/components/menu-label/menu-label.component';
import { MockComponents, MockPipes } from 'ng-mocks';
import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { SlidesLabelComponent } from './slides-label.component';
import { StringSlicerPipe } from '@shared/pipes/string-slicer.pipe';

describe('SlidesLabelComponent', () => {
  let spectator: Spectator<SlidesLabelComponent>;
  const createComponent = createComponentFactory({
    component: SlidesLabelComponent,
    declarations: [
      MockComponents(
        MenuLabelComponent,
      ),
      MockPipes(
        StringSlicerPipe
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
