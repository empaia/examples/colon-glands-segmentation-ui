import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SlideEntity } from '@slides/store';

@Component({
  selector: 'app-slides-label',
  templateUrl: './slides-label.component.html',
  styleUrls: ['./slides-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlidesLabelComponent {
  @Input() slide: SlideEntity | undefined;
  @Input() selected!: boolean;
  @Input() contentLoaded!: boolean;
  @Input() labelId!: string;
  @Input() displayId!: string;
}
