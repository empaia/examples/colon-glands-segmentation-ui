import { DictParserPipe } from '@shared/pipes/dict-parser.pipe';
import { MaterialModule } from '@material/material.module';
import { CopyToClipboardDirective } from '@shared/directives/copy-to-clipboard.directive';
import { SlideImageItemComponent } from '@slides/components/slide-image-item/slide-image-item.component';
import { MockComponents, MockDirectives, MockPipes } from 'ng-mocks';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { SlideItemComponent } from './slide-item.component';
import { StringSlicerPipe } from '@shared/pipes/string-slicer.pipe';

describe('SlideItemComponent', () => {
  let spectator: Spectator<SlideItemComponent>;
  const createComponent = createComponentFactory({
    component: SlideItemComponent,
    imports: [
      MaterialModule,
    ],
    declarations: [
      MockComponents(
        SlideImageItemComponent,
      ),
      MockDirectives(
        CopyToClipboardDirective,
      ),
      MockPipes(
        DictParserPipe,
        StringSlicerPipe,
      )
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {
      slide: {
        id: 'Test_Id',
        disabled: false,
        dataView: {
          id: 'Test_Id',
          case_id: 'Test_Case_Id',
          created_at: 0,
          updated_at: 0,
        }
      }
    }
  }));

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
