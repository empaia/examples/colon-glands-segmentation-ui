import { MissingSelectionErrorComponent } from '@shared/components/missing-selection-error/missing-selection-error.component';
import { SlideItemComponent } from '@slides/components/slide-item/slide-item.component';
import { MockComponents } from 'ng-mocks';
import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { SlidesComponent } from './slides.component';

describe('SlidesComponent', () => {
  let spectator: Spectator<SlidesComponent>;
  const createComponent = createComponentFactory({
    component: SlidesComponent,
    declarations: [
      MockComponents(
        SlideItemComponent,
        MissingSelectionErrorComponent,
      )
    ]
  });

  beforeEach(() => spectator = createComponent());

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });
});
