import { createServiceFactory, SpectatorService, mockProvider } from '@ngneat/spectator/jest';
import { ScopeService } from '@scope/services/scope.service';
import { AccessTokenService } from '@token/services/access-token.service';
import { WbsUrlService } from '@wbs-url/services/wbs-url.service';
import { SlideConversionService } from './slide-conversion.service';

describe('SlideConversionService', () => {
  let spectator: SpectatorService<SlideConversionService>;
  const createService = createServiceFactory({
    service: SlideConversionService,
    providers: [
      mockProvider(AccessTokenService),
      mockProvider(ScopeService),
      mockProvider(WbsUrlService),
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
});
