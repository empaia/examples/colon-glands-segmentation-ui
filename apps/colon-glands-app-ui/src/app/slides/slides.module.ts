import { EffectsModule } from '@ngrx/effects';
import { SLIDES_MODULE_FEATURE_KEY, reducers } from '@slides/store/slides-feature.state';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from './../shared/shared.module';
import { LetModule } from '@ngrx/component';
import { MaterialModule } from '@material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlidesEffects, SlidesImagesEffects } from '@slides/store';
import { SlideImageItemComponent } from './components/slide-image-item/slide-image-item.component';
import { SlideItemComponent } from './components/slide-item/slide-item.component';
import { SlidesComponent } from './components/slides/slides.component';
import { SlidesLabelComponent } from './components/slides-label/slides-label.component';
import { SlidesContainerComponent } from './containers/slides-container/slides-container.component';



@NgModule({
  declarations: [
    SlideImageItemComponent,
    SlideItemComponent,
    SlidesComponent,
    SlidesLabelComponent,
    SlidesContainerComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    LetModule,
    SharedModule,
    StoreModule.forFeature(
      SLIDES_MODULE_FEATURE_KEY,
      reducers
    ),
    EffectsModule.forFeature([
      SlidesEffects,
      SlidesImagesEffects,
    ])
  ],
  exports: [
    SlidesContainerComponent,
    SlidesLabelComponent,
  ]
})
export class SlidesModule { }
