#!/bin/sh
ASSETS_PATH=/usr/share/nginx/html/assets/env
envsubst < ${ASSETS_PATH}/env.template.js > ${ASSETS_PATH}/env.js

nginx -g 'daemon off;'
