# v next

## 23-09-12
### Demo App
- Show additional information for each polygon on click (area, prediction scores)

##21-04-30
###Slide-Viewer
- Removed field slideName from slide model

##21-03-22
###Test-Suite-Web-Client
- Removed Annotation-Tree Component
- Added highlighting feature to job-list for current selected job

##21-03-19
###Test-Suite-Web-Client
- Fixed highlighting bug in slide list, currently selected slide will 
now be highlighted

##21-03-17
### Workspace and demo app
- Added artificial level in slide-conversion of all apps
- Adjusted tile loaders and viewer for slides with artificial level

##21-03-16
### Test-Suite-Web-Client
- Separate annotation data structure for annotation list and annotation view
- Add class list to filter annotations in list and view
- Add Pagination functionality to the ngrx store
- Add annotation filter with a viewport and npp range (tight range)
- Add routing functionality to select job and slide per uri
- Regain "Zoom To" functionality

## 21-02-12
### Workbench Client
- Add workbench client to monorepo workspace
  - basic scss classes
  - basic store 
  - route based selection and fetching of cases and slides
  - basic test setup
## 21-03-03
### Demo App
- Add service to provide dummy user id & user-id header to api
- Add env var to set user id for wbs on deployment

### SlideViewer v.0.1.0
- new tile resolver interface function: tileLoaderFn
- tileLoaderFn makes it possible to set headers on tile requests. see: https://openlayers.org/en/latest/apidoc/module-ol_Tile.html#~LoadFunction 
- see [changelog.md](/libs/slide-viewer/CHANGELOG.md)

### Workspace
- add gitlab ci pipeline to test production builds on every push to gitlab
