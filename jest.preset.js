const nxPreset = require('@nrwl/jest/preset').default;

module.exports = { 
  ...nxPreset,
  moduleDirectories: [
    "node_modules/.compiled",
    "node_modules"
  ],
  setupFiles: [
    'jest-canvas-mock'
  ],
  transform: {
    '^.+\\.(ts|mjs|js|html)$': 'jest-preset-angular',
  },
  transformIgnorePatterns: ['node_modules/(?!.*\\.mjs$)'],
};
