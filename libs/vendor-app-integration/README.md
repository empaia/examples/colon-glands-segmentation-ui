# Vendor-App-Integration-Module

This library is using Angular and ngrx to integrate vendor supplied apps into the WBC2.0 App iFrame slot.

## Public Api Usage

include in `VendorAppIntegrationModule` in your App Module:

```ts
@NgModule({
  //...
  imports: [
    //...
    /* app integration module */
    VendorAppIntegrationModule,
  ],
  //...
})
```

in your template add

```html
<ngx-vendor-app-integration
  [appUrl]="appUrl"
  [scope]="scope"
  [token]="token"
  [wbsUrl]="wbsUrl"
  (receiveTokenRequest)="onTokenRequest($event)"
  (receiveScopeReady)="onScopeReady($event)"
  (receiveTokenReady)="onTokenReady($event)"
  (receiveWbsUrlReady)="onWbsUrlReady($event)"
>
</ngx-vendor-app-integration>
```

### Inputs

`[appUrl]`: the url (`string`) of the app that should be integrated and interacted with

`[scope]`: a scope object of type `Scope`. The type field must be declared as `scope`. Scope is defined in [scope.model.ts](./src/lib/scope/store/scope/scope.model.ts)

`[token]`: a token object of type `Token`. The type field must be declared as `token`. Token is defined in [token.model.ts](./src/lib/token/store/token/token.model.ts)

`[wbsUrl]`: a wbsUrl object of type `WbsUrl`. The type field must be declared as `wbsUrl`. WbsUrl is defined in [wbs-url.model.ts](./src/lib/wbs-url/store/wbs-url/wbs-url.model.ts)

### Output Events

`(receiveTokenRequest)`: emits an `Request` Object, which informs your platform that the access token for the app is expired.

`(receiveScopeReady)`: emits an `ScopeReady` Object, which informs your platform that the current selected app is ready to receive the `Scope` Object for the first time.

`(receiveTokenReady)`: emits an `TokenReady` Object, which informs your platform that the current selected app is ready to receive the `Token` Object for the first time.

`(receiveWbsUrlReady)`: emits an `WbsUrlReady` Object, which informs your platform that the current selected app is ready to receive the `WbsUrl` Object for the first time.

### Build

Run `nx build vendor-app-integration` to build the project. The build artifacts will be stored in the `dist/` directory.

### Watch Mode

`nx build vendor-app-integration --watch`
