import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import {
  reducers,
  WBS_URL_MODULE_FEATURE_KEY,
} from './store/wbs-url-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { WbsUrlEffects } from './store';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(WBS_URL_MODULE_FEATURE_KEY, reducers),
    EffectsModule.forFeature([WbsUrlEffects]),
  ],
})
export class WbsUrlModule {}
