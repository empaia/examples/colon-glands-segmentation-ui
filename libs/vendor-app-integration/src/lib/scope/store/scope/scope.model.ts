/* eslint-disable @typescript-eslint/no-explicit-any */
export interface Scope {
  id: string;
  type: 'scope';
}

export interface ScopeReady {
  type: 'scopeReady';
}

export const SCOPE_TYPE = 'scope';
export const SCOPE_READY_TYPE = 'scopeReady';

export function isTypeOfScopeReady(object: any): object is ScopeReady {
  return 'type' in object && object['type'] === SCOPE_READY_TYPE;
}

export interface IdScope {
  id: string;
  scope: Scope;
}
