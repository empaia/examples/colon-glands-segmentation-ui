import * as fromScope from './scope.actions';

describe('loadScopes', () => {
  it('should return an action', () => {
    expect(fromScope.sendScope({ idScope: undefined }).type).toBe(
      '[AI/Scope] Send Scope'
    );
  });
});
