import { Action, combineReducers, createFeatureSelector } from '@ngrx/store';
import * as fromScope from './scope/scope.reducer';

export const SCOPE_MODULE_FEATURE_KEY = 'scopeModuleFeature';

export const selectScopeFeatureState = createFeatureSelector<State>(
  SCOPE_MODULE_FEATURE_KEY
);

export interface State {
  [fromScope.SCOPE_FEATURE_KEY]: fromScope.State;
}

export function reducers(state: State | undefined, action: Action) {
  return combineReducers({
    [fromScope.SCOPE_FEATURE_KEY]: fromScope.reducer,
  })(state, action);
}
