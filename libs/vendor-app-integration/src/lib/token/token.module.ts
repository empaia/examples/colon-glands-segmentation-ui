import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import {
  reducers,
  TOKEN_MODULE_FEATURE_KEY,
} from './store/token-feature.state';
import { EffectsModule } from '@ngrx/effects';
import { TokenEffects } from './store';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(TOKEN_MODULE_FEATURE_KEY, reducers),
    EffectsModule.forFeature([TokenEffects]),
  ],
})
export class TokenModule {}
