import * as fromToken from './token.actions';

describe('loadTokens', () => {
  it('should return an action', () => {
    expect(fromToken.sendToken({ idToken: undefined }).type).toBe(
      '[AI/Token] Send Token'
    );
  });
});
