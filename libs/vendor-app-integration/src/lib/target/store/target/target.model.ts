export type Target = HTMLIFrameElement;

export interface IdTarget {
  id: string;
  target: Target;
}
