import * as fromTarget from './target.actions';

describe('loadTargets', () => {
  it('should return an action', () => {
    expect(
      fromTarget.setTarget({
        idTarget: null,
      }).type
    ).toBe('[Integration/Target] Set Target');
  });
});
