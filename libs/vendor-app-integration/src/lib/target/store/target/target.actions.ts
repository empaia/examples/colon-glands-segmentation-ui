import { createAction, props } from '@ngrx/store';
import { IdTarget } from './target.model';

export const setTarget = createAction(
  '[Integration/Target] Set Target',
  props<{ idTarget: IdTarget }>()
);

export const removeTargets = createAction(
  '[Integration/Target] Remove Target',
  props<{ ids: string[] }>()
);
