import { createReducer, on } from '@ngrx/store';
import { IdTarget } from './target.model';
import * as TargetActions from './target.actions';
import { createEntityAdapter, EntityState } from '@ngrx/entity';

export const TARGET_FEATURE_KEY = 'target';

export type State = EntityState<IdTarget>;

export const targetAdapter = createEntityAdapter<IdTarget>();

export const initialState: State = targetAdapter.getInitialState();

export const reducer = createReducer(
  initialState,
  on(
    TargetActions.setTarget,
    (state, { idTarget }): State =>
      targetAdapter.upsertOne(idTarget, {
        ...state,
      })
  ),
  on(
    TargetActions.removeTargets,
    (state, { ids }): State =>
      targetAdapter.removeMany(ids, {
        ...state,
      })
  )
);
