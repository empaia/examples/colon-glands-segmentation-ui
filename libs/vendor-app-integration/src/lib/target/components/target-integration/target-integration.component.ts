import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Target } from '../../store/target/target.model';
import { IFRAME_BORDER, IFRAME_DIMENSION } from './target-integration.models';

@Component({
  selector: 'ngx-target-integration',
  templateUrl: './target-integration.component.html',
  styleUrls: ['./target-integration.component.scss'],
})
export class TargetIntegrationComponent implements AfterViewInit {
  @Input() id!: string;
  @Input() app!: string;
  @Input() sandboxParameters!: string;
  @Output() setApp = new EventEmitter<Target>();

  public readonly IFRAME_HOST = 'iframe-host-';
  public readonly EMBEDDED_ID = 'embeddedApp-';

  ngAfterViewInit(): void {
    const target = this.creatIframe();
    this.setApp.emit(target);
  }

  private creatIframe(): Target {
    const div = document.getElementById(this.IFRAME_HOST + this.id) as HTMLDivElement;
    const iframe = document.createElement('iframe') as HTMLIFrameElement;
    iframe.id = this.EMBEDDED_ID + this.id;
    iframe.src = this.app;
    iframe.width = IFRAME_DIMENSION;
    iframe.height = IFRAME_DIMENSION;
    iframe.style.border = IFRAME_BORDER;
    this.sandboxParameters.split(' ').forEach(p => iframe.sandbox.add(p));
    div.appendChild(iframe);
    return iframe;
  }
}
