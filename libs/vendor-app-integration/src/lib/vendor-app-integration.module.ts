import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorAppIntegrationComponent } from './vendor-app-integration/vendor-app-integration.component';
import { TargetModule } from './target/target.module';
import { ScopeModule } from './scope/scope.module';
import { TokenModule } from './token/token.module';
import { WbsUrlModule } from './wbs-url/wbs-url.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { IdScope, Scope, ScopeReady, SCOPE_TYPE } from './scope/store';
import {
  IdToken,
  Token,
  TokenReady,
  Request,
  TOKEN_TYPE,
  TOKEN_REQUEST_TYPE,
} from './token/store';
import { IdWbsUrl, WbsUrl, WbsUrlReady, WBS_URL_TYPE } from './wbs-url/store';
import { SharedModule } from './shared/shared.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

export {
  VendorAppIntegrationComponent,
  IdScope as AppScope,
  IdToken as AppToken,
  IdWbsUrl as AppWbsUrl,
  Scope,
  ScopeReady,
  SCOPE_TYPE,
  Token,
  TokenReady,
  Request,
  TOKEN_TYPE,
  TOKEN_REQUEST_TYPE,
  WbsUrl,
  WbsUrlReady,
  WBS_URL_TYPE,
};

@NgModule({
  imports: [
    CommonModule,
    ScopeModule,
    SharedModule,
    TargetModule,
    TokenModule,
    WbsUrlModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      name: 'Vendor App Integration Store',
    }),
    // TODO Dev tools should only be included in non production builds!
    // !environment.production ? StoreDevtoolsModule.instrument({
    //   name: 'App Integration Store'
    // }) : [],
  ],
  declarations: [VendorAppIntegrationComponent],
  exports: [VendorAppIntegrationComponent],
})
export class VendorAppIntegrationModule {}
