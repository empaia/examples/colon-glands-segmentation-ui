import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MaterialModule } from './material/material.module';
import { ResizableModule } from 'angular-resizable-element';
import {
  TileResolver,
  Slide,
  ImageInfo,
  Level,
} from './models/slide';
import {
  Annotation,
  AnnotationRectangle,
  AnnotationCircle,
  AnnotationPolygon,
  AnnotationPoint,
  AnnotationReferenceType,
  AnnotationEntity,
} from './models/annotation';
import { UiConfig } from './models/viewer-config';
import {
  AnnotationCharacterLimit,
  CharacterLimit,
} from './models/character-limit';

/** module config */
import {
  SlideViewerModuleConfigurationParams,
  SlideViewerModuleConfiguration
} from './config/module-config';

/* containers */
import { SlideViewerComponent } from './containers/slide-viewer/slide-viewer.component';
import { SlideViewContainerComponent } from './containers/slide-view-container/slide-view-container.component';

/* view components */
//import { OverviewComponent } from './components/overview/overview.component';
import { ScaleLineComponent } from './components/scale-line/scale-line.component';
//import { ZoomSliderComponent } from './components/zoom-slider/zoom-slider.component';
import { ViewerComponent } from './components/viewer/viewer.component';
import { ExtOverviewComponent } from './components/ext-overview/ext-overview.component';
import { DndContainerComponent } from './components/dnd-container/dnd-container.component';
import { CreateAnnotationCardComponent } from './components/create-annotation-card/create-annotation-card.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { AnnotationLayerComponent } from './components/annotation-layer/annotation-layer.component';
import { CurrentView, ZoomView } from './models/ui';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { SvmStoreModule } from './store/svm-store.module';
import { AnnotationCreateComponent } from './components/annotation-create/annotation-create.component';
import { MeasurementToolComponent } from './components/measurement-tool/measurement-tool.component';
import { AnnotationType } from './models/annotation-type';
import { ReactiveComponentModule } from '@ngrx/component';
import { ExtZoomSliderComponent } from './components/ext-zoom-slider/ext-zoom-slider.component';
import { AnnotationClusterLayerComponent } from './components/annotation-cluster-layer/annotation-cluster-layer.component';
import { ToolbarInteractionType, InteractionType, DrawType } from './models/interaction';
import { DataCreatorType } from './models/creator';
import { Class } from './models/class';
import { AnnotationColorPaletteComponent } from "./components/annotation-color-palette/annotation-color-palette.component";
import { CloseElementDirective } from './directives/close-element.directive';
import { AnnotationHighlightConfig, HighlightType } from './config/annotation-highlight.config';
import { XaiLayerComponent } from './components/xai-layer/xai-layer.component';
import { RectangleExplanationEntity } from './models/rectangle-explanation';

export {
  /**
   * Component exports
   * TODO: do not export components that are not for the public api
   */
  SlideViewerComponent,
  /**
   * Interface exports
   * TODO: do not export interfaces that are not for the public api
   */
  TileResolver,
  Annotation,
  AnnotationCircle,
  AnnotationRectangle,
  AnnotationPolygon,
  AnnotationPoint,
  AnnotationType,
  AnnotationReferenceType,
  AnnotationEntity,
  RectangleExplanationEntity,
  Class,
  DataCreatorType,
  Slide,
  ImageInfo,
  Level,
  CurrentView,
  ZoomView,
  ToolbarInteractionType,
  InteractionType,
  DrawType,
  UiConfig,
  AnnotationCharacterLimit,
  CharacterLimit,
  AnnotationHighlightConfig,
  HighlightType,
};

/**
 * Module Components
 */
const COMPONENTS = [
  SlideViewContainerComponent,
  ViewerComponent,
  //OverviewComponent,
  ExtOverviewComponent,
  ScaleLineComponent,
  //ZoomSliderComponent,
  ExtZoomSliderComponent,
  DndContainerComponent,
  CreateAnnotationCardComponent,
  ToolbarComponent,
  AnnotationLayerComponent,
  AnnotationCreateComponent,
  SlideViewerComponent,
  MeasurementToolComponent,
  AnnotationClusterLayerComponent,
  AnnotationColorPaletteComponent,
  XaiLayerComponent
];

@NgModule({
  declarations: [...COMPONENTS, CloseElementDirective],
  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
    MatMenuModule,
    ReactiveFormsModule,
    DragDropModule,
    ResizableModule,
    SvmStoreModule,
    StoreModule.forRoot({
      /* EMPTY Root store */
    }),
    StoreDevtoolsModule.instrument({
      name: 'Slide Viewer Module',
      // In a production build you would want to disable the Store Devtools
      // logOnly: environment.production,
    }),
    ReactiveComponentModule,
  ],
  exports: [
    SlideViewerComponent
  ],
  providers: [
    SlideViewerModuleConfiguration
  ]
})
export class SlideViewerModule {
  static forRoot(params: SlideViewerModuleConfigurationParams = {}): ModuleWithProviders<SlideViewerModule> {
    return {
      ngModule: SlideViewerModule,
      providers: [
        {
          provide: SlideViewerModuleConfiguration,
          useValue: params
        }
      ]
    };
  }
}
