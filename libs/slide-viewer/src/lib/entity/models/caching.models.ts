import { IdSelector } from '@ngrx/entity';

export interface MinimalEntity {
  id: string;
}

export interface EntityCachingState<T> {
  entityCache: Map<string, T>;
  addedEntityIds: string[] | undefined;
  cacheSizeSoftConstraint: number;
}

export interface EntityCachingStateAdapter<T> {
  addOne<S extends EntityCachingState<T>>(entity: T, state: S): S;
  addMany<S extends EntityCachingState<T>>(entities: T[], state: S): S;
  removeOne<S extends EntityCachingState<T>>(key: string, state: S): S;
  removeMany<S extends EntityCachingState<T>>(keys: string[], state: S): S;
  removeToIndex<S extends EntityCachingState<T>>(index: number, state: S): S;
  removeAll<S extends EntityCachingState<T>>(state: S): S;
}

export interface EntityCachingSelectors<T, V extends EntityCachingState<T>> {
  selectIds: (state: V) => string[];
  selectAll: (state: V) => T[];
  selectTotal: (state: V) => number;
}

export interface EntityCachingAdapter<T> extends EntityCachingStateAdapter<T> {
  selectedId: IdSelector<T>;
  getInitialState(cacheSize?: number): EntityCachingState<T>;
  getInitialState<S>(state: S, cacheSize?: number): EntityCachingState<T> & S;
  getSelectors(): EntityCachingSelectors<T, EntityCachingState<T>>;
}
