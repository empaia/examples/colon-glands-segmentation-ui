import { EntityCachingState } from '../models/caching.models';
import { DEFAULT_ENTITY_CACHE_SIZE } from '../models/caching.constants';

export class CachingState<T> implements EntityCachingState<T>{
  addedEntityIds: string[] | undefined = undefined;
  cacheSizeSoftConstraint: number;
  entityCache: Map<string, T> = new Map<string, T>();

  constructor(cacheSize?: number) {
    this.cacheSizeSoftConstraint = cacheSize ? cacheSize : DEFAULT_ENTITY_CACHE_SIZE;
  }
}
