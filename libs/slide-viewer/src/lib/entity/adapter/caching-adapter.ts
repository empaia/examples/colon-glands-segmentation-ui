import {
  EntityCachingAdapter,
  EntityCachingSelectors,
  EntityCachingState,
  MinimalEntity,
} from '../models/caching.models';
import { CachingState } from '../state/caching-state';
import { CachingSelectors } from '../selectors/caching-selectors';
import { IdSelector } from '@ngrx/entity';

export class CachingAdapter<T> implements EntityCachingAdapter<T> {

  constructor(selectedId?: IdSelector<T>) {
    const select = Object.assign({selectedId: (instance: MinimalEntity) => instance.id}, {selectedId: selectedId});
    this.selectedId = select.selectedId;
  }

  selectedId: IdSelector<T>;

  addMany<S extends EntityCachingState<T>>(entities: T[], state: S): S {
    const newState = { ...state };
    newState.addedEntityIds = [];
    for (const entity of entities) {
      const id = this.selectedId(entity) as string;
      if (!newState.entityCache.has(id)) {
        // newState.entityCache.delete(id);
        newState.entityCache.set(id, entity);
        newState.addedEntityIds.push(id);
      }
    }
    return newState;
  }

  addOne<S extends EntityCachingState<T>>(entity: T, state: S): S {
    const newState = { ...state };
    newState.addedEntityIds = [];
    const id = this.selectedId(entity) as string;
    if (!newState.entityCache.has(id)) {
      // newState.entityCache.delete(id);
      newState.entityCache.set(id, entity);
      newState.addedEntityIds.push(id);
    }
    // newState.entityCache.set(id, entity);
    // newState.addedEntityIds.push(id);
    return newState;
  }

  getInitialState(cacheSize?: number): EntityCachingState<T>;
  getInitialState<S>(state: S, cacheSize?: number): EntityCachingState<T> & S;
  getInitialState<S>(state?: S, cacheSize?: number): EntityCachingState<T> {
    const cachingState = new CachingState<T>(cacheSize);
    return state ? { ...cachingState, ...state } : cachingState;
  }

  getSelectors(): EntityCachingSelectors<T, EntityCachingState<T>> {
    return new CachingSelectors<T, EntityCachingState<T>>();
  }

  removeAll<S extends EntityCachingState<T>>(state: S): S {
    const newState = { ...state };
    newState.addedEntityIds = [];
    newState.entityCache.clear();
    return newState;
  }

  removeMany<S extends EntityCachingState<T>>(keys: string[], state: S): S {
    const newState = { ...state };
    keys.forEach(key => newState.entityCache.delete(key));
    return newState;
  }

  removeToIndex<S extends EntityCachingState<T>>(index: number, state: S): S {
    const newState = { ...state };
    Array.from(newState.entityCache.keys())
      .splice(0, index)
      .forEach(key => newState.entityCache.delete(key));
    return newState;
  }

  removeOne<S extends EntityCachingState<T>>(key: string, state: S): S {
    const newState = { ...state };
    newState.entityCache.delete(key);
    return newState;
  }
}

export function createEntityCachingAdapter<T>(selectedId?: IdSelector<T>): EntityCachingAdapter<T> {
  return new CachingAdapter<T>(selectedId);
}
