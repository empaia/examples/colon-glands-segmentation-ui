import { Extent } from 'ol/extent';

// DO NOT REMOVE @dynamic comment: fixes production build bug  - https://github.com/ng-packagr/ng-packagr/issues/641

// @dynamic
export class AxisConverter {
  public static invertYAxisOfArray(coordinates: number[][]): number[][] {
    return coordinates.map(c => this.invertYAxis(c));
  }

  public static invertYAxis(coordinates: number[]): number[] {
    if (coordinates.length !== 2) {
      throw new Error('AxisConverter unexpected Array length');
    }
    return [coordinates[0], -coordinates[1]];
  }

  public static invertYAxisOfExtent(extent: Extent): Extent {
    // inverts negative y values
    const tmp: Extent = [
      extent[0],
      -extent[1],
      extent[2],
      -extent[3]
    ];
    // extent must be reordered to fit the extent requirements
    // [minX, minY, maxX, maxY]
    const reordered: Extent = [
      Math.min(tmp[0], tmp[2]),
      Math.min(tmp[1], tmp[3]),
      Math.max(tmp[2], tmp[0]),
      Math.max(tmp[3], tmp[1])
    ];
    return reordered;
  }
}
