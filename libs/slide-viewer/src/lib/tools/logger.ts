/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-console */
export enum LOG_LEVEL {
  NONE = 0,
  ASSERT = 1,
  ERROR = 2,
  WARN = 3,
  INFO = 4,
  DEBUG = 5,
  VERBOSE = 6,
}

abstract class Logger {
  log: any;
  info: any;
  warn: any;
  error: any;
  table: any;
}

class LoggerService implements Logger {
  log: any;
  info: any;
  warn: any;
  error: any;
  table: any;
  invokeConsoleMethod(type: string, args?: any): void { }
}

const noop = (): any => undefined;

class ConsoleLoggerService implements Logger {
  private loggingLevel = 0;

  constructor(logLevel: LOG_LEVEL) {
    this.setLevel(logLevel);
  }
  invokeConsoleMethod(type: string, args?: any): void {
    const logFn = console[type] || console.log || noop;
    const enhancedArgs = args.map((arg: any) => JSON.stringify(arg, null, 2));
    logFn.apply(console, [enhancedArgs]);
  }

  public setLevel(level: number) {
    this.loggingLevel = level;
  }

  get log(): any {
    return this.loggingLevel >= LOG_LEVEL.VERBOSE
      ? console.log.bind(console)
      : noop;
  }

  get info(): any {
    return this.loggingLevel >= LOG_LEVEL.INFO
      ? console.info.bind(console)
      : noop;
  }

  get warn(): any {
    return this.loggingLevel >= LOG_LEVEL.WARN
      ? console.warn.bind(console)
      : noop;
  }

  get error(): any {
    return this.loggingLevel >= LOG_LEVEL.ERROR
      ? console.error.bind(console)
      : noop;
  }

  get table(): any {
    return this.loggingLevel >= LOG_LEVEL.VERBOSE
      ? console.table.bind(console)
      : noop;
  }

  get assert(): any {
    return this.loggingLevel >= LOG_LEVEL.ASSERT
      ? console.assert.bind(console)
      : noop;
  }
}

export const logger = new ConsoleLoggerService(LOG_LEVEL.NONE);
