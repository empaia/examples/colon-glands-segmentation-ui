import { Coordinate } from 'ol/coordinate';
import { getCenter } from 'ol/extent';
import { Circle, Geometry, Point, Polygon } from 'ol/geom';
import {
  Annotation,
  AnnotationCircle,
  AnnotationPoint,
  AnnotationPolygon,
  AnnotationRectangle,
  AnnotationReferenceType,
} from '../models/annotation';
import { AnnotationType } from '../models/annotation-type';
import { DrawType } from '../models/interaction';
import { logger } from './logger';
import { AxisConverter } from './axis-converter';

export class GeometryToAnnotation {
  private static fakeAnnotationData: Partial<Annotation> = {
    name: 'INVALID-name',
    description: 'INVALID-desc',
    creatorId: 'INVALID-user_id',
    creatorType: undefined,
    referenceId: 'INVALID-ref_id',
    referenceType: AnnotationReferenceType.Wsi,
    nppCreated: -1,
    annotationType: undefined,
    // classes: {}
  };

  static geometryToAnnotation(
    geometry: Geometry,
    drawMode: DrawType
  ): Annotation {
    let annotation: Annotation;
    if (geometry instanceof Circle) {
      const [x, y] = geometry.getCenter().map(Math.round);
      const radius = Math.round(geometry.getRadius());
      annotation = new AnnotationCircle({
        ...GeometryToAnnotation.fakeAnnotationData,
        annotationType: AnnotationType.CIRCLE,
        center: [x, -y],
        radius,
      });
    } else if (geometry instanceof Point) {
      const coordinates = AxisConverter.invertYAxis(
        geometry.getCoordinates().map(Math.round)
      );
      annotation = new AnnotationPoint({
        ...GeometryToAnnotation.fakeAnnotationData,
        annotationType: AnnotationType.POINT,
        center: coordinates,
        coordinates,
      });
    } else if (geometry instanceof Polygon) {
      let coordinates = AxisConverter
        .invertYAxisOfArray(
          geometry
            .getCoordinates()[0]
            .map((items: Coordinate) => items.map(Math.round))
        );
      const extend = geometry.getExtent();
      // no need for subpixel accuracy -> round to nearest int
      const center = AxisConverter.invertYAxis(
        getCenter(extend).map((items) => Math.round(items))
      );

      if (drawMode === DrawType.Rectangle) {
        coordinates = this.rearrangeRectangle(coordinates);
        annotation = new AnnotationRectangle({
          ...GeometryToAnnotation.fakeAnnotationData,
          annotationType: AnnotationType.RECTANGLE,
          center,
          coordinates,
        });
        logger.info(`Box annotation ${annotation}`);
      } else if (drawMode === DrawType.Polygon || drawMode === DrawType.PolygonAlternative) {
        // based on the DICOM standard we remove the last coordinate here
        // which is equal to the first coordinate of a polygon in ol
        coordinates = coordinates.slice(0, -1);
        annotation = new AnnotationPolygon({
          ...this.fakeAnnotationData,
          annotationType: AnnotationType.POLYGON,
          center,
          coordinates,
        });
      }
    } else {
      throw new Error('geometry object not implemented');
    }
    return annotation;
  }

  /**
   * Rearrange coordinates of a rectangle so it starts at the upper left corner
   * and follows the contour clockwise
   * @param coordinates, All coordinate pairs of the rectangle
   * @returns coordinates, Rearranged coordinates
   */
  private static rearrangeRectangle(coordinates: number[][]): number[][] {
    if (coordinates.length !== 5) {
      throw new Error(`GeometryToAnnotation - rearrangeRectangle: Expected Array of length 5 - got array length ${coordinates.length}`);
    }
    // Reordering the rectangle coordinates, so it can start on the upper left corner
    // and follows the contour clockwise
    return [
      coordinates[3],
      coordinates[2],
      coordinates[1],
      coordinates[0],
      coordinates[3]
    ];
  }
}
