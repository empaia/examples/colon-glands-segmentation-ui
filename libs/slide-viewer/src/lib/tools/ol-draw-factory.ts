import Draw, { createBox } from 'ol/interaction/Draw';
import { GeometryType } from '../models/geometry';
import VectorSource from 'ol/source/Vector';
import { DrawType, MouseButtonType, MouseInteractionType } from '../models/interaction';
import { Geometry } from 'ol/geom';
import { MapBrowserEvent } from 'ol';
import { always } from 'ol/events/condition';

function drawCondition(
  event: MapBrowserEvent<UIEvent>,
  mouse: MouseInteractionType,
  geometryType: string
): boolean {
  const left = mouseButtonCondition(event, MouseButtonType.Left, mouse.left as DrawType | undefined, geometryType);
  const right = mouseButtonCondition(event, MouseButtonType.Right, mouse.right as DrawType | undefined, geometryType);
  return left || right;
}

function drawFreehandCondition(
  event: MapBrowserEvent<UIEvent>,
  mouse: MouseInteractionType,
  geometryType: string,
): boolean {
  const left = mouseButtonFreehandCondition(event, MouseButtonType.Left, mouse.left as DrawType | undefined, geometryType);
  const right = mouseButtonFreehandCondition(event, MouseButtonType.Right, mouse.right as DrawType | undefined, geometryType);
  return always() && (left || right);
}

function mouseButtonCondition(
  event: MapBrowserEvent<UIEvent>,
  mouseType: MouseButtonType,
  drawType: DrawType | undefined,
  geometryType: string
): boolean {
  return event.originalEvent?.['buttons'] === mouseType &&
    (drawType?.toLowerCase() === geometryType.toLowerCase()
      || drawType === DrawType.PolygonAlternative && geometryType === GeometryType.POLYGON
    );
}

function mouseButtonFreehandCondition(
  event: MapBrowserEvent<UIEvent>,
  mouseType: MouseButtonType,
  drawType: DrawType | undefined,
  geometryType: string
): boolean {
  return event.originalEvent?.['buttons'] !== mouseType && drawType?.toLowerCase() !== geometryType.toLowerCase();
}

export default interface OpenLayersDraw {
  source: VectorSource<Geometry>;
  mouse: MouseInteractionType;
  getDraw(): Draw;
}

class OpenLayersDrawCircle implements OpenLayersDraw {
  constructor(
    public source: VectorSource<Geometry>,
    public mouse: MouseInteractionType
  ) {}
  public getDraw = (): Draw => {
    const d = new Draw({
      type: GeometryType.CIRCLE,
      source: this.source,
      condition: event => drawCondition(event, this.mouse, GeometryType.CIRCLE),
      freehandCondition: event => drawFreehandCondition(event, this.mouse, GeometryType.CIRCLE),
    });
    return d;
  };
}

class OpenLayersDrawRectangle implements OpenLayersDraw {
  constructor(
    public source: VectorSource<Geometry>,
    public mouse: MouseInteractionType
  ) {}
  public getDraw = (): Draw => {
    const d = new Draw({
      type: GeometryType.CIRCLE,
      geometryFunction: createBox(),
      source: this.source,
      condition: event => drawCondition(event, this.mouse, DrawType.Rectangle),
      freehandCondition: event => drawFreehandCondition(event, this.mouse, DrawType.Rectangle),
    });
    return d;
  };
}

class OpenLayersDrawPolygon implements OpenLayersDraw {
  constructor(
    public source: VectorSource<Geometry>,
    public mouse: MouseInteractionType,
    public freehand: boolean
  ) {}
  public getDraw = (): Draw => {
    const d = new Draw({
      type: GeometryType.POLYGON,
      condition: event => drawCondition(event, this.mouse, GeometryType.POLYGON),
      freehandCondition: this.freehand ? event => drawFreehandCondition(event, this.mouse, GeometryType.POLYGON) : () => false,
      source: this.source,
    });
    return d;
  };
}

class OpenLayersDrawLineString implements OpenLayersDraw {
  constructor(
    public source: VectorSource<Geometry>,
    public mouse: MouseInteractionType
  ) {}
  public getDraw = (): Draw => {
    const d = new Draw({
      type: GeometryType.LINE_STRING,
      source: this.source,
      condition: event => drawCondition(event, this.mouse, GeometryType.LINE_STRING),
      freehandCondition: event => drawFreehandCondition(event, this.mouse, GeometryType.LINE_STRING),
      stopClick: true,
    });
    return d;
  };
}

class OpenLayersDrawPoint implements OpenLayersDraw {
  constructor(public source: VectorSource<Geometry>, public mouse: MouseInteractionType) {}
  public getDraw = (): Draw => {
    const d = new Draw({
      type: GeometryType.POINT,
      source: this.source,
      condition: event => drawCondition(event, this.mouse, GeometryType.POINT),
    });
    return d;
  };
}

export class OpenLayersDrawFactory {
  public static fromDrawType(
    drawType: DrawType,
    source: VectorSource<Geometry>,
    mouse: MouseInteractionType
  ): OpenLayersDraw {
    switch (drawType) {
      case DrawType.Circle:
        return new OpenLayersDrawCircle(source, mouse);
      case DrawType.Rectangle:
        return new OpenLayersDrawRectangle(source, mouse);
      case DrawType.Polygon:
        return new OpenLayersDrawPolygon(source, mouse, true);
      case DrawType.PolygonAlternative:
        return new OpenLayersDrawPolygon(source, mouse, false);
      case DrawType.LineString:
        return new OpenLayersDrawLineString(source, mouse);
      case DrawType.Point:
        return new OpenLayersDrawPoint(source, mouse);
      default:
        throw new Error('Draw Type not supported by OpenLayers');
    }
  }
}
