import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { MockComponents } from 'ng-mocks';

import { SlideViewContainerComponent } from './slide-view-container.component';
import { MaterialModule } from '../../material/material.module';
import { ViewerComponent } from '../../components/viewer/viewer.component';
import { DndContainerComponent } from '../../components/dnd-container/dnd-container.component';
import { ExtOverviewComponent } from '../../components/ext-overview/ext-overview.component';
import { ScaleLineComponent } from '../../components/scale-line/scale-line.component';
import { AnnotationCreateComponent } from '../../components/annotation-create/annotation-create.component';
import { AnnotationLayerComponent } from '../../components/annotation-layer/annotation-layer.component';
import { ReactiveComponentModule } from '@ngrx/component';

//import { ZoomSliderComponent } from '../../components/zoom-slider/zoom-slider.component';
import { ToolbarComponent } from '../../components/toolbar/toolbar.component';
import { ExtZoomSliderComponent } from '../../components/ext-zoom-slider/ext-zoom-slider.component';
import { AnnotationClusterLayerComponent } from '../../components/annotation-cluster-layer/annotation-cluster-layer.component';

describe('SlideViewerComponent', () => {
  let spectator: Spectator<SlideViewContainerComponent>;
  const createComponent = createComponentFactory({
    component: SlideViewContainerComponent,
    providers: [
      provideMockStore({}),
      //mockProvider((Store))
    ],
    declarations: [
      ...MockComponents(
        ViewerComponent,
        DndContainerComponent,
        ExtOverviewComponent,
        ScaleLineComponent,
        AnnotationCreateComponent,
        AnnotationLayerComponent,
        AnnotationClusterLayerComponent,
        //ZoomSliderComponent,
        ExtZoomSliderComponent,
        ToolbarComponent,
      ),
    ],
    imports: [
      MaterialModule,
      ReactiveComponentModule
    ]
  });

  beforeEach(() => spectator = createComponent({
    props: {}
  }));

  it('should be defined', () => {
    spectator.inject(MockStore);
    expect(spectator.component).toBeDefined();
  });

  describe('Toolbar rendering', () => {
    it.todo('should render only the zoom-slider');

    it.todo('should render all toolbar elements');
  });
});
