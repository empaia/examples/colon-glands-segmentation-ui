import { Component, ChangeDetectionStrategy, EventEmitter, Output, ViewChild, Input } from '@angular/core';
import { asyncScheduler, Observable } from 'rxjs';
import { distinctUntilChanged, filter, observeOn } from 'rxjs/operators';
import { Annotation } from '../../models/annotation';
import { Store } from '@ngrx/store';

import {
  AnnotationSelectors,
} from '../../store/annotations';
import { SlideSelectors } from '../../store/slide';
import { MouseInteractionType, ToolbarInteractionType } from '../../models/interaction';
import { UiActions, UiSelectors } from '../../store/ui';
import { ClusterSelectors } from '../../store/cluster';
import { CurrentView, ZoomView } from '../../models/ui';
import { Slide } from '../../models/slide';
import { SvmErrorMessage } from '../../models/errors';
import { CharacterLimit } from '../../models/character-limit';
import { CharacterConfigSelectors } from '../../store/character-config';
import { compareDistinct } from '../../models/rxjs-operations';
import { ClassesActions } from '../../store/classes';
import { MatMenuTrigger } from '@angular/material/menu';
import { ToolbarComponent } from '../../components/toolbar/toolbar.component';
import { AnnotationHighlightConfig } from '../../config/annotation-highlight.config';
import { RectangleExplanationSelectors } from '../../store/rectangle-explanations';

@Component({
  selector: 'svm-slide-view-container',
  templateUrl: './slide-view-container.component.html',
  styleUrls: ['./slide-view-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SlideViewContainerComponent {
  public slide$: Observable<Slide>;

  public rectangleExplanations$: Observable<Array<Annotation>>;

  public annotations$: Observable<Array<Annotation>>;
  public discardAnnotations$: Observable<string[]>;
  public clearAnnotations$: Observable<boolean>;
  public highlightedAnnotations$: Observable<Array<string>>;
  public focusedAnnotationId$: Observable<string>;
  public hiddenAnnotationIds$: Observable<string[]>;
  public hiddenRectangleExplanationIds$: Observable<string[]>;

  public selectedDrawTool$: Observable<MouseInteractionType>;
  public currentUiTool$: Observable<MouseInteractionType>;
  public deactivateToolbar$: Observable<boolean>;
  public showAnnotationBar$: Observable<boolean>;

  public zoomView$: Observable<ZoomView>;
  public updateViewer$: Observable<number>;

  public clusterDistance$: Observable<number>;
  public annotationCentroids$: Observable<number[][]>;

  public autoSetAnnotationTitle$: Observable<boolean>;
  public renderHideNavigationButton$: Observable<boolean>;

  public allowedInteractionTypes$: Observable<ToolbarInteractionType[] | undefined>;

  public characterConfig$: Observable<CharacterLimit | undefined>;
  public annotationHighlightConfig$: Observable<AnnotationHighlightConfig>;

  public examinationState$: Observable<boolean | undefined>;

  public xaiAlpha$: Observable<number>;
  public showXaiLayer$: Observable<boolean>;

  @Output() public viewerDoubleClicked = new EventEmitter();
  @Output() public annotationCreated = new EventEmitter<Annotation>();
  @Output() public annotationHover = new EventEmitter<Array<string>>();
  @Output() public errorMessage = new EventEmitter<SvmErrorMessage>();
  @Output() public moveEnd = new EventEmitter<CurrentView>();
  @Output() public toolSelected = new EventEmitter<ToolbarInteractionType>();
  @Output() public annotationClick = new EventEmitter<string[]>();

  public id = 'map1'; // Math.random().toString();
  public id2 = 'map2';

  @ViewChild(ToolbarComponent) toolbar!: ToolbarComponent;
  private readonly TOOLBAR_TOGGLE_TIMEOUT = 50;

  constructor(private store: Store) {
    this.slide$ = this.store.select(SlideSelectors.selectSlide).pipe(
      distinctUntilChanged((prev, curr) => prev?.slideId === curr?.slideId),
    );

    this.zoomView$ = this.store.select(UiSelectors.selectZoomView);

    this.annotations$ = this.store
      .select(AnnotationSelectors.selectRenderAnnotations)
      .pipe(
        filter(a => !!a),
      );

    this.rectangleExplanations$ = this.store
      .select(RectangleExplanationSelectors.selectRenderAnnotations)
      .pipe(
        filter(a => !!a),
      );

    this.discardAnnotations$ = this.store.select(AnnotationSelectors.selectDiscardAnnotationIds);

    this.clearAnnotations$ = this.store.select(AnnotationSelectors.selectClearState).pipe(
      observeOn(asyncScheduler)
    );

    this.highlightedAnnotations$ = this.store.select(AnnotationSelectors.selectHighlighted);

    this.focusedAnnotationId$ = this.store.select(AnnotationSelectors.selectFocused);

    this.hiddenAnnotationIds$ = this.store
      .select(AnnotationSelectors.selectHiddenAnnotationIds)
      .pipe(
        distinctUntilChanged(
          compareDistinct
        )
      );

    this.hiddenRectangleExplanationIds$ = this.store
      .select(RectangleExplanationSelectors.selectHiddenAnnotationIds)
      .pipe(
        distinctUntilChanged(
          compareDistinct
        )
      );

    this.currentUiTool$ = this.store.select(UiSelectors.selectUiTool);

    this.selectedDrawTool$ = this.store.select(UiSelectors.selectUiDrawTool);

    this.clusterDistance$ = this.store.select(ClusterSelectors.selectClusterDistance);

    this.annotationCentroids$ = this.store.select(AnnotationSelectors.selectCentroids);

    this.showAnnotationBar$ = this.store.select(UiSelectors.selectAnnotationBarVisibility);

    this.updateViewer$ = this.store.select(UiSelectors.selectUpdateViewer);

    this.autoSetAnnotationTitle$ = this.store.select(UiSelectors.selectAutoAnnotationTitle);

    this.renderHideNavigationButton$ = this.store.select(UiSelectors.selectRenderHideNavigationButton);

    this.allowedInteractionTypes$ = this.store.select(UiSelectors.selectAllowedInteractionTypes);

    this.characterConfig$ = this.store.select(CharacterConfigSelectors.selectCharacterConfig);

    this.annotationHighlightConfig$ = this.store.select(UiSelectors.selectAnnotationHighlightConfig);

    this.examinationState$ = this.store.select(UiSelectors.selectExaminationState);

    this.xaiAlpha$ = this.store.select(UiSelectors.selectXaiAlpha);
    this.showXaiLayer$ = this.store.select(UiSelectors.selectShowXaiLayerState);
  }

  featureHover(ids: string[]) {
    this.annotationHover.emit(ids);
    this.store.dispatch(ClassesActions.sendHoveredClasses({ annotationIds: ids }));
  }

  annotationCreate(annotation: Annotation): void {
    this.annotationCreated.emit(annotation);
  }

  toolSelectionChanged(mode: ToolbarInteractionType) {
    this.toolSelected.emit(mode);
    this.store.dispatch(UiActions.setUiToolMode({ toolMode: { left: mode } }));
  }

  viewChanged(view: CurrentView) {
    this.moveEnd.emit(view);
  }

  zoomViewChanged(view: ZoomView) {
    this.store.dispatch(UiActions.zoomviewChanged({ zoomview: view }));
  }

  onErrorMessage(message: SvmErrorMessage): void {
    this.errorMessage.emit(message);
  }

  onViewerToolbarChange(): void {
    setTimeout(
      (trigger: MatMenuTrigger) => {
        trigger?.updatePosition();
      },
      this.TOOLBAR_TOGGLE_TIMEOUT,
      this.toolbar?.trigger
    );
    this.viewerDoubleClicked.emit();
  }
}
