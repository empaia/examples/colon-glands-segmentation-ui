import {
  Component,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Slide } from '../../models/slide';
import { Annotation, AnnotationEntity } from '../../models/annotation';
import { CurrentView } from '../../models/ui';
import { logger, LOG_LEVEL } from '../../tools/logger';

import { Store } from '@ngrx/store';
import { SlideActions } from '../../store/slide';
import {
  AnnotationActions,
  AnnotationSelectors
} from '../../store/annotations';
import {
  RectangleExplanationActions,
  RectangleExplanationSelectors
} from '../../store/rectangle-explanations';
import { UiActions } from '../../store/ui';
import { SlideViewerModuleConfiguration } from '../../config/module-config';
import { CLEAR_STORE } from '../../store/meta.reducer';
import { ToolbarInteractionType } from '../../models/interaction';
import { ClusterActions } from '../../store/cluster';
import { UiConfig } from '../../models/viewer-config';
import { CharacterLimit } from '../../models/character-limit';
import { CharacterConfigActions } from '../../store/character-config';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { AnnotationClass } from "../../models/annotation-class";
import { FeatureStyleService } from "../../services/feature-style-service";
import { ClassNamespace } from '../../models/class-namespace';
import { ClassesActions, ClassesSelectors } from '../../store/classes';
import { Class } from '../../models/class';
import { AnnotationHighlightConfig } from '../../config/annotation-highlight.config';

@UntilDestroy()
@Component({
  selector: 'svm-slide-viewer',
  templateUrl: './slide-viewer.component.html',
  styleUrls: ['./slide-viewer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SlideViewerComponent implements OnDestroy {
  private destroy$ = new Subject();

  @Output() public annotationAdded = new EventEmitter<Partial<Annotation>>();
  @Output() public annotationHover = new EventEmitter<string[]>();
  @Output() public annotationClick = new EventEmitter<string[]>();
  @Output() public annotationErrorMessage = new EventEmitter<string>();

  @Output() public slideMoveEnd = new EventEmitter<CurrentView>();
  // @Output() public selectedSlideChanged = new EventEmitter<string>();
  @Output() public doubleClicked = new EventEmitter();
  @Output() public toolSelected = new EventEmitter<ToolbarInteractionType>();
  @Output() public requestAnnotations = new EventEmitter<string[]>();
  @Output() public requestRectangleExplanations = new EventEmitter<string[]>();
  @Output() public annotationClassColors = new EventEmitter<{[key: string]: string}>();
  @Output() public hoveredClasses = new EventEmitter<Class[] | undefined>();

  @Input()
  public set slide(val: Slide) {
    this.store.dispatch(SlideActions.addSlide({ slide: val }));
  }

  @Input()
  public set selectedSlideId(val: string) {
    this.store.dispatch(SlideActions.selectSlideId({ slideId: val }));
  }

  @Input()
  public set annotationIds(val: string[]) {
    this.store.dispatch(
      AnnotationActions.setAnnotationIds({ annotationIds: val })
    );
  }

  @Input()
  public set allAnnotationClasses(annotationClasses: AnnotationClass[]) {
    this.featureStyleService.updateAnnotationClassToColorMapping(annotationClasses);
  }

  @Input()
  public set annotations(val: Array<AnnotationEntity>) {
    if (val && val.length) {
      this.store.dispatch(
        AnnotationActions.setAnnotations({ annotations: val })
      );
    }
  }

  @Input()
  public set hideAnnotationIds(val: string[]) {
    if (val) {
      this.store.dispatch(AnnotationActions.hideAnnotations({ hiddenIds: val }));
    }
  }

  @Input()
  public set removeAnnotations(val: string[]) {
    if (val) {
      this.store.dispatch(AnnotationActions.hideAnnotations({ hiddenIds: val }));
    }
  }

  @Input()
  public set clearAnnotations(val: boolean) {
    if (val) {
      this.store.dispatch(AnnotationActions.clearAnnotations());
    }
  }

  @Input()
  public set highlightAnnotations(val: Array<string>) {
    if (val) {
      this.store.dispatch(AnnotationActions.highlightAnnotations({ ids: val }));
    }
  }

  @Input()
  public set focusAnnotation(val: string) {
    this.store.dispatch(AnnotationActions.focusAnnotation({ id: val }));
  }

  @Input() public set clusterDistance(distance: number) {
    if (distance) {
      this.store.dispatch(ClusterActions.setClusterDistance({
        clusterDistance: distance
      }));
    }
  }

  @Input() public set annotationCentroidsForClustering(clusters: number[][]) {
    if (clusters) {
      this.store.dispatch(AnnotationActions.setCentroids({
        centroids: clusters
      }));
    }
  }

  @Input() public set highlightClasses(val: ClassNamespace[]) {
    if (val && val.length) {
      this.store.dispatch(ClassesActions.setHoveredClasses({ classesEntities: val }));
    }
  }

  @Input() public set uiConfig(config: UiConfig) {
    if (config) {
      this.store.dispatch(
        UiActions.setUIConfig({
          uiConfig: config
        })
      );
    }
  }

  @Input()
  public set rectangleExplanationIds(val: string[]) {
    this.store.dispatch(
      RectangleExplanationActions.setAnnotationIds({ annotationIds: val })
    );
  }

  @Input()
  public set rectangleExplanations(val: Array<AnnotationEntity>) {
    if (val && val.length) {
      this.store.dispatch(
        RectangleExplanationActions.setAnnotations({ annotations: val })
      );
    }
  }

  @Input()
  public set hideRectangleExplanationIds(val: string[]) {
    if (val) {
      this.store.dispatch(RectangleExplanationActions.hideAnnotations({ hiddenIds: val }));
    }
  }

  @Input()
  public set removeRectangleExplanations(val: string[]) {
    if (val) {
      this.store.dispatch(RectangleExplanationActions.hideAnnotations({ hiddenIds: val }));
    }
  }

  @Input()
  public set clearRectangleExplanations(val: boolean) {
    if (val) {
      this.store.dispatch(RectangleExplanationActions.clearAnnotations());
    }
  }


  @Input() public set updateSlideViewer(value: number) {
    this.store.dispatch(UiActions.setUpdateViewer({ update: value }));
  }

  @Input() public set interactionType(interactionType: ToolbarInteractionType) {
    this.store.dispatch(UiActions.setUiToolMode({ toolMode: { left: interactionType } }));
  }

  @Input() public set allowedInteractionTypes(allowedTypes: ToolbarInteractionType[]) {
    this.store.dispatch(UiActions.setAllowedInteractionTypes({ allowedTypes }));
  }

  @Input() public set characterConfig(config: CharacterLimit) {
    this.store.dispatch(CharacterConfigActions.setCharacterConfig({ config }));
  }

  @Input() public set examinationState(examinationClosed: boolean) {
    if (examinationClosed !== null && examinationClosed !== undefined) {
      this.store.dispatch(UiActions.setExaminationState({ examinationClosed }));
    }
  }

  @Input() public set annotationHighlightConfig(annotationHighlightConfig: AnnotationHighlightConfig) {
    this.store.dispatch(UiActions.setAnnotationHighlightConfig({ annotationHighlightConfig }));
  }

  @Input() public set xaiAlpha(xaiAlpha: number) {
    this.store.dispatch(UiActions.setXaiAlpha({ xaiAlpha }));
  }

  @Input() public set showXaiLayer(showXaiLayer: boolean) {
    this.isXaiLayerShown = showXaiLayer
    this.store.dispatch(UiActions.setShowXaiLayer({ showXaiLayer }));
  }

  private isXaiLayerShown = false;

  constructor(
    private store: Store,
    public config: SlideViewerModuleConfiguration,
    private featureStyleService: FeatureStyleService
  ) {
    this.store
      .select(AnnotationSelectors.selectMissingCachedAnnotations)
      .pipe(
        takeUntil(this.destroy$),
        filter(ids => !!ids.missingAnnotations.length)
      )
      .subscribe(ids => this.requestAnnotations.emit(ids.missingAnnotations));

    this.store
      .select(RectangleExplanationSelectors.selectMissingCachedAnnotations)
      .pipe(
        takeUntil(this.destroy$),
        filter(ids => this.isXaiLayerShown && !!ids.missingAnnotations.length)
      )
      .subscribe(ids => this.requestRectangleExplanations.emit(ids.missingAnnotations));

    this.store
      .select(ClassesSelectors.selectHoveredClasses)
      .pipe(takeUntil(this.destroy$))
      .subscribe(classes => this.hoveredClasses.emit(classes));

    if (config.logging === true) {
      logger.setLevel(LOG_LEVEL.VERBOSE);
    }
    this.featureStyleService.annotationClassColorsMappingChange
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe((next) => {
        this.onAnnotationClassColorsMappingChanged(next);
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.store.dispatch(CLEAR_STORE());
  }

  private onAnnotationClassColorsMappingChanged(annotationClassColorsMapping: {[key: string | null]: string}): void {
    this.annotationClassColors.emit(annotationClassColorsMapping);
  }

}
