import { Tile } from 'ol';

/**
 * public interface defining a Slide ressource
 */
export interface Slide {
  slideId: string;
  resolver: TileResolver;
  imageInfo: ImageInfo;
}

export interface ImageInfo {
  width: number;
  height: number;
  tileSizes: number[][];
  npp: number;
  numberOfLevels: number;
  resolutions: number[];
  levels?: Level[];
  numberOfOverZoomLevels?: number;
}

export interface Level {
  dimensions: number[];
  downsample: number;
  npp?: number;
  tiles?: number[];
}

/**
 * the App is responsible for implementing this function
 * the implementation of this function defines how the image tiles are resolved
 * the return value of this function should be the route for fetching image tiles
 */
export type TileResolverFn = (coords: number[]) => string;

/**
 * this function should fetch image tiles by itself
 * https://openlayers.org/en/latest/apidoc/module-ol_Tile.html#~LoadFunction
 */
export type TileLoaderFn = (p0: Tile, p1: string) => void;

/**
 * public interface which the App should implement
 * to resolve deep zoom tiles
 * see demo app - file: slide-server-tile-loader.ts for
 * implementation example
 */
export interface TileResolver {
  tileResolverFn: TileResolverFn | null;
  tileLoaderFn: TileLoaderFn | null;
  startZoom: number;
  imageSourceUrl: string;
  imageId: string;
  numberOfLevels: number;
  hasArtificialLevel?: boolean;
}
