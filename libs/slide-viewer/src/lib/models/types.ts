// import { InfoBar } from './ui';

export const PROJECTION_KEY = 'pixels';
export const PROJECTION_M_TO_NM = 1e9; // 1000000000.0  - from meters to nanometers
export const PROJECTION_NM_TO_M = 1e-9;  // 0.000000001 - from nm to m
// used in ext-overview
export const ZOOMBOX_ID = 'zoombox';
export const VIEW_MOVED_BY = 'movedBy';

export const NPP_MIN_RANGE_FACTOR = 0.75;
export const NPP_MAX_RANGE_FACTOR = 1.5;
