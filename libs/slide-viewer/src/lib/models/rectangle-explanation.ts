import { Annotation, AnnotationEntity } from './annotation';


export class RectangleExplanation extends Annotation {
  importance: number;

  constructor(opt: RectangleExplanation) {
    super(opt);
    this.importance = opt.importance;
  }
}

export interface RectangleExplanationEntity extends AnnotationEntity {
  importance: number;
}
