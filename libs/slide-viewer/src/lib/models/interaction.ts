export enum DrawType {
  Circle = 'circle',
  Rectangle = 'rectangle',
  LineString = 'lineString',
  Polygon = 'polygon',
  PolygonAlternative = 'polygonAlternative',
  Point = 'point',
}

export enum InteractionType {
  Move = 'move',
  Measure = 'measure',
  Select = 'select', // TODO: implement
}

export enum MouseButtonType {
  None,
  Left,
  Right,
}

export type ToolbarInteractionType = DrawType | InteractionType;

export interface MouseInteractionType {
  left?: ToolbarInteractionType | undefined;
  right?: ToolbarInteractionType | undefined;
}
