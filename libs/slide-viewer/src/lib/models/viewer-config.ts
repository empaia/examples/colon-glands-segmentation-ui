export interface UiConfig {
  showAnnotationBar: boolean;
  autoSetAnnotationTitle: boolean;
  renderHideNavigationButton: boolean;
}
