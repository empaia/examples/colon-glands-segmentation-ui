import { Coordinate } from 'ol/coordinate';
import { Extent } from 'ol/extent';

export interface CurrentView {
  extent: Extent;
  zoom: number;
  nppCurrent: number;
  nppMin: number;
  nppMax: number;
  nppBase: number;
}

export interface ZoomView {
  extent: Extent;
  resolution: number;
  sender: ViewObserver;
  receivers: ViewObserver[];
}

export enum ViewObserver {
  None,
  MainMap,
  OverviewMap,
  ZoomSlider,
}

/**
 * UNUSED
 * public interface to configure viewer
 * TODO: Implement behavior of the viewer depending on this interface
 */
export interface ViewerConfig {
  numWindows: number;
  showSlideNavigation: boolean;
  showClassView: boolean;
}

export interface IAnnotationCard {
  titel: string;
  description?: string;
}

/**
 *
 */
export class Popup {
  private id: number;
  constructor(private coordinates: Coordinate, private element: HTMLElement) {}
  getElement() {
    return this.element;
  }
  getCoordinates(): Coordinate {
    return this.coordinates;
  }
  getId(): number {
    return this.id;
  }
}

export type AnnotationStyleMode = "DEFAULT" | "DRAWING" | "HOVER";
