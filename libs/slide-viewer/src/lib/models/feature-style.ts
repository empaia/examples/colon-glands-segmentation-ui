import { Style } from 'ol/style';

export type ColorName = 'blue'|'turquoise'|'dark_green'|'light_green'|'red'|'magenta'|'dark_orange'|'orange'|'yellow';
export type ColorSequenceRecord = Record<ColorName, Array<string>>;

export interface AnnotationPalette {
  hover: string;
  roi: string;
  drawing: string;
  annotationClasses: string[];
}

export type FeatureStyleMode = 'DEFAULT'|'DRAWING'|'HOVER'|'ANNOTATION_CLASSES';
export type FeatureStyleEntry = [Style, Style];
export type FeatureStylesList = Array<FeatureStyleEntry>;
export type FeatureStyle = Record<FeatureStyleMode, FeatureStyleEntry|FeatureStylesList>;
