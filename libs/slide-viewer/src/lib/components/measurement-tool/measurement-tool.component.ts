import { AfterViewInit, Component, ElementRef, Input, Renderer2, } from '@angular/core';
import { DrawEvent } from 'ol/interaction/Draw';
import Overlay from 'ol/Overlay';
import { EventsKey } from 'ol/events';
import OlMap from 'ol/Map';
import { Draw } from 'ol/interaction';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import { DrawType, InteractionType } from '../../models/interaction';
import BaseEvent from 'ol/events/Event';
import { Geometry, LineString } from 'ol/geom';
import { Feature } from 'ol';
import { OpenLayersDrawFactory } from '../../tools/ol-draw-factory';
import { unByKey } from 'ol/Observable';
import { Fill, Stroke, Style } from 'ol/style';
import { PROJECTION_M_TO_NM } from '../../models/types';

@Component({
  selector: 'svm-measurement-tool',
  templateUrl: './measurement-tool.component.html',
  styleUrls: ['./measurement-tool.component.scss'],
  // eslint-disable-next-line @angular-eslint/no-host-metadata-property
  host: {
    '(document:keydown)': 'onKeyPressed($event)',
  },
})
export class MeasurementToolComponent implements  AfterViewInit {
  private viewerHost: OlMap;
  private draw: Draw;
  private source: VectorSource<Geometry>;
  private layer: VectorLayer<VectorSource<Geometry>>;
  private measureTooltip: Overlay;
  private listener: EventsKey;
  private sketch: Feature<Geometry>;

  private _isActive: boolean;
  @Input() public get isActive() {
    return this._isActive;
  }

  public set isActive(val: boolean) {
    this._isActive = val;
    if (val) {
      this.setMeasureActive();
    } else {
      this.disableDraw();
    }
  }

  private measureStyle = new Style({
    fill: new Fill({
      color: 'rgba(255, 255, 255, 0.2)',
    }),
    stroke: new Stroke({
      color: 'rgba(0, 0, 0, 0.5)',
      lineDash: [10, 10],
      width: 3,
    }),
  });

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService,
    private renderer: Renderer2,
    private measureTooltipElement: ElementRef
  ) {}


  ngAfterViewInit(): void {
    this.initDraw();
  }

  initDraw(): void {
    this.source = new VectorSource({ wrapX: false });
    this.layer = new VectorLayer({
      source: this.source,
      style: this.measureStyle,
    });
    this.viewerHost = this.mapService.getMap(this.mapIdService);

    this.layer.setVisible(true);
    this.layer.setZIndex(10);

    this.viewerHost.addLayer(this.layer);
    this.createMeasureTooltip();
  }

  setMeasureActive(): void {
    if (!this.viewerHost) {
      return;
    }

    const mode = DrawType.LineString;
    this.disableDraw();
    const openLayersDraw = OpenLayersDrawFactory.fromDrawType(
      mode,
      this.source,
      { left: InteractionType.Measure },
    );
    this.draw = openLayersDraw.getDraw();

    this.viewerHost.addInteraction(this.draw);

    this.draw.on('drawstart', (event: DrawEvent) => this.onMeasureStart(event));
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    this.draw.on('drawabort', (event: DrawEvent) => this.onMeasureAbort());
    this.draw.on('drawend', (event: DrawEvent) => this.onMeasureEnd(event));
  }

  disableDraw(): void {
    if (!this.viewerHost) {
      return;
    }

    this.viewerHost.removeInteraction(this.draw);
    if (this.draw) {
      this.draw.abortDrawing();
      this.draw.setActive(false);
    }
  }

  onMeasureStart(event: DrawEvent): void {
    this.sketch = event.feature;
    this.listener = this.sketch.getGeometry().on('change', (evt: BaseEvent) => {
      const geometry = evt.target;
      if (geometry instanceof LineString) {
        const output = this.formatLength(geometry);
        const coordinates = geometry.getLastCoordinate();
        this.measureTooltipElement.nativeElement.innerHTML = output;
        this.measureTooltip.setPosition(coordinates);
      }
    });
  }

  onMeasureAbort(): void {
    this.createMeasureTooltip();
  }

  onMeasureEnd(event: DrawEvent): void {
    this.viewerHost.removeInteraction(this.draw);
    this.sketch = event.feature;
    const geometry = event.feature.getGeometry();
    if (geometry instanceof LineString) {
      this.measureTooltipElement.nativeElement.className =
        'ol-tooltip ol-tooltip-static';
      this.measureTooltip.setOffset([0, -7]);
      this.measureTooltipElement.nativeElement = null;
      this.createMeasureTooltip();
      this.sketch = undefined;
    }

    unByKey(this.listener);
  }

  onKeyPressed(event: KeyboardEvent): void {
    if (event.code === 'Escape' && this.draw && this.draw.getActive()) {
      this.draw.abortDrawing();
    }
  }

  private formatLength(line: LineString): string {
    const npp =
      this.viewerHost.getView().getProjection().getMetersPerUnit() * PROJECTION_M_TO_NM;
    const length = line.getLength() * npp;

    let output: string;
    if (length >= 1e7) {
      output = (length / 1e7).toFixed(2) + ' cm';
    } else if (length >= 1e6) {
      output = (length / 1e6).toFixed(2) + ' mm';
    } else if (length >= 1e3) {
      output = (length / 1e3).toFixed(2) + ' \u00b5m';
    } else {
      output = length.toFixed(2) + ' nm';
    }
    return output;
  }

  private createMeasureTooltip(): void {
    if (this.measureTooltipElement.nativeElement) {
      this.measureTooltipElement.nativeElement.parentNode.removeChild(
        this.measureTooltipElement.nativeElement
      );
      this.measureTooltipElement.nativeElement = this.renderer.createElement(
        'div'
      ) as HTMLDivElement;
      this.measureTooltipElement.nativeElement.className =
        'ol-tooltip ol-tooltip-measure';
      this.measureTooltip = new Overlay({
        element: this.measureTooltipElement.nativeElement,
        offset: [0, -15],
        positioning: 'bottom-center',
      });
      this.viewerHost.addOverlay(this.measureTooltip);
    }
  }
}
