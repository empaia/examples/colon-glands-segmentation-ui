import {
  AfterViewInit,
  ChangeDetectionStrategy,
  EventEmitter,
  Output
} from '@angular/core';
import { Component, Input } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import OlMap from 'ol/Map';
import { Slide } from '../../models/slide';
import { ViewObserver, ZoomView } from '../../models/ui';
import { MapIdService } from '../../services/map-id.service';
import { MapService } from '../../services/map.service';

@Component({
  selector: 'svm-ext-zoom-slider',
  templateUrl: './ext-zoom-slider.component.html',
  styleUrls: ['./ext-zoom-slider.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExtZoomSliderComponent implements AfterViewInit {
  @Input() mapId: string;

  public zoomValue: number;
  public zoomMax: number;
  public zoomMin: number;
  public zoomLevels: number[];

  @Output() public readonly zoomViewUpdated = new EventEmitter<ZoomView>();

  private _zoomView: ZoomView;
  @Input() public get zoomView() {
    return this._zoomView;
  }

  public set zoomView(val: ZoomView) {
    if (val) {
      this.updateZoomSlider(val);
    }
    this._zoomView = val;
  }

  private _slide: Slide;
  @Input() public get slide() {
    return this._slide;
  }

  public set slide(val: Slide) {
    this.setMinMaxZoom(val);
    this._slide = val;
  }

  private viewerHost: OlMap;

  constructor(
    private mapService: MapService,
    private mapIdService: MapIdService,
  ) { }


  ngAfterViewInit(): void {
    this.viewerHost = this.mapService.getMap(this.mapIdService || this.mapId);
    // alternative to using the ngrx store for view updates
    // this.viewerHost.set('ext-overview',this);

    // initialize on remount as setter runs before map layer can initialize
    this.setMinMaxZoom(this.slide);
  }

  setMinMaxZoom(slide: Slide) {
    if (slide) {
      this.zoomMin = 0;
      // calculate resolutions inbetween with step length 2^n
      this.zoomLevels = [];
      for (let i = 0; 2 ** i <= Math.max.apply(null, slide.imageInfo.resolutions); i++) {
        this.zoomLevels.push(2 ** i);
      }
      this.zoomMax = this.zoomLevels.length - 1;
    }
  }

  updateZoomSlider(zoomView: ZoomView) {
    if (zoomView.receivers.includes(ViewObserver.ZoomSlider)) {
      const log2n = Math.log(zoomView.resolution) / Math.log(2);
      this.zoomValue = log2n;//zoomView.resolution;
    }
  }

  zoomIn() {
    if (this.zoomLevels) {
      const zoomInLevels = this.zoomLevels.filter(x => x < (2 ** this.zoomValue));
      if (zoomInLevels.length > 0) {
        const log2n = Math.log(zoomInLevels[zoomInLevels.length - 1]) / Math.log(2);
        this.zoomValue = log2n;
        const resolution = zoomInLevels[zoomInLevels.length - 1];
        // this.updateMapByResolution(resolution);
        this.setViewerHostResolution(resolution);
      }
    }
  }

  zoomOut() {
    if (this.zoomLevels) {
      const zoomOutLevels = this.zoomLevels?.filter(x => x > (2 ** this.zoomValue));
      if (zoomOutLevels.length > 0) {
        const log2n = Math.log(zoomOutLevels[0]) / Math.log(2);
        this.zoomValue = log2n;
        const resolution = zoomOutLevels[0];
        // this.updateMapByResolution(resolution);
        // update host map directly
        this.setViewerHostResolution(resolution);
      }
    }
  }

  sliderChanged(event: MatSliderChange) {
    this.zoomValue = event.value;
    const resolution = 2 ** event.value;
    // this.updateMapByResolution(resolution);
    // update host map directly
    this.setViewerHostResolution(resolution);
  }

  setViewerHostResolution(resolution: number) {
    this.viewerHost.getView().setResolution(resolution);
  }

  // updateMapByResolution(resolution: number) {
  //   this.zoomViewUpdated.emit({
  //     extent: null,
  //     resolution: resolution,
  //     sender: ViewObserver.ZoomSlider,
  //     receivers: [ViewObserver.MainMap]
  //   });
  // }
}
