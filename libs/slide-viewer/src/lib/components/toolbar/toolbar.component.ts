import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input, OnChanges,
  Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  DrawType,
  InteractionType,
  ToolbarInteractionType,
} from '../../models/interaction';
import { AfterViewInit } from '@angular/core';
import { MatMenuTrigger } from "@angular/material/menu";
import { FeatureStyleService } from '../../services/feature-style-service';
import { AnnotationPalette } from '../../models/feature-style';
import { MatButtonToggleChange } from '@angular/material/button-toggle';

@Component({
  selector: 'svm-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements AfterViewInit, OnChanges {

  private _renderNavigationButton: boolean;

  @Input() public set renderHideNavigationButton(val: boolean) {
    this._renderNavigationButton = val;
  }

  @Input() public activeTool: ToolbarInteractionType = InteractionType.Move;

  @Input() public allowedTools: ToolbarInteractionType[] | undefined = undefined;

  @Input() public isExaminationClosed: boolean | undefined = undefined;

  @Output() public selectedTool = new EventEmitter<ToolbarInteractionType>();
  @Output() public toggleAppNavigation = new EventEmitter<void>();

  public navigationHidden = false;
  public annotationStrokeWidth: number;
  public annotationPalettes: AnnotationPalette[];
  public annotationClassesPalettesMenuVisible = false;
  public allowedToolsSet: Set<ToolbarInteractionType>;

  readonly DrawType = DrawType;
  readonly InteractionType = InteractionType;

  @ViewChild('hideNavigation') hideNavigationButton!: ElementRef;
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  constructor(private featureStyleService: FeatureStyleService) {
    this.annotationPalettes = this.featureStyleService.getAnnotationPalettes();
  }

  ngAfterViewInit(): void {
    this.annotationStrokeWidth = this.featureStyleService.getStrokeWidth();
    this.hideNavigationButton.nativeElement.style.display = this._renderNavigationButton? 'block' : 'none';
    this.trigger.menuOpened.subscribe(() => {
      this.annotationClassesPalettesMenuVisible = true;
    });
    this.trigger.menuClosed.subscribe(() => {
      this.annotationClassesPalettesMenuVisible = false;
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.allowedTools) {
      this.allowedToolsSet = new Set<ToolbarInteractionType>(this.allowedTools);
    }
  }

  onToggleSelectionChange(event: MatButtonToggleChange): void {
    this.activeTool = event.value;
    this.selectedTool.emit(this.activeTool);
  }

  toggleNavigation() {
    this.navigationHidden = !this.navigationHidden;
    this.toggleAppNavigation.emit();
  }

  annotationStrokeWidthSliderChanged(value: number): void {
    this.featureStyleService.setStrokeWidth(value);
  }

  isCurrentAnnotationColorPalette(annotationPalette: AnnotationPalette): boolean {
    const index = this.featureStyleService.getAnnotationPalettes().indexOf(annotationPalette);
    return index === this.featureStyleService.getAnnotationPaletteIndex();
  }

  activateAnnotationColorPalette(annotationPalette: AnnotationPalette, $event: MouseEvent): void {
    $event.stopPropagation();
    const index = this.featureStyleService.getAnnotationPalettes().indexOf(annotationPalette);
    this.featureStyleService.setAnnotationPaletteIndex(index);
  }

  onClosePalette(): void {
    if (this.trigger.menuOpen) {
      this.trigger.closeMenu();
    }
  }
}
