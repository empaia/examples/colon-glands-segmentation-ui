import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Host,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { Feature, Map as OlMap, MapBrowserEvent } from 'ol';
import { FeatureLike } from 'ol/Feature';
import VectorSource from 'ol/source/Vector';
import VectorImageLayer from 'ol/layer/VectorImage';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import { Annotation } from '../../models/annotation';
import { logger } from '../../tools/logger';
import { AnnotationToOlFeature } from '../../tools/annotation-to-olfeature';
import { ViewerCalculationsService } from '../../services/viewer-calculations.service';
import { ANNOTATION_FOCUS_ERROR_MSG, SvmErrorMessage } from '../../models/errors';
import { Geometry } from 'ol/geom';
import { FeatureStyleService } from "../../services/feature-style-service";
import { FeatureStyleEntry } from "../../models/feature-style";
import { AnnotationHighlightConfig, DEFAULT_ANNOTATION_HIGHLIGHT_CONFIG, HighlightType } from '../../config/annotation-highlight.config';
import { Subject, takeUntil } from 'rxjs';

/**
 * Layer that displays annotations on a Slide.
 * Reacts to hide/show/hover events of Annotation List.
 * Handles Zooming to an annotation
 */
@Component({
  selector: 'svm-annotation-layer',
  templateUrl: './annotation-layer.component.html',
  styleUrls: ['./annotation-layer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnnotationLayerComponent implements AfterViewInit, OnDestroy {
  private viewerHost: OlMap;
  private source: VectorSource<Geometry>;
  private layer: VectorImageLayer<VectorSource<Geometry>>;

  // viewer hover (internal)
  private _hoveredFeaturesList = new Array<Feature<Geometry>>();
  // external highlight (sidenav list)
  private _highlightFeaturesList = new Array<Feature<Geometry>>();

  private destroy$ = new Subject();

  // set parameter of EventEmitter to true to trigger the change detection
  // and avoiding ExpressionChangedAfterItHasBeenCheckedError
  @Output() public errorMessage = new EventEmitter<SvmErrorMessage>(true);

  private _focusFeature: string;

  @Input() public set addAnnotations(val: Annotation[]) {
    if (val.length) {
      this.addNewAnnotations(val);
    }
  }

  @Input() public set removeAnnotations(val: string[]) {
    if (val.length) {
      this.removeFeatures(val);
    }
  }

  @Input() public set clearAnnotations(val: boolean) {
    if (val) {
      this.removeAllFeatures();
    }
  }

  @Input() public set hideAnnotations(val: string[]) {
    if (val.length) {
      this.removeFeatures(val);
    }
  }

  private _highlightedFeatures: string[];
  @Input() public get highlightedFeatures(): string[] {
    return this._highlightedFeatures;
  }
  public set highlightedFeatures(val: string[]) {
    if (!val) {
      return;
    }
    this.highlightFeatures(val);
  }

  @Input() public get focusFeature(): string {
    return this._focusFeature;
  }
  public set focusFeature(val: string) {
    if (!val) {
      return;
    }
    this.zoomToAnnotation(val);
  }

  @Input() public highlightConfig: AnnotationHighlightConfig = DEFAULT_ANNOTATION_HIGHLIGHT_CONFIG;

  @Input() public set toggleExplanations(show: boolean) {
    this.layer.setVisible(show);
  }

  @Output() public readonly hoveredFeatures = new EventEmitter<string[]>();
  @Output() public readonly clickedFeature = new EventEmitter<string[]>();

  constructor(
    private mapService: MapService,
    @Host()
    private mapIdService: MapIdService,
    private viewerCalculationService: ViewerCalculationsService,
    private featureStyleService: FeatureStyleService
  ) {
    this.featureStyleService.annotationStrokeWidthChange
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.onFeatureStyleChange());
    this.featureStyleService.annotationPaletteChange
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.onFeatureStyleChange());
  }


  public ngAfterViewInit(): void {
    const id = this.mapIdService;
    this.viewerHost = this.mapService.getMap(id);
    this.initializeLayer();

    this.initializeEventListener();
  }

  public ngOnDestroy(): void {
    this.removeAllFeatures();
    this.destroy$.next(null);
  }

  private initializeLayer() {
    this.source = new VectorSource({
      useSpatialIndex: false,
      wrapX: false,
    });

    const featureStyleService = this.featureStyleService;

    this.layer = new VectorImageLayer({
      renderBuffer: 10,
      source: this.source,
      style: function (feature) {
        let style: FeatureStyleEntry;
        if (feature.get('is_roi')) {
          style = featureStyleService.getFeatureStyle().DEFAULT as FeatureStyleEntry;
        } else {
          const classIndex: number = feature.get('class_index');
          const classList = featureStyleService.getFeatureStyle().ANNOTATION_CLASSES;
          style = ((classIndex !== undefined && classIndex < classList.length)
            ? classList[classIndex]
            : classList[classList.length - 1]) as FeatureStyleEntry;
        }
        return style;
      },
    });

    this.layer.setZIndex(101);
    this.viewerHost.addLayer(this.layer);
  }

  private initializeEventListener() {
    switch(this.highlightConfig.highlightType) {
      case HighlightType.CLICK:
        this.setHighlightOnClickEventListener();
        break;
      case HighlightType.OVER:
        this.setHighlightOnHoverEventListener();
        break;
      default:
        this.setHighlightOnHoverEventListener();
    }
  }

  private setHighlightOnHoverEventListener(): void {
    let throttleFlag = true;
    this.viewerHost.on('pointermove', (event: MapBrowserEvent<UIEvent>) => {
      if(!event.dragging) {
        // we trigger hover function every 50ms to reduce computational expensive operations
        if(throttleFlag) {
          this.onFeatureHover(event);
          throttleFlag = false;
          setTimeout(() => throttleFlag = true, 50);
        }
      }
      return true;
    });
    this.viewerHost.on('singleclick', (event: MapBrowserEvent<UIEvent>) => {
      this.onFeatureClick(event);
      return true;
    });
  }

  private setHighlightOnClickEventListener(): void {
    this.viewerHost.on('singleclick', (event: MapBrowserEvent<UIEvent>) => {
      this.highlightOnFeatureClick(event);
      this.onFeatureClick(event);
      return true;
    });
  }

  public addNewAnnotations(annotationList: Array<Annotation>): void {
    if (!this.source) {
      return;
    }

    const features = this.transformAnnotationsToFeatures(annotationList);
    this.source.addFeatures(features);
  }

  public transformAnnotationsToFeatures(annotations: Annotation[]): Feature<Geometry>[] {
    return annotations.map(annotation => {
      const feature = AnnotationToOlFeature.annotationToFeature(annotation);
      AnnotationToOlFeature.addAnnotationInfoToFeature(annotation, feature);
      if (annotation.classes && annotation.classes.length > 0) {
        const firstClass = annotation.classes.map(it => it.value)[0];
        if (firstClass.endsWith('.roi')) {
          feature.set('is_roi', true);
        } else {
          let classIndex = this.featureStyleService.getAnnotationClassColorIndex(firstClass);
          if (classIndex < 0) {
            // wbs v1 "ead"-route probably not available yet, or ead did not specify any class,
            // use the next available color index and persist it for this class:
            classIndex = this.featureStyleService.addAnnotationClassToColorMapping(firstClass);
          }
          feature.set('class_index', classIndex);
        }
      }
      return feature;
    });
  }

  public highlightFeatures(annotationIds: string[]) {
    this.clearAllFeatureHighlights();
    this.highlightAnnotations(annotationIds);
  }

  private highlightAnnotations(ids: string[]): void {
    ids.forEach((id) => {
      if (id && this.source) {
        const feat = this.source.getFeatureById(id);
        if (feat) {
          this._highlightFeaturesList.push(feat);
          feat.setStyle(this.featureStyleService.getFeatureStyle().HOVER as FeatureStyleEntry);
        }
      }
    });
  }

  private clearAllFeatureHighlights(): void {
    this._highlightFeaturesList.forEach(f => f.setStyle());
    this._highlightFeaturesList = [];
  }

  public removeAllFeatures(): void {
    if (!this.source) {
      return;
    }
    this.source.clear(true);
  }

  public removeFeatures(annotationIds: string[]): void {
    annotationIds.forEach(annotationId => {
      const feature = this.source.getFeatureById(annotationId);
      if (feature) {
        this.source.removeFeature(feature);
      }
    });
  }

  private zoomToAnnotation(annotationId: string): void {
    if (annotationId) {
      const feat = this.source.getFeatureById(annotationId);
      // if the feature is found zoom to it
      if (feat) {
        const featGeometry = feat.getGeometry();
        const imageExtent = this.viewerCalculationService.getImageExtentFromSourceLayers(this.viewerHost.getLayers());

        if (this.viewerCalculationService.isGeometryInExtent(featGeometry, imageExtent)) {
          const view = this.viewerHost.getView();
          view.fit(featGeometry.getExtent());
        } else {
          // annotation is outside the image boundaries
          this.errorMessage.emit(ANNOTATION_FOCUS_ERROR_MSG);
        }
      } else {
        logger.error(
          '[LayerComponent] Focused annotation not Found! id:',
          annotationId
        );
      }
    }
  }

  private onFeatureStyleChange(): void {
    this.layer.changed();
  }

  private onFeatureHover(event: MapBrowserEvent<UIEvent>): void {
    const prev: Set<string> = new Set(
      this._hoveredFeaturesList.map((f) => f.getId() as string)
    );
    const curr: Set<string> = new Set();

    try {
      this.viewerHost.forEachFeatureAtPixel(event.pixel, (feat: FeatureLike) => {
        // the point indicating that we are in draw mode does not have a id set
        // every other annotation should have an id set via addAnnotationInfoToFeature function
        const id = feat.getId() as string;
        if (id) {
          curr.add(id);
        }
      });
    } catch (ignoreError) {
      // the hover method has no possibility to check whether a feature was discarded
      // or is still on the map. if the feature does not exist, it will throw a
      // type error that will be ignored.
    }

    const same = this.sameHighlightSets(prev, curr);
    if (same) {
      return;
    }
    this.resetStyleOfHoverFeatures();

    curr.forEach((id) => {
      if (id) {
        const feat = this.source.getFeatureById(id);
        if (feat) {
          this._hoveredFeaturesList.push(feat);
          feat.setStyle(this.featureStyleService.getFeatureStyle().HOVER as FeatureStyleEntry);
        }
      }
    });

    // notify sidenav annotation list about hovered annotation
    this.hoveredFeatures.emit(curr.size > 0 ? [...curr] : []);
  }

  private sameHighlightSets(prev: Set<string>, current: Set<string>): boolean {
    if (prev.size !== current.size) {
      return false;
    }
    for (const a of prev) {
      if (!current.has(a)) {
        return false;
      }
    }
    return true;
  }

  private onFeatureClick(event: MapBrowserEvent<UIEvent>): void {
    const annotations = new Set<string>(
      // only add features to annotation with an id. Otherwise it's a cluster
      this.viewerHost.getFeaturesAtPixel(event.pixel).filter(f => f.getId()).map(f => f.getId() as string)
    );
    this.clickedFeature.emit(annotations.size ? [...annotations] : []);
  }

  private highlightOnFeatureClick(event: MapBrowserEvent<UIEvent>): void {
    this.resetStyleOfHoverFeatures();
    this._hoveredFeaturesList = this.viewerHost.getFeaturesAtPixel(event.pixel).filter(f => f.getId()) as Feature<Geometry>[];
    this._hoveredFeaturesList.forEach(feature => feature.setStyle(this.featureStyleService.getFeatureStyle().HOVER as FeatureStyleEntry));
  }

  private resetStyleOfHoverFeatures(): void {
    this._hoveredFeaturesList.forEach(f => f.setStyle());
    this._hoveredFeaturesList = [];
  }
}
