/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnotationLayerComponent } from './annotation-layer.component';

describe('LayerComponent', () => {
  let component: AnnotationLayerComponent;
  let fixture: ComponentFixture<AnnotationLayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LayerComponent],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnotationLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
