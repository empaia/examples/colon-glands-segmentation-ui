import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ElementRef,
  ViewChild,
  Input,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormControl,
  Validators,
  UntypedFormGroup,
  AbstractControl,
} from '@angular/forms';
import { AnnotationCharacterLimit } from '../../models/character-limit';
import { IAnnotationCard } from '../../models/ui';
import { ANNOTATION_FORM_ERROR_MESSAGES } from '../../models/form-errors';
import { AnnotationCardErrorStateMatcher } from '../../models/error-matcher';

@Component({
  selector: 'svm-create-annotation-card',
  templateUrl: './create-annotation-card.component.html',
  styleUrls: ['./create-annotation-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateAnnotationCardComponent implements OnInit {
  @Input() annotationCharacterConfig: AnnotationCharacterLimit | undefined = undefined;
  @Input() defaultTitle: string | undefined = undefined;

  @Output() cancelClick = new EventEmitter();
  @Output() saveClick = new EventEmitter<IAnnotationCard>();

  private formBuilder = new UntypedFormBuilder();
  public formGroup!: UntypedFormGroup;
  public matcher = new AnnotationCardErrorStateMatcher();

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      title: new UntypedFormControl(
        this.defaultTitle ?? '',
        this.annotationCharacterConfig
          ? [Validators.required, Validators.maxLength(this.annotationCharacterConfig.name)]
          : Validators.required
      ),
      description: new UntypedFormControl(
        '',
        this.annotationCharacterConfig
          ? [Validators.maxLength(this.annotationCharacterConfig.description)]
          : undefined
      ),
    });
  }

  @ViewChild('inputAutoFocus', { static: false })
  set inputAutoFocus(element: ElementRef<HTMLInputElement>) {
    if (element) {
      element.nativeElement.focus();
    }
  }

  saveAnnotationClick() {
    this.saveClick.emit({
      titel: this.title.value,
      description: this.description.value,
    });
  }

  get title(): UntypedFormControl {
    return this.formGroup?.get('title') as UntypedFormControl;
  }

  get description(): UntypedFormControl {
    return this.formGroup?.get('description') as UntypedFormControl;
  }

  getErrors(control: AbstractControl, name: string): string[] | null {
    const messages = ANNOTATION_FORM_ERROR_MESSAGES[name];
    return Object.keys(control.errors).map(error => messages[error]);
  }
}
