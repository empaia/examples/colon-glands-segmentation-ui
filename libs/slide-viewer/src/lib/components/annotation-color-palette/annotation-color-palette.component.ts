import {Component, Input} from '@angular/core';
import {FeatureStyleService} from "../../services/feature-style-service";
import {AnnotationPalette} from "../../models/feature-style";

@Component({
  selector: 'svm-annotation-color-palette',
  templateUrl: './annotation-color-palette.component.html',
  styleUrls: ['./annotation-color-palette.component.scss']
})
export class AnnotationColorPaletteComponent {
  @Input() annotationPalette: AnnotationPalette;

  constructor(private featureStyleService: FeatureStyleService) {
    const index = this.featureStyleService.getAnnotationPaletteIndex();
    this.annotationPalette = this.featureStyleService.getAnnotationPalette(index);
  }
}
