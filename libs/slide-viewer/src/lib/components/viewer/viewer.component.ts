import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter, HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';

import { Map, MapBrowserEvent, MapEvent, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import { getCenter } from 'ol/extent';
import { XYZ } from 'ol/source';
import Projection from 'ol/proj/Projection';
import { addProjection as olAddProjection } from 'ol/proj';

import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import { Slide } from '../../models/slide';
import { CurrentView, ViewObserver, ZoomView } from '../../models/ui';
import { PROJECTION_KEY, PROJECTION_NM_TO_M, VIEW_MOVED_BY } from '../../models/types';
import { SlideSourceLoader } from '../../services/slide-source';
import { ViewerCalculationsService } from '../../services/viewer-calculations.service';
import { AxisConverter } from '../../tools/axis-converter';
import { DoubleClickZoom, DragPan } from 'ol/interaction';
import { noModifierKeys } from 'ol/events/condition';
import { MouseButtonType } from '../../models/interaction';

@Component({
  selector: 'svm-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss'],
  providers: [MapIdService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewerComponent implements OnInit, AfterViewInit, OnDestroy {
  public map: Map;
  public layer: TileLayer<XYZ>;

  public elementHeight = '100%';
  public elementWidth = '100%';

  @Input() mapId: string;

  private _slide: Slide;
  @Input() public set slide(val: Slide) {
    if (val) {
      this.initSlide(val);
    } else {
      this.clearSlide();
    }
    this._slide = val;
  }

  public get slide() {
    return this._slide;
  }

  private _updateViewer: number;
  @Input() public set updateViewer(val: number) {
    if (val !== undefined && this.map) {
      // TODO: fix using timeout - this behavior depends on speed of host system (slow system could take longer than 200ms to register changes)
      setTimeout(() => this.map.updateSize(), 200);
      //this.map.updateSize();
    }
  }
  public get updateViewer() {
    return this._updateViewer;
  }

  private _zoomView: ZoomView;
  @Input() public set zoomView(val: ZoomView) {
    if (val) {
      this.updateMapByZoomView(val);
    }
    this._zoomView = val;
  }
  public get zoomView() {
    return this._zoomView;
  }

  @Output() public readonly moveEnd = new EventEmitter<CurrentView>();
  @Output() public readonly moveEndZoomView = new EventEmitter<ZoomView>();
  @Output() public readonly doubleClick = new EventEmitter<MapBrowserEvent<UIEvent>>();

  constructor(
    private host: ElementRef,
    private mapService: MapService,
    private mapIdService: MapIdService,
    private viewerCalculationsService: ViewerCalculationsService,
  ) {
  }

  ngOnInit(): void {
    this.mapIdService.setId(this.mapId);
    this.map = this.mapService.getMap(this.mapId);

    if (!this.mapId) {
      this.mapId = 'map';
    }
    this.map.setTarget(this.host.nativeElement.firstElementChild);

    this.addLeftAndRightMouseButtonToMove();

    this.removeDoubleClickToZoomInteraction();

    this.map.on('pointerdrag', (event: MapBrowserEvent<UIEvent>) => {
      this.pointerDragEventCallback(event);
    });
    this.map.on('moveend', (event: MapEvent) => {
      this.moveEndDragEventCallback(event);
      this.moveEndEventCallback(event);
    });
    this.map.on('dblclick', (event: MapBrowserEvent<UIEvent>) => {
      this.doubleClick.emit(event);
    });
  }

  ngAfterViewInit() {
    /**
     * on 'remount' we need to init the slide again
     * to make the map layer fit inside the viewport
     * TODO: find a more elegant way to solve this
     * without running the whole re-initialization process
     */
    if (this.slide) {
      this.initSlide(this.slide);
    }
  }

  ngOnDestroy(): void {
    this.clearSlide();

    // TODO: do we need to remove event listeners?
    // this.map.removeEventListener
  }

  // prevent the default context menu from opening
  @HostListener('contextmenu', ['$event'])
  onRightClick(event: MouseEvent): void {
    event.preventDefault();
  }

  private mouseInteractionCondition(event: MapBrowserEvent<UIEvent>): boolean {
    return noModifierKeys(event)
      && (
        event.originalEvent?.['buttons'] === MouseButtonType.Left
        || event.originalEvent?.['buttons'] === MouseButtonType.Right
      );
  }

  addLeftAndRightMouseButtonToMove(): void {
    this.map.addInteraction(new DragPan({
      condition: this.mouseInteractionCondition,
    }));
  }

  removeDoubleClickToZoomInteraction() {
    const dblClick = this.map.getInteractions().getArray().find((interaction) => {
      return interaction instanceof DoubleClickZoom;
    });
    this.map.removeInteraction(dblClick);
  }

  updateMapByZoomView(v: ZoomView) {
    if (v.sender === ViewObserver.OverviewMap) {
      this.map.getView().set(VIEW_MOVED_BY + ViewObserver[v.sender], true);
      this.map.getView().fit(v.extent);
      this.map.updateSize();
    } else if (v.sender === ViewObserver.ZoomSlider) {
      this.map.getView().set(VIEW_MOVED_BY + ViewObserver[v.sender], true);
      this.map.getView().setResolution(v.resolution);
      //this.map.getView().animate({ resolution: v.resolution, duration: 200 });
      this.map.updateSize();
    }
  }

  pointerDragEventCallback(event: MapBrowserEvent<UIEvent>) {
    const extent = event.map.getView().calculateExtent(event.map.getSize());
    const resolution = event.map.getView().getResolution();
    this.moveEndZoomView.emit({
      extent,
      resolution: resolution,
      sender: ViewObserver.MainMap,
      receivers: [ViewObserver.OverviewMap]
    });
  }

  moveEndDragEventCallback(event: MapEvent) {
    const extent = event.map.getView().calculateExtent(event.map.getSize());
    const resolution = event.map.getView().getResolution();

    // alternative to using the ngrx store for view updates
    // const overview = this.map.get('ext-overview') as ExtZoomSliderComponent;
    // if(overview){
    //   overview.updateZoomSlider({
    //     receivers: [ViewObserver.ZoomSlider],
    //     resolution,
    //     sender: ViewObserver.MainMap,
    //     extent,
    //   });
    // }

    if (this.map.getView().get(VIEW_MOVED_BY + ViewObserver[ViewObserver.OverviewMap]) === true) {
      // only update the zoom slider
      this.moveEndZoomView.emit({
        extent,
        resolution,
        sender: ViewObserver.MainMap,
        receivers: [ViewObserver.ZoomSlider],
      });
      this.map.getView().set(VIEW_MOVED_BY + ViewObserver[ViewObserver.OverviewMap], false);
    } else if (this.map.getView().get(VIEW_MOVED_BY + ViewObserver[ViewObserver.ZoomSlider]) === true) {
      // only update the overview map
      this.moveEndZoomView.emit({
        extent,
        resolution,
        sender: ViewObserver.MainMap,
        receivers: [ViewObserver.OverviewMap],
      });
      this.map.getView().set(VIEW_MOVED_BY + ViewObserver[ViewObserver.ZoomSlider], false);
    } else {
      // update both controls (native move on main map)
      this.moveEndZoomView.emit({
        extent,
        resolution,
        sender: ViewObserver.MainMap,
        receivers: [ViewObserver.OverviewMap, ViewObserver.ZoomSlider],
      });
    }
  }

  moveEndEventCallback(event: MapEvent) {
    const extent = AxisConverter.invertYAxisOfExtent(
      event.map.getView().calculateExtent(event.map.getSize())
    );
    const view = event.map.getView();
    const zoom = view.getZoom();
    const nppCurrent = this.viewerCalculationsService.nppForView(view);
    const [nppMin, nppMax] = this.viewerCalculationsService.nppExtrema(view);
    const currentView: CurrentView = { extent, zoom, nppCurrent, nppMin, nppMax, nppBase: this._slide.imageInfo.npp };
    this.moveEnd.emit(currentView);
  }

  // ngAfterViewInit(): void {
  //   // this.slideContainer.nativeElement.id = this.targetId;
  //   // this.map.setTarget(this.slideContainer.nativeElement);
  //   // this.mapService.setImageSources(this.mapId, this.slideId);
  //   // this.map.updateSize();
  // }

  initSlide(slide: Slide) {
    if (!this.map) {
      this.mapIdService.setId(this.mapId);
      this.map = this.mapService.getMap(this.mapId);
    }
    this.mapService.setImageSources(this.mapId, slide.slideId);
    this.map.updateSize();
    this.setNewSource(slide);
  }

  /* currently only used for Debugging */
  //onMapClick(event: MapBrowserEvent<UIEvent>): void {
  //  const curZoom = event.map.getView().getZoom();
  //}

  setNewSource(slide: Slide): void {
    this.mapService.setImageSources(this.mapId, slide.slideId);

    this.clearSlide();
    const source = SlideSourceLoader.createXYZSource(slide);


    this.layer = new TileLayer({
      source,
      // extent: projectionExtent
      preload: Infinity,
    });

    // this.layer.setSource(source);

    // this.map.setLayerGroup(new LayerGroup({ layers: [layer] }));
    this.map.addLayer(this.layer);

    const projectionCode = this.createPixelProjection(slide.imageInfo.npp);
    const view = this.createView(source, projectionCode);

    this.map.setView(view);
    this.fitToExtend(source, this.map);
    this.map.render();
  }

  fitToExtend(source: XYZ, map: Map) {
    const extent = source.getTileGrid().getExtent();
    this.map.getView().fit(extent, { size: map.getSize() });
  }

  createView(source: XYZ, projectionCode: string): View {
    const extent = source.getTileGrid().getExtent();
    return new View({
      resolutions: source.getTileGrid().getResolutions(),
      extent,
      center: getCenter(extent),
      enableRotation: false,
      constrainOnlyCenter: true,
      showFullExtent: true,
      projection: projectionCode,
    });
  }

  createPixelProjection(npp: number): string {
    const projectionCode = PROJECTION_KEY;
    const projection = new Projection({
      code: PROJECTION_KEY,
      units: 'pixels',
      metersPerUnit: npp * PROJECTION_NM_TO_M,
      getPointResolution: (resolution) => resolution,
    });

    olAddProjection(projection);
    return projectionCode;
  }

  private clearSlide(): void {
    if (this.layer) {
      this.map.removeLayer(this.layer);
    }
  }
}
