import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScaleLineComponent } from './scale-line.component';

describe('ScalelineComponent', () => {
  let component: ScaleLineComponent;
  let fixture: ComponentFixture<ScaleLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScaleLineComponent],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScaleLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
