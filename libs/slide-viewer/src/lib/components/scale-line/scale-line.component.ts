import {
  AfterViewInit,
  Component,
  ElementRef,
  Host,
  Input,
  Optional,
} from '@angular/core';
import { ScaleLine } from 'ol/control';
import { Map } from 'ol';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';

@Component({
  selector: 'svm-scale-line',
  templateUrl: './scale-line.component.html',
  styleUrls: ['./scale-line.component.scss'],
})
export class ScaleLineComponent implements AfterViewInit {
  @Input() mapId: string;

  private instance: ScaleLine;
  private viewerHost: Map;

  constructor(
    private elementRef: ElementRef,
    @Host()
    @Optional()
    private mapIdService: MapIdService,
    private mapService: MapService
  ) { }

  ngAfterViewInit(): void {
    // if control is outside of viewer component id has to be set
    // if (this.mapId) {
    //   this.viewerHost = this.mapService.getMap(this.mapId)
    // } else if (this.viewerComponent) {
    //   this.viewerHost = this.viewerComponent.map;
    // } else {
    //   throw new Error('No Map instance set in ScaleLineComponent')
    // }
    // // this.instance = new ScaleLine({ ...this, bar: true, className: 'z-high', target: this.targetId });
    // this.instance = new ScaleLine();
    // this.viewerHost.addControl(this.instance);
    this.init();
  }

  init(): void {
    const id = this.mapIdService || this.mapId;
    this.viewerHost = this.mapService.getMap(id);


    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const target = this.elementRef.nativeElement.parentElement
      ? this.elementRef.nativeElement
      : null;

    this.instance = new ScaleLine({
      // className: 'custom-scale-line .ol-scale-line .ol-scale-line-inner',
      minWidth: 100,
      target
    });

    this.viewerHost.addControl(this.instance);
  }
}
