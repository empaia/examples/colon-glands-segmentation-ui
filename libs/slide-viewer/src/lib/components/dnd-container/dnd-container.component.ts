import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewChild,
} from '@angular/core';

import { ResizeEvent, Edges } from 'angular-resizable-element';
import {
  ElementSize,
  ResizableElementService,
} from '../../services/resizable-element.service';

@Component({
  selector: 'svm-dnd-container',
  templateUrl: './dnd-container.component.html',
  styleUrls: ['./dnd-container.component.scss'],
  providers: [ResizableElementService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DndContainerComponent implements AfterViewInit {
  readonly MIN_WIDTH = 50;
  // eslint-disable-next-line @typescript-eslint/ban-types
  public style: object = {};
  public resizeEdges: Edges = {
    bottom: true,
    right: true,
    top: true,
    left: true,
  };

  // TODO: clean solution for this
  private _resetSize: string;
  @Input() public get resetSize() {
    return this._resetSize;
  }
  public set resetSize(val: string) {
    this._resetSize = val;
  }

  @ViewChild('dndContainer') containerDiv!: HTMLDivElement;

  public isMinimized = false;

  constructor(private sizeService: ResizableElementService) { }

  ngAfterViewInit(): void {
    const initial: ElementSize = {
      width: 256,
      height: 256,
    };
    this.sizeService.setStyle(initial);
  }

  onResizing(event: ResizeEvent): void {

    this.style = {
      position: 'fixed',
      left: `${event.rectangle.left}px`,
      top: `${event.rectangle.top}px`,
      width: `${event.rectangle.width}px`,
      height: `${event.rectangle.height}px`,
    };
    this.sizeService.setStyle({
      width: event.rectangle.width,
      height: event.rectangle.height,
    });
  }

  validate(event: ResizeEvent): boolean {
    const MIN_DIMENSIONS_PX = 50;
    if (
      event.rectangle.width &&
      event.rectangle.height &&
      (event.rectangle.width < MIN_DIMENSIONS_PX ||
        event.rectangle.height < MIN_DIMENSIONS_PX)
    ) {
      return false;
    }
    return true;
  }

  // TODO: implement container minimize
  minimize(): void {
    // if (this.isMinimized) {
    //   this.style = { ...this.restoreStyle };
    //   this.isMinimized = false;
    //   this.sizeService.setStyle({
    //     width: parseInt(this.style.width, 10),
    //     height: parseInt(this.style.height, 10),
    //   });
    // } else {
    //   this.isMinimized = true;
    //   this.restoreStyle = { ...this.style };
    //   const width = 68;
    //   const height = 24;
    //   this.style = {
    //     ...this.style,
    //     width: `${width} px`,
    //     height: `${height} px`
    //   };
    //   this.sizeService.setStyle({
    //     width: width,
    //     height: height
    //   });
    // }
  }
}
