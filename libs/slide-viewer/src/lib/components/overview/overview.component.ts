import {
  Component,
  OnInit,
  AfterViewInit,
  Input,
  Optional,
  ElementRef,
  Host,
  OnDestroy,
} from '@angular/core';

import { OverviewMap } from 'ol/control';
import { Observable } from 'rxjs';
import { XYZ } from 'ol/source';
import TileLayer from 'ol/layer/Tile';
import { View, Map } from 'ol';
import { getCenter } from 'ol/extent';

import { filter } from 'rxjs/operators';
import { MapService } from '../../services/map.service';
import { MapIdService } from '../../services/map-id.service';
import {
  ElementSize,
  ResizableElementService,
} from '../../services/resizable-element.service';
import { Slide } from '../../models/slide';
import { SlideSourceLoader } from '../../services/slide-source';
import { PROJECTION_KEY } from '../../models/types';

/**
 * Slide Overview / Map Overview Component.
 * Can  be embedded into a Drag n Drop (DnD) Container.
 * TODO: Calculate the correct zoom level of overview View depending on the Size
 * of the Slide and the current size of the embedding container (DnD Window).
 * TODO: Render crosshair on zoom in
 * TODO: Click on overview to center map
 * See: ext-overview for further hints how to implement these feature
 */

@Component({
  selector: 'svm-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() mapId: string;

  private _slide: Slide;
  @Input() public get slide() {
    return this._slide;
  }

  public set slide(val: Slide) {
    if (val) {
      this.source = SlideSourceLoader.createXYZSource(val);
      this.createInstanceFromSource(this.source);
    } else {
      this.source = undefined;
    }
    this._slide = val;
  }

  private viewerHost: Map;
  private instance: OverviewMap;
  private source: XYZ;
  // private slide$: Observable<Slide>;

  private elementSize$: Observable<ElementSize>;

  private layer: TileLayer<XYZ>;

  private readonly viewPadding = 10;

  constructor(
    private mapService: MapService,
    @Host()
    @Optional()
    private mapIdService: MapIdService,
    private elementRef: ElementRef,
    @Host()
    @Optional()
    private sizeService: ResizableElementService,
    // private store: Store
  ) {
    // this.slide$ = this.store.select(SlideSelectors.selectSlide);
    this.elementSize$ = this.sizeService ? this.sizeService.style : null;
  }

  ngOnInit(): void {
    this.elementSize$
      ?.pipe(
        filter((p) => p !== undefined),
        filter(() => !!this.instance),
        filter(() => !!this.viewerHost)
        // delay(500),
      )
      .subscribe((s) => {
        this.updateSize(s);
        this.updateZoomLevel(this.source);
      });
  }

  private updateSize(size: ElementSize) {
    if (this.instance && size) {
      this.instance.getOverviewMap().getTargetElement().style.height = `${
        size.height - this.viewPadding
      }px`;
      this.instance.getOverviewMap().getTargetElement().style.width = `${
        size.width - this.viewPadding
      }px`;

      this.instance.getOverviewMap().setSize([size.width, size.height]);
      this.instance.getOverviewMap().updateSize();
    }
  }

  ngOnDestroy(): void {
    this.viewerHost.removeControl(this.instance);
  }

  ngAfterViewInit(): void {
    const id = this.mapIdService || this.mapId;
    this.viewerHost = this.mapService.getMap(id);
  }

  createInstanceFromSource(source: XYZ) {
    if (!this.viewerHost) {
      return;
    }

    this.clearOverview();
    const target = this.elementRef.nativeElement.parentElement
      ? this.elementRef.nativeElement
      : null;

    this.layer = new TileLayer({
      source,
      preload: Infinity,
    });

    const extent = source.getTileGrid().getExtent();

    this.instance = new OverviewMap({
      className: 'ol-overviewmap ol-custom-overviewmap',
      // target: this.viewerHost.getTarget(),
      target,
      collapsed: false,

      collapsible: false,
      layers: [this.layer],
      view: new View({
        center: getCenter(extent),
        extent,
        projection: PROJECTION_KEY,
        resolutions: source.getTileGrid().getResolutions(),
      }),
    });

    this.instance.getOverviewMap().getView().fit(extent);
    this.viewerHost.addControl(this.instance);
    // this.instance.setTarget(this.overviewDiv.nativeElement);
    this.instance.getOverviewMap().updateSize();

    this.updateZoomLevel(source);
  }

  clearOverview(): void {
    if (this.instance) {
      this.viewerHost.removeControl(this.instance);
    }
    if (this.layer) {
      this.instance.getOverviewMap().removeLayer(this.layer);
    }
  }

  updateElement(): void {
    this.instance.getOverviewMap().updateSize();
  }

  private updateZoomLevel(source: XYZ): void {
    if (!source) {
      return;
    }
    const tileSize = source.getTileGrid().getTileSize(0) as number;
    const viewPort = Math.min(...this.instance.getOverviewMap().getSize());
    const zoomLock = Math.round(viewPort / tileSize) - 1;
    const view = this.instance.getOverviewMap().getView();
    view.setZoom(zoomLock < 0 ? 0 : zoomLock);
    view.setMinZoom(zoomLock < 0 ? 0 : zoomLock);
    view.setMaxZoom(zoomLock < 0 ? 0 : zoomLock);
    view.fit(source.getTileGrid().getExtent());
  }
}
