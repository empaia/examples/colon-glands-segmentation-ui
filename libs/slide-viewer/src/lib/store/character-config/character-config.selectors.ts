import { createSelector } from '@ngrx/store';
import { selectModuleFeatureState, ModuleState } from '../module.state';

export const selectCharacterConfigState = createSelector(
  selectModuleFeatureState,
  (state: ModuleState) => state.svmCharacterConfig
);

export const selectCharacterConfig = createSelector(
  selectCharacterConfigState,
  (state) => state.config
);

export const selectAnnotationCharacterConfig = createSelector(
  selectCharacterConfigState,
  (state) => state.config.annotation
);
