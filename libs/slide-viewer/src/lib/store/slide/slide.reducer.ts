import { Action, createReducer, on } from '@ngrx/store';
import { Slide } from '../../models/slide';
import * as SlideActions from './slide.actions';

export interface State {
  slide: Slide;
  slideId: string;
  loaded: boolean;
}

export const initialState: State = {
  slide: null,
  slideId: undefined,
  loaded: false,
};

const slideReducer = createReducer(
  initialState,

  on(SlideActions.addSlideReady, (state, { slide }): State => ({
    ...state,
    slide,
  })),

  on(SlideActions.selectSlideId, (state, { slideId }): State => ({
    ...state,
    slideId,
  }))

  // on(SlideActions.loadSlides, (state): State => state),
  // on(SlideActions.loadSlidesSuccess, (state, _action): State => state),
  // on(SlideActions.loadSlidesFailure, (state, _action): State => state)
);

// a reducer creator wrapper function - Only needed if using View Engine AOT
export function reducer(state: State | undefined, action: Action) {
  return slideReducer(state, action);
}

export const selectSlide = (state: State) => state.slide;
// export const selectTileSource = (state: State) => state.slideTileSource;
export const selectSlideLoaded = (state: State) => state.loaded;
