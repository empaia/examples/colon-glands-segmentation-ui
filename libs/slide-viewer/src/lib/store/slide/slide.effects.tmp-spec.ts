import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { SlideEffects } from './slide.effects';

describe('SlideEffects', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let actions$: Observable<any>;
  let effects: SlideEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SlideEffects, provideMockActions(() => actions$)],
      teardown: { destroyAfterEach: false },
    });

    effects = TestBed.inject(SlideEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
