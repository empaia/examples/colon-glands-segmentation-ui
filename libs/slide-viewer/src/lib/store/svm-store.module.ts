import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import * as fromStore from './module.state';
import { SVM_FEATURE_KEY } from './store.keys';
import { AnnotationEffects } from './annotations';
import { SlideEffects } from './slide';
import { UiEffects } from './ui';
import { ClassesEffects } from './classes/classes.effects';
@NgModule({
  imports: [
    CommonModule,
    /**
     * StoreModule.forFeature is used for composing state
     * from feature modules. These modules can be loaded
     * eagerly or lazily and will be dynamically added to
     * the existing state.
     */
    StoreModule.forFeature(
      SVM_FEATURE_KEY,
      fromStore.reducersSVM,
      { metaReducers: fromStore.metaReducers }
    ),

    /**
     * Effects.forFeature is used to register effects
     * from feature modules. Effects can be loaded
     * eagerly or lazily and will be started immediately.
     *
     * All Effects will only be instantiated once regardless of
     * whether they are registered once or multiple times.
     */
    EffectsModule.forFeature([SlideEffects, AnnotationEffects, UiEffects, ClassesEffects]),
  ],
  // declarations: [],
})
export class SvmStoreModule { }
