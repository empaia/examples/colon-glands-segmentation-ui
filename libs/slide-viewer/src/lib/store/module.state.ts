import {
  Action,
  combineReducers,
  createFeatureSelector,
  MetaReducer,
} from '@ngrx/store';

import { clearState } from './meta.reducer';

import * as  AnnotationFeature from './annotations/annotation.reducer';
import * as  RectangleExplanationFeature from './rectangle-explanations/rectangle-explanation.reducer';
import * as  SlideFeature from './slide/slide.reducer';
import * as  UiFeature from './ui/ui.reducer';
import * as ClusterFeature from './cluster/cluster.reducer';
import * as CharacterConfigFeature from './character-config/character-config.reducer';
import * as ClassesFeature from './classes/classes.reducer';

import {
  SVM_ANNOTATIONS_FEATURE_KEY,
  SVM_FEATURE_KEY,
  SVM_SLIDE_FEATURE_KEY,
  SVM_UI_FEATURE_KEY,
  SVN_CLUSTER_FEATURE_KEY,
  SVM_CHARACTER_CONFIG_KEY,
  SVM_CLASSES_FEATURE_KEY,
  RECTANGLE_EXPLANATIONS_FEATURE_KEY,
} from './store.keys';

export const selectModuleFeatureState = createFeatureSelector<ModuleState>(
  SVM_FEATURE_KEY
);

export interface ModuleState {
  [SVM_SLIDE_FEATURE_KEY]: SlideFeature.State;
  [SVM_ANNOTATIONS_FEATURE_KEY]: AnnotationFeature.State;
  [SVM_UI_FEATURE_KEY]: UiFeature.State;
  [SVN_CLUSTER_FEATURE_KEY]: ClusterFeature.State;
  [SVM_CHARACTER_CONFIG_KEY]: CharacterConfigFeature.State;
  [SVM_CLASSES_FEATURE_KEY]: ClassesFeature.State;
  [RECTANGLE_EXPLANATIONS_FEATURE_KEY]: RectangleExplanationFeature.State;
}

// make AOT compiler in prod mode happy - see https://github.com/ngrx/platform/issues/2193
export function reducersSVM(state: ModuleState | undefined, action: Action) {
  return combineReducers({
    [SVM_SLIDE_FEATURE_KEY]: SlideFeature.reducer,
    [SVM_ANNOTATIONS_FEATURE_KEY]: AnnotationFeature.reducer,
    [SVM_UI_FEATURE_KEY]: UiFeature.reducer,
    [SVN_CLUSTER_FEATURE_KEY]: ClusterFeature.reducer,
    [SVM_CHARACTER_CONFIG_KEY]: CharacterConfigFeature.reducer,
    [SVM_CLASSES_FEATURE_KEY]: ClassesFeature.reducer,
    [RECTANGLE_EXPLANATIONS_FEATURE_KEY]: RectangleExplanationFeature.reducer,
  })(state, action);
}

// clear state on module destroy
export const metaReducers: MetaReducer<ModuleState>[] = [clearState];

export const selectModuleState = createFeatureSelector<ModuleState>(SVM_FEATURE_KEY);

