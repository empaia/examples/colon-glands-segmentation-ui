import { AnnotationMappingService } from './../../services/annotation-mapping.service';
import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import * as RectangleExplanationActions from './rectangle-explanation.actions';
import * as RectangleExplanationSelectors from './rectangle-explanation.selectors';
import { Store } from '@ngrx/store';
import { filter, map } from 'rxjs/operators';

@Injectable()
export class RectangleExplanationEffects {

  addAnnotationIds$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RectangleExplanationActions.setAnnotationIds),
      map(action => action.annotationIds),
      map(ids => this.annotationIdSet.addMany(ids)),
      map(() => RectangleExplanationActions.setAnnotationIdsReady())
    );
  });

  // we need to make place for the newly requested annotations, so we clean up the cache if needed
  checkCleanUpCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RectangleExplanationActions.setAnnotationIdsReady),
      concatLatestFrom(() => [
        this.store.select(RectangleExplanationSelectors.selectMissingCachedAnnotations),
        this.store.select(RectangleExplanationSelectors.selectCurrentAnnotationCacheSize),
        this.store.select(RectangleExplanationSelectors.selectMaxAnnotationCacheSize)
      ]),
      filter(([_action, cache, cacheSize, maxSize]) => cacheSize + cache.missingAnnotations.length > maxSize),
      map(([_action, cache, cacheSize]) => cacheSize - cache.cachedAnnotations.length),
      filter(index => index > 0),
      map(index => this.annotationIdSet.discard(index)),
      map(annotationIds => RectangleExplanationActions.discardAnnotations({ annotationIds }))
    );
  });

  clearAnnotations$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(RectangleExplanationActions.clearAnnotations),
      map(() => this.annotationIdSet.clear())
    );
  }, { dispatch: false });

  constructor(
    private actions$: Actions,
    private store: Store,
    private annotationIdSet: AnnotationMappingService,
  ) {}
}
