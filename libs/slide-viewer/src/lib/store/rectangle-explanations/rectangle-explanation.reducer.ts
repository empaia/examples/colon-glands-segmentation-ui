import { Action, createReducer, on } from '@ngrx/store';
import { AnnotationEntity } from '../../models/annotation';
import * as RectangleExplanationActions from './rectangle-explanation.actions';
import {
  EntityCachingAdapter,
  EntityCachingState,
  createEntityCachingAdapter
} from '../../entity';

export interface State extends EntityCachingState<AnnotationEntity> {
  // additional entities state properties
  addAnnotationIds: string[];
  highlighted: Array<string>;
  focus: string;
  centroids: number[][];
  discardedIds: string[];
  hiddenIds: string[];
  clear: boolean;
}

export const annotationAdapter: EntityCachingAdapter<AnnotationEntity> = createEntityCachingAdapter<AnnotationEntity>(
  (annotation) => annotation.id
);

export const initialState: State = annotationAdapter.getInitialState({
  // externally set annotation features that should be highlighted
  addAnnotationIds: [],
  highlighted: [],
  focus: undefined,
  centroids: [],
  discardedIds: [],
  hiddenIds: [],
  clear: false,
});

const annotationReducer = createReducer(
  initialState,
  on(RectangleExplanationActions.addAnnotation, (state, { annotation }): State =>
    annotationAdapter.addOne(annotation, {
      ...state,
      clear: false,
    })
  ),
  on(RectangleExplanationActions.setAnnotations, (state, { annotations }): State =>
    annotationAdapter.addMany(annotations, {
      ...state,
      clear: false,
    })
  ),
  on(RectangleExplanationActions.clearAnnotations, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      ...initialState,
      clear: true,
    })
  ),
  on(RectangleExplanationActions.highlightAnnotations, (state, { ids }): State => ({
    ...state,
    highlighted: ids,
    clear: false,
  })),
  on(RectangleExplanationActions.focusAnnotation, (state, { id }): State => ({
    ...state,
    focus: id,
    clear: false,
  })),
  on(RectangleExplanationActions.setCentroids, (state, { centroids }): State => ({
    ...state,
    centroids,
    clear: false,
  })),
  on(RectangleExplanationActions.setAnnotationIds, (state, { annotationIds }): State => ({
    ...state,
    addAnnotationIds: annotationIds,
    clear: false,
  })),
  on(RectangleExplanationActions.discardAnnotations, (state, { annotationIds }): State =>
    annotationAdapter.removeMany(annotationIds, {
      ...state,
      discardedIds: annotationIds,
      clear: false,
    })
  ),
  on(RectangleExplanationActions.hideAnnotations, (state, { hiddenIds }): State => ({
    ...state,
    hiddenIds,
    clear: false,
  }))
);

// a reducer creator wrapper function - Only needed if using View Engine AOT
export function reducer(state: State | undefined, action: Action) {
  return annotationReducer(state, action);
}


export const {
  selectIds,
  selectAll,
  selectTotal,
} = annotationAdapter.getSelectors();
