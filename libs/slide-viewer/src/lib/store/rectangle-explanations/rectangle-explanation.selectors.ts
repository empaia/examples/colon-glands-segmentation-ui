import { createSelector } from '@ngrx/store';
import { selectModuleFeatureState, ModuleState } from '../module.state';
import {
  selectAll,
  selectTotal,
} from './rectangle-explanation.reducer';
import { Annotation } from '../../models/annotation';


export const selectAnnotationFeatureState = createSelector(
  selectModuleFeatureState,
  (state: ModuleState) => state.svmRectangleExplanations
);

export const selectAnnotations = createSelector(
  selectAnnotationFeatureState,
  selectAll
);

export const selectHighlighted = createSelector(
  selectAnnotationFeatureState,
  (state) => state.highlighted
);

export const selectFocused = createSelector(
  selectAnnotationFeatureState,
  (state) => state.focus
);

export const selectCentroids = createSelector(
  selectAnnotationFeatureState,
  (state) => state.centroids
);

export const selectAddedAnnotationIds = createSelector(
  selectAnnotationFeatureState,
  (state) => state.addAnnotationIds
);

export const selectRenderAnnotations = createSelector(
  selectAnnotationFeatureState,
  selectAddedAnnotationIds,
  (state, annotationIds) => {
    const cachedAnnotations: Annotation[] = [];
    let annotation: Annotation | undefined;
    for (const id of annotationIds) {
      annotation = state.entityCache.get(id);
      if (annotation) {
        cachedAnnotations.push(annotation);
      } else {
        return undefined;
      }
    }
    return cachedAnnotations;
  }
);

export const selectAddedAnnotations = createSelector(
  selectAnnotationFeatureState,
  (state) => {
    const addedAnnotations: Annotation[] = [];
    let annotation: Annotation | undefined;
    for (const id of state.addedEntityIds) {
      annotation = state.entityCache.get(id);
      if (annotation) {
        addedAnnotations.push(annotation);
      } else {
        return undefined;
      }
    }
    return addedAnnotations;
  }
);

export const selectDiscardAnnotationIds = createSelector(
  selectAnnotationFeatureState,
  (state) => state.discardedIds
);

export const selectClearState = createSelector(
  selectAnnotationFeatureState,
  (state) => state.clear
);

export const selectHiddenAnnotationIds = createSelector(
  selectAnnotationFeatureState,
  (state) => state.hiddenIds
);

export const selectMissingCachedAnnotations = createSelector(
  selectAnnotationFeatureState,
  selectAddedAnnotationIds,
  (state, addedAnnotationIds) => {
    //there is nothing cached yet
    if (!state.entityCache || state.entityCache.size < 1) {
      return {
        missingAnnotations: addedAnnotationIds,
        cachedAnnotations: [],
      };
    }
    // we check out cache for uncached and already cached annotations
    const missing: string[] = [];
    const keep: string[] = [];
    addedAnnotationIds.forEach(id => {
      if(!state.entityCache.has(id)) {
        missing.push(id);
      } else {
        keep.push(id);
      }
    });

    return {
      missingAnnotations: missing,
      cachedAnnotations: keep,
    };
  }
);

export const selectCurrentAnnotationCacheSize = createSelector(
  selectAnnotationFeatureState,
  selectTotal
);

export const selectMaxAnnotationCacheSize = createSelector(
  selectAnnotationFeatureState,
  (state) => state.cacheSizeSoftConstraint
);

export const selectAnnotationCache = createSelector(
  selectAnnotationFeatureState,
  (state) => state.entityCache
);
