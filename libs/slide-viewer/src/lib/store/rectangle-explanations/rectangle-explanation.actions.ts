import { createAction, props } from '@ngrx/store';

import { AnnotationEntity } from '../../models/annotation';

export const setAnnotationIds = createAction(
  '[SVM Rectangle Explanation] Set Annotation Ids',
  props<{ annotationIds: string[] }>()
);

export const setAnnotationIdsReady = createAction(
  '[SVM Rectangle Explanation] Set Annotation Ids Ready',
);

export const addAnnotation = createAction(
  '[SVM Rectangle Explanation] Add Annotation',
  props<{ annotation: AnnotationEntity }>()
);

export const setAnnotations = createAction(
  '[SVM Rectangle Explanation] Set Annotations',
  props<{ annotations: AnnotationEntity[] }>()
);

export const clearAnnotations = createAction(
  '[SVM Rectangle Explanation] Clear Annotations'
);

export const highlightAnnotations = createAction(
  '[SVM Rectangle Explanation] Highlight Annotations',
  props<{ ids: string[] }>()
);

export const focusAnnotation = createAction(
  '[SVM Rectangle Explanation] Focus Annotation',
  props<{ id: string }>()
);

export const setCentroids = createAction(
  '[SVM Rectangle Explanation] Set Annotation Centroids',
  props<{ centroids: number[][] }>()
);

export const discardAnnotations = createAction(
  '[SVM Rectangle Explanation] Discard Annotations',
  props<{ annotationIds: string[] }>()
);

export const hideAnnotations = createAction(
  '[SVM Rectangle Explanation] Hide Annotations',
  props<{ hiddenIds: string[] }>()
);
