import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';

import { AnnotationEffects } from './annotation.effects';
import * as AnnotationsActions from './annotation.actions';
import * as AnnotationsSelectors from './annotation.selectors';
import { AnnotationMappingService } from '../../services/annotation-mapping.service';

describe('AnnotationEffects', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let actions$: Observable<any>;
  let effects: AnnotationEffects;
  let store: MockStore;
  let mappingService: AnnotationMappingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AnnotationEffects, 
        provideMockActions(() => actions$),
        provideMockStore(),
        AnnotationMappingService,
      ],
    });

    effects = TestBed.inject(AnnotationEffects);
    store = TestBed.inject(MockStore);
    mappingService = TestBed.inject(AnnotationMappingService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should add annotation ids', (done) => {
    const idList = ['test1', 'test2'];
    actions$ = of(AnnotationsActions.setAnnotationIds({ annotationIds: idList }));
    effects.addAnnotationIds$.subscribe((_ids) => {
      expect(mappingService.getAll().size).toBe(idList.length);
      expect(mappingService.getAll()).toEqual(new Set(idList));
      done();
    });
  });

  it('should clear all annotations', (done) => {
    const idList = ['test1', 'test2'];
    actions$ = of(AnnotationsActions.setAnnotationIds({ annotationIds: idList }));
    actions$ = of(AnnotationsActions.clearAnnotations());
    effects.clearAnnotations$.subscribe((_ids) => {
      expect(mappingService.getAll().size).toBe(0);
      done();
    });
  });

  it('should discard annotations when cache full', (done) => {
    const cached = ['cached1', 'cached2', 'cached3', 'cached4'];
    mappingService.addMany(cached );
    store.overrideSelector(
      AnnotationsSelectors.selectMissingCachedAnnotations, 
      { missingAnnotations: ['missing1', 'missing2'], cachedAnnotations: cached },
    );
    store.overrideSelector(AnnotationsSelectors.selectCurrentAnnotationCacheSize, 5);
    store.overrideSelector(AnnotationsSelectors.selectMaxAnnotationCacheSize, 5);

    actions$ = of(AnnotationsActions.setAnnotationIdsReady());
    effects.checkCleanUpCache$.subscribe((ids) => {
      expect(mappingService.getAll().size).toBe(3);
      expect(ids.annotationIds).toEqual(['cached1']);
      done();
    });
  });

  it('should add more annotations then cache size', (done) => {
    const idList = ['test1', 'test2', 'test3', 'test4', 'test5', 'test6'];
    store.overrideSelector(
      AnnotationsSelectors.selectMissingCachedAnnotations, 
      { missingAnnotations: idList, cachedAnnotations: [] },
    );
    store.overrideSelector(AnnotationsSelectors.selectCurrentAnnotationCacheSize, 5);
    store.overrideSelector(AnnotationsSelectors.selectMaxAnnotationCacheSize, 5);

    actions$ = of(AnnotationsActions.setAnnotationIds({ annotationIds: idList }));
    effects.addAnnotationIds$.subscribe((_ids) => {
      expect(mappingService.getAll().size).toBe(idList.length);
    });

    actions$ = of(AnnotationsActions.setAnnotationIdsReady());
    effects.checkCleanUpCache$.subscribe((ids) => {
      expect(mappingService.getAll().size).toBe(1);
      expect(ids.annotationIds).toEqual([...idList].splice(0, idList.length - 1));
      done();
    });
  });
});
