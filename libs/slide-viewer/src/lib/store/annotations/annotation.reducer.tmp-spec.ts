import { highlightAnnotations } from './annotation.actions';
import { mockAnnotations } from '../../models/mocks/annotation.mocks';
import {
  reducer,
  initialState,
  State,
} from './annotation.reducer';
import { AnnotationEntity } from '../../models/annotation';

describe('Annotation Reducer', () => {
  const mockState: State = {
    /* ids: mockAnnotations.map((a) => a.id),
    entities: {
      // is there a better way to init dict from array?
      // TODO: should we check array length before access
      [mockAnnotations[0].id]: mockAnnotations[0],
      [mockAnnotations[1].id]: mockAnnotations[1],
      [mockAnnotations[2].id]: mockAnnotations[2],
    }, */
    entityCache: new Map<string, AnnotationEntity>(),
    addedEntityIds: [],
    cacheSizeSoftConstraint: 100000,
    addAnnotationIds: [],
    highlighted: [],
    focus: undefined,
    centroids: [],
  };

  beforeAll(() => {
    for (let i = 0; i < 3; i++) {
      mockState.entityCache.set(mockAnnotations[i].id, mockAnnotations[i] as AnnotationEntity);
    }
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const action = {} as any;
      const result = reducer(mockState, action);
      expect(result).toBe(mockState);
    });

    it('should return the default state', () => {
      const action = { type: 'Unknown' };
      const state = reducer(initialState, action);
      expect(state).toBe(initialState);
    });
  });

  describe('AnnotationActions highlightAnnotations', () => {
    it('should add item to highlight array (snap)', () => {
      const action = highlightAnnotations({ ids: [mockAnnotations[0].id] });
      const result = reducer(mockState, action);
      expect(result).toMatchSnapshot();
    });

    it('should add item to highlight array', () => {
      const action = highlightAnnotations({ ids: [mockAnnotations[0].id] });
      const result = reducer(mockState, action);
      expect(result.highlighted.length).toBe(1);
      expect(result.highlighted[0]).toBe(mockAnnotations[0].id);
    });

    /* it('SELECT item from highlight array', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const action = highlightAnnotations({ ids: [mockAnnotations[0].id] });

      const result = selectHighlighted({
        ...mockState,
        highlighted: [mockAnnotations[0].id],
      });
      expect(result.length).toBe(1);
      expect(result[0]).toBe(mockAnnotations[0].id);

      expect(result).toMatchSnapshot();
    }); */
  });
});
