import { Action, createReducer, on } from '@ngrx/store';
import { AnnotationEntity } from '../../models/annotation';
import * as AnnotationActions from './annotation.actions';
import {
  EntityCachingAdapter,
  EntityCachingState,
  createEntityCachingAdapter
} from '../../entity';

export interface State extends EntityCachingState<AnnotationEntity> {
  // additional entities state properties
  addAnnotationIds: string[];
  highlighted: Array<string>;
  focus: string;
  centroids: number[][];
  discardedIds: string[];
  hiddenIds: string[];
  clear: boolean;
}

export const annotationAdapter: EntityCachingAdapter<AnnotationEntity> = createEntityCachingAdapter<AnnotationEntity>(
  (annotation) => annotation.id
);

export const initialState: State = annotationAdapter.getInitialState({
  // externally set annotation features that should be highlighted
  addAnnotationIds: [],
  highlighted: [],
  focus: undefined,
  centroids: [],
  discardedIds: [],
  hiddenIds: [],
  clear: false,
});

const annotationReducer = createReducer(
  initialState,
  on(AnnotationActions.addAnnotation, (state, { annotation }): State =>
    annotationAdapter.addOne(annotation, {
      ...state,
      clear: false,
    })
  ),
  on(AnnotationActions.setAnnotations, (state, { annotations }): State =>
    annotationAdapter.addMany(annotations, {
      ...state,
      clear: false,
    })
  ),
  on(AnnotationActions.clearAnnotations, (state): State =>
    annotationAdapter.removeAll({
      ...state,
      ...initialState,
      clear: true,
    })
  ),
  on(AnnotationActions.highlightAnnotations, (state, { ids }): State => ({
    ...state,
    highlighted: ids,
    clear: false,
  })),
  on(AnnotationActions.focusAnnotation, (state, { id }): State => ({
    ...state,
    focus: id,
    clear: false,
  })),
  on(AnnotationActions.setCentroids, (state, { centroids }): State => ({
    ...state,
    centroids,
    clear: false,
  })),
  on(AnnotationActions.setAnnotationIds, (state, { annotationIds }): State => ({
    ...state,
    addAnnotationIds: annotationIds,
    clear: false,
  })),
  on(AnnotationActions.discardAnnotations, (state, { annotationIds }): State =>
    annotationAdapter.removeMany(annotationIds, {
      ...state,
      discardedIds: annotationIds,
      clear: false,
    })
  ),
  on(AnnotationActions.hideAnnotations, (state, { hiddenIds }): State => ({
    ...state,
    hiddenIds,
    clear: false,
  }))
);

// a reducer creator wrapper function - Only needed if using View Engine AOT
export function reducer(state: State | undefined, action: Action) {
  return annotationReducer(state, action);
}


export const {
  selectIds,
  selectAll,
  selectTotal,
} = annotationAdapter.getSelectors();
