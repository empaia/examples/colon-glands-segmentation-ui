import * as AnnotationsSelectors from './annotation.selectors';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';
import { State } from './annotation.reducer';
import { AnnotationEntity } from '../../slide-viewer.module';

describe('Annotation Selectors', () => {
  const state: State = {
    addAnnotationIds: [],
    highlighted:  [],
    focus: '',
    centroids: [],
    discardedIds: [],
    hiddenIds: [],
    clear: false,
    entityCache: undefined,
    addedEntityIds: [],
    cacheSizeSoftConstraint: 10,
  };

  let actions$: Observable<never>;
  let store: MockStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideMockActions(() => actions$),
        provideMockStore(),
      ],
    });

    store = TestBed.inject(MockStore);
  });

  it('should select the feature state', (done) => {
    store.overrideSelector(AnnotationsSelectors.selectAnnotationFeatureState, state);
    store.select(AnnotationsSelectors.selectAnnotationFeatureState).subscribe((selectedState) => {
      expect(selectedState).toEqual(state);
      done();
    });
  });

  it('should select missing cached annotations', (done) => {
    const annotations =  ['id1', 'id2', 'id3'];
    store.overrideSelector(AnnotationsSelectors.selectAnnotationFeatureState, state);
    store.overrideSelector(AnnotationsSelectors.selectAddedAnnotationIds, annotations);
    store.select(AnnotationsSelectors.selectMissingCachedAnnotations).subscribe((selectedState) => {
      expect(selectedState).toEqual({
        cachedAnnotations: [],
        missingAnnotations: annotations,
      });
      done();
    });
  });

  it('should select missing cached annotations exceeding cache size', (done) => {
    const annotations =  ['id1', 'id2', 'id3', 'id4', 'id5', 'id6'];
    state.entityCache = new Map<string, AnnotationEntity>();
    annotations.slice(0, 3).forEach(id => state.entityCache.set(id, undefined));
    store.overrideSelector(AnnotationsSelectors.selectAnnotationFeatureState, state);
    store.overrideSelector(AnnotationsSelectors.selectAddedAnnotationIds, annotations);
    store.select(AnnotationsSelectors.selectMissingCachedAnnotations).subscribe((selectedState) => {
      expect(selectedState).toEqual({
        cachedAnnotations: annotations.slice(0, 3),
        missingAnnotations: annotations.slice(3, annotations.length),
      });
      done();
    });
  });
});
