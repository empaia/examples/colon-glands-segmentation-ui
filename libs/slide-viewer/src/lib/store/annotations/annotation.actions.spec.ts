import { addAnnotation } from './annotation.actions';

describe('loadAnnotations', () => {
  it('should return an action', () => {
    expect(addAnnotation({ annotation: undefined }).type).toBe(
      '[SVM Annotation] Add Annotation'
    );
  });
});
