import { createAction, props } from '@ngrx/store';

import { AnnotationEntity } from '../../models/annotation';

export const setAnnotationIds = createAction(
  '[SVM Annotation] Set Annotation Ids',
  props<{ annotationIds: string[] }>()
);

export const setAnnotationIdsReady = createAction(
  '[SVM Annotation] Set Annotation Ids Ready',
);

export const addAnnotation = createAction(
  '[SVM Annotation] Add Annotation',
  props<{ annotation: AnnotationEntity }>()
);

export const setAnnotations = createAction(
  '[SVM Annotation] Set Annotations',
  props<{ annotations: AnnotationEntity[] }>()
);

export const clearAnnotations = createAction(
  '[SVM Annotation] Clear Annotations'
);

export const highlightAnnotations = createAction(
  '[SVM Annotation] Highlight Annotations',
  props<{ ids: string[] }>()
);

export const focusAnnotation = createAction(
  '[SVM Annotation] Focus Annotation',
  props<{ id: string }>()
);

export const setCentroids = createAction(
  '[SVM Annotation] Set Annotation Centroids',
  props<{ centroids: number[][] }>()
);

export const discardAnnotations = createAction(
  '[SVM Annotation] Discard Annotations',
  props<{ annotationIds: string[] }>()
);

export const hideAnnotations = createAction(
  '[SVM Annotation] Hide Annotations',
  props<{ hiddenIds: string[] }>()
);
