import { createSelector } from '@ngrx/store';
import { selectModuleFeatureState, ModuleState } from '../module.state';
import { DrawType, MouseInteractionType } from '../../models/interaction';

export const selectUiFeatureState = createSelector(
  selectModuleFeatureState,
  (state: ModuleState) => state.svmUiState
);

export const selectUiTool = createSelector(
  selectUiFeatureState,
  (state) => state?.interactionMode
);

export const selectUiDrawTool = createSelector(
  selectUiTool,
  // (tool) => Object.values(DrawType).includes(tool as DrawType) ? tool : undefined
  // tool => Object.values(DrawType).includes(tool.left as DrawType) ? tool : undefined
  tool => {
    const mouseInteraction: MouseInteractionType = {
      left: Object.values(DrawType).includes(tool.left as DrawType) ? tool.left : undefined,
      right: Object.values(DrawType).includes(tool.right as DrawType) ? tool.right : undefined,
    };
    return mouseInteraction;
  }
);

/* export const selectIsMeasuring = pipe(
  select(selectUiTool),
  map((tool) => (tool === InteractionType.Measure))
); */

export const selectZoomView = createSelector(
  selectUiFeatureState,
  (state) => state?.zoomView
);

export const selectUiConfig = createSelector(
  selectUiFeatureState,
  (state) => state.config
);

export const selectAnnotationBarVisibility = createSelector(
  selectUiFeatureState,
  (state) => state.config?.showAnnotationBar
);

export const selectAutoAnnotationTitle = createSelector(
  selectUiFeatureState,
  (state) => state.config?.autoSetAnnotationTitle
);

export const selectRenderHideNavigationButton = createSelector(
  selectUiFeatureState,
  (state) => state.config?.renderHideNavigationButton
);

export const selectUpdateViewer = createSelector(
  selectUiFeatureState,
  (state) => state?.updateViewer
);

export const selectAllowedInteractionTypes = createSelector(
  selectUiFeatureState,
  (state) => state.allowedTypes
);

export const selectExaminationState = createSelector(
  selectUiFeatureState,
  (state) => state.examinationClosed
);

export const selectAnnotationHighlightConfig = createSelector(
  selectUiFeatureState,
  (state) => state.annotationHighlightConfig
);

export const selectXaiAlpha = createSelector(
  selectUiFeatureState,
  (state) => state.xaiAlpha
);

export const selectShowXaiLayerState = createSelector(
  selectUiFeatureState,
  (state) => state.showXaiLayer
);