import { setAnnotationBarVisibility } from './ui.actions';

describe('UiAction', () => {
  describe('set AnnotationToolBar', () => {
    it('should return the action Set AnnotationBar Visibility with true', () => {
      const action = setAnnotationBarVisibility({ showAnnotationBar: true });
      expect(action).toMatchObject({
        showAnnotationBar: true,
        type: '[SVM Ui] Set AnnotationBar Visibility',
      });
    });

    it('should return the action Set AnnotationBar Visibility with false', () => {
      const action = setAnnotationBarVisibility({ showAnnotationBar: false });
      expect(action).toMatchObject({
        showAnnotationBar: false,
        type: '[SVM Ui] Set AnnotationBar Visibility',
      });
    });
  });
});
