import { Action, createReducer, on } from '@ngrx/store';
import {
  InteractionType,
  MouseInteractionType,
  ToolbarInteractionType,
} from '../../models/interaction';
import { ViewObserver, ZoomView } from '../../models/ui';
import { UiConfig } from '../../models/viewer-config';
import * as UiActions from './ui.actions';
import { AnnotationHighlightConfig, DEFAULT_ANNOTATION_HIGHLIGHT_CONFIG } from '../../config/annotation-highlight.config';

export interface State {
  config: UiConfig;
  interactionMode: MouseInteractionType;
  zoomView: ZoomView;
  updateViewer: number;
  allowedTypes: ToolbarInteractionType[] | undefined;
  examinationClosed: boolean | undefined;
  annotationHighlightConfig: AnnotationHighlightConfig;
  xaiAlpha: number;
  showXaiLayer: boolean;
}

export const initialState: State = {
  config: {
    showAnnotationBar: true,
    autoSetAnnotationTitle: false,
    renderHideNavigationButton: false,
  },
  interactionMode: {
    left: InteractionType.Move,
    right: InteractionType.Move,
  },
  zoomView: {
    extent: [0, 0, 0, 0],
    resolution: 0,
    sender: ViewObserver.MainMap,
    receivers: [ViewObserver.None],
  },
  updateViewer: undefined,
  allowedTypes: undefined,
  examinationClosed: undefined,
  annotationHighlightConfig: DEFAULT_ANNOTATION_HIGHLIGHT_CONFIG,
  xaiAlpha: 0.7,
  showXaiLayer: false,
};

const uiReducer = createReducer(
  initialState,

  on(UiActions.setUiToolMode, (state, action): State => ({
    ...state,
    interactionMode: action.toolMode,
  })),
  on(UiActions.zoomviewChanged, (state, { zoomview }): State => ({
    ...state,
    zoomView: zoomview,
  })),

  on(UiActions.setUIConfig, (state, { uiConfig }): State => ({
    ...state,
    config: uiConfig
  })),

  on(UiActions.setAnnotationBarVisibility, (state, { showAnnotationBar }): State => ({
    ...state,
    config: {
      ...state.config,
      showAnnotationBar: showAnnotationBar,
    },
  })),

  on(UiActions.setAutoAnnotationTitle, (state, { autoSetAnnotationTitle }): State => ({
    ...state,
    config: {
      ...state.config,
      autoSetAnnotationTitle
    },
  })),
  on(UiActions.setRenderHideNavigationButton, (state, { renderHideNavigationButton }): State => ({
    ...state,
    config: {
      ...state.config,
      renderHideNavigationButton,
    },
  })),
  on(UiActions.setUpdateViewer, (state, { update }): State => ({
    ...state,
    updateViewer: update,
  })),
  on(UiActions.setAllowedInteractionTypes, (state, { allowedTypes }): State => ({
    ...state,
    allowedTypes
  })),
  on(UiActions.setExaminationState, (state, { examinationClosed }): State => ({
    ...state,
    examinationClosed
  })),
  on(UiActions.setAnnotationHighlightConfig, (state, { annotationHighlightConfig }): State => ({
    ...state,
    annotationHighlightConfig,
  })),
  on(UiActions.setXaiAlpha, (state, { xaiAlpha }): State => ({
    ...state,
    xaiAlpha,
  })),
  on(UiActions.setShowXaiLayer, (state, { showXaiLayer }): State => ({
    ...state,
    showXaiLayer,
  })),
);

// a reducer creator wrapper function - Only needed if using View Engine AOT
export function reducer(state: State | undefined, action: Action) {
  return uiReducer(state, action);
}
