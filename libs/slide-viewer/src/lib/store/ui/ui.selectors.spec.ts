import { selectUiFeatureState, selectAnnotationBarVisibility } from './ui.selectors';
import { initialState } from './ui.reducer';

describe('Ui Selectors', () => {
  it('should select the feature state', () => {
    const result = selectUiFeatureState.projector(initialState);

    expect(result).toEqual(undefined);
  });

  describe('AnnotationBar Selector', () => {
    it('should select the annotationBa to be true', () => {
      const result = selectAnnotationBarVisibility.projector(initialState);
      expect(result).toBe(true);
    });
  });
});
