import { createSelector } from '@ngrx/store';
import {
  selectModuleFeatureState,
  ModuleState,
} from '../module.state';

export const selectClassesFeatureState = createSelector(
  selectModuleFeatureState,
  (state: ModuleState) => state.svmClasses
);

export const selectHoveredClasses = createSelector(
  selectClassesFeatureState,
  (state) => state.classes
);
