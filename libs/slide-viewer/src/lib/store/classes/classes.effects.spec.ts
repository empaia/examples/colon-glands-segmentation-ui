import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ClassesEffects } from './classes.effects';
import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { provideMockStore } from '@ngrx/store/testing';

describe('ClassesEffects', () => {
  let actions$: Observable<unknown>;
  let spectator: SpectatorService<ClassesEffects>;
  const createService = createServiceFactory({
    service: ClassesEffects,
    providers: [
      provideMockActions(() => actions$),
      provideMockStore()
    ]
  });

  beforeEach(() => spectator = createService());

  it('should be created', () => {
    const effects = spectator.service;
    expect(effects).toBeTruthy();
  });
});
