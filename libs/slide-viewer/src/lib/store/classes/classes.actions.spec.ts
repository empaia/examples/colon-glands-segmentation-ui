import * as fromClasses from './classes.actions';

describe('loadClassess', () => {
  it('should return an action', () => {
    expect(fromClasses.setHoveredClasses({
      classesEntities: []
    }).type).toBe('[SVM Classes] Set Hovered Classes');
  });
});
