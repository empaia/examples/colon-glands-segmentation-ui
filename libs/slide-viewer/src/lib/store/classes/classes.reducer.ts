import { Action, createReducer, on } from '@ngrx/store';
import * as ClassesActions from './classes.actions';
import { Class } from '../../models/class';


export interface State {
  classes?: Class[] | undefined;
}

export const initialState: State = {
  classes: undefined,
};

const classesReducer = createReducer(
  initialState,
  on(ClassesActions.sendHoveredClassesReady, (state, { classes }): State => ({
    ...state,
    classes,
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return classesReducer(state, action);
}
