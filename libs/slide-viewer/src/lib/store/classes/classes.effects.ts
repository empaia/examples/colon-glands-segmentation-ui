import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as ClassesActions from './classes.actions';
import * as AnnotationsActions from '../annotations/annotation.actions';
import * as AnnotationsSelectors from '../annotations/annotation.selectors';
import { filter, map } from 'rxjs/operators';



@Injectable()
export class ClassesEffects {

  // highlight the annotations of the hovered classes
  setHoveredClasses$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.setHoveredClasses),
      map(action => action.classesEntities),
      concatLatestFrom(() =>
        this.store.select(AnnotationsSelectors.selectAnnotations)
      ),
      filter(([, annotations]) => !!annotations && !!annotations.length),
      map(([classes, annotations]) =>
        classes.map(c => annotations
          .filter(a => a.classes && a.classes.map(ac => ac.value).includes(c.value))
          .concat(annotations.filter(a => (!a.classes || a.classes?.length <= 0) && !c.value))
        )
      ),
      map(annotations => annotations.reduce((acc, val) => acc.concat((val), []))),
      map(annotations => annotations.map(a => a.id)),
      map(ids => AnnotationsActions.highlightAnnotations({ ids }))
    );
  });

  sendClassValues$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.sendHoveredClasses),
      map(action => action.annotationIds),
      filter(annotationIds => !!annotationIds && !!annotationIds.length),
      concatLatestFrom(() =>
        this.store.select(AnnotationsSelectors.selectAnnotationCache)
      ),
      map(([annotationIds, cache]) => annotationIds.map(id => cache.get(id)).map(a => a.classes)),
      map(classes => classes.reduce((acc, val) => acc.concat((val), []))),
      map(classes => ClassesActions.sendHoveredClassesReady({ classes }))
    );
  });

  // set hovered classes to undefined if the hovered annotation id list contain no entries
  clearClassValues$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ClassesActions.sendHoveredClasses),
      map(action => action.annotationIds),
      filter(annotationIds => !annotationIds.length),
      map(() => ClassesActions.sendHoveredClassesReady({ classes: undefined }))
    );
  });

  constructor(
    private readonly actions$: Actions,
    private readonly store: Store,
  ) {}

}
