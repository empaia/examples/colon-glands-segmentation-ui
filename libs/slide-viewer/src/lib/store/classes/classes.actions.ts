import { createAction, props } from '@ngrx/store';
import { ClassNamespace } from '../../models/class-namespace';
import { Class } from '../../models/class';

export const setHoveredClasses = createAction(
  '[SVM Classes] Set Hovered Classes',
  props<{ classesEntities: ClassNamespace[] }>()
);

export const sendHoveredClasses = createAction(
  '[SVM Classes] Send Class Values',
  props<{ annotationIds: string[] | undefined }>()
);

export const sendHoveredClassesReady = createAction(
  '[SVM Classes] Send Class Values Ready',
  props<{ classes: Class[] | undefined }>()
);
