import * as ClassesActions from './classes.actions';
import * as ClassesFeature from './classes.reducer';
import * as ClassesSelectors from './classes.selectors';
export * from './classes.effects';

export { ClassesActions, ClassesFeature, ClassesSelectors };
