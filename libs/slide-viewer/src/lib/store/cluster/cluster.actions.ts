import { createAction, props } from '@ngrx/store';

export const setClusterDistance = createAction(
  '[SVM Cluster] Set Cluster Distance',
  props<{ clusterDistance: number }>()
);
