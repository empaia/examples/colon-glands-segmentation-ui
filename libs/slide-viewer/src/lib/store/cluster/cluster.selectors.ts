import { createSelector } from '@ngrx/store';
import { selectModuleFeatureState, ModuleState } from '../module.state';

export const selectClusterFeatureState = createSelector(
  selectModuleFeatureState,
  (state: ModuleState) => state.svmCluster
);

export const selectClusterDistance = createSelector(
  selectClusterFeatureState,
  (state) => state.clusterDistance
);

