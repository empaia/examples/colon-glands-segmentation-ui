enum HighlightType {
  CLICK,
  OVER,
}

interface AnnotationHighlightConfig {
  highlightType: HighlightType;
}

const DEFAULT_ANNOTATION_HIGHLIGHT_CONFIG: AnnotationHighlightConfig = {
  highlightType: HighlightType.OVER,
};

export {
  HighlightType,
  AnnotationHighlightConfig,
  DEFAULT_ANNOTATION_HIGHLIGHT_CONFIG,
};
