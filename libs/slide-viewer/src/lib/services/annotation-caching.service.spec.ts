import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { AnnotationCachingService } from './annotation-caching.service';
import { Annotation, AnnotationReferenceType } from '../models/annotation';
import { AnnotationType } from '../models/annotation-type';

describe('AnnotationCachingService', () => {
  let spectator: SpectatorService<AnnotationCachingService>;
  const createService = createServiceFactory({
    service: AnnotationCachingService,
  });

  let annotationMock: Annotation;

  beforeEach(() => {
    spectator = createService();

    annotationMock = {
      id: 'dummy',
      name: 'dummy',
      createdAt: new Date('2021-11-04T11:52:34.000Z'),
      updatedAt: new Date('2021-11-04T11:52:34.000Z'),
      description: 'dummy',
      creatorId: 'dummy',
      referenceId: 'dummy',
      referenceType: 'wsi' as AnnotationReferenceType,
      nppCreated: 0,
      annotationType: 'rectangle' as AnnotationType,
    };
  });

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });

  it('cache is empty and uncached annotations are requested', () => {
    expect(
      spectator.service.getUncachedAnnotationIds(['test-id1', 'test-id2'])
    ).toStrictEqual(['test-id1', 'test-id2']);
  });

  it('annotation is in cache and uncached annotations are requested', () => {
    const annotation = { ...annotationMock, id: 'test-id1' };
    spectator.service.addAnnotations([annotation]);
    expect(
      spectator.service.getUncachedAnnotationIds(['test-id1', 'test-id2'])
    ).toStrictEqual(['test-id2']);
  });

  it('annotation is in cache and requested', () => {
    const annotation = { ...annotationMock, id: 'test-id1' };
    spectator.service.addAnnotations([annotation]);
    expect(
      spectator.service.getAnnotations(['test-id1'])
    ).toStrictEqual([annotation]);
  });

  it('annotations are in cache and cache size is filling up, reaching its bounds', () => {
    spectator.service.cacheSize = 10;

    const annotations = [];
    for (let i = 0; i <= 5; i ++) {
      annotations.push({ ...annotationMock, id: `test-id${i}` });
    }
    spectator.service.addAnnotations(annotations);
    
    const annotIds = ['test-id6', 'test-id7', 'test-id8', 'test-id9', 'test-id10', 'test-id11'];

    expect(spectator.service.getUncachedAnnotationIds(annotIds)).toStrictEqual(annotIds);
    expect(
      spectator.service.getAnnotations(['test-id2', 'test-id3', 'test-id4', 'test-id5'])
    ).toStrictEqual([annotations[2], annotations[3], annotations[4], annotations[5]]);
  });

  it('provoked a cache miss', () => {
    spectator.service.cacheSize = 10;

    const annotations = [];
    for (let i = 0; i <= 5; i ++) {
      annotations.push({ ...annotationMock, id: `test-id${i}` });
    }
    spectator.service.addAnnotations(annotations);
    
    const annotIds = ['test-id6', 'test-id7', 'test-id8', 'test-id9', 'test-id10', 'test-id11'];

    expect(spectator.service.getUncachedAnnotationIds(annotIds)).toStrictEqual(annotIds);
    expect(
      spectator.service.getAnnotations(['test-id1', 'test-id2', 'test-id3', 'test-id4'])
    ).toBeUndefined();
  });


  it('caching more annotations than soft constraint allows', () => {
    spectator.service.cacheSize = 10;

    const annotations = [];
    const annotationIds = [];
    for (let i = 0; i <= 20; i ++) {
      annotations.push({ ...annotationMock, id: `test-id${i}` });
      annotationIds.push(`test-id${i}`);
    }
    
    spectator.service.addAnnotations(annotations);
    expect(
      spectator.service.getAnnotations(annotationIds).length
    ).toBe(21);
  });

  it('clear cache', () => {
    const annotations = [];
    const annotationIds = [];
    for (let i = 0; i <= 5; i ++) {
      annotations.push({ ...annotationMock, id: `test-id${i}` });
      annotationIds.push(`test-id${i}`);
    }

    spectator.service.addAnnotations(annotations);
    expect(
      spectator.service.getAnnotations(annotationIds).length
    ).toBe(6);

    spectator.service.clearCache();
    // cache is empty, so we get a cache miss when requesting annotations
    expect(
      spectator.service.getAnnotations(annotationIds)
    ).toBeUndefined();
  });

  it('delete annotation from cache by id', () => {
    const annotations = [];
    const annotationIds = [];
    for (let i = 0; i <= 5; i ++) {
      annotations.push({ ...annotationMock, id: `test-id${i}` });
      annotationIds.push(`test-id${i}`);
    }

    spectator.service.addAnnotations(annotations);
    expect(
      spectator.service.getAnnotations(annotationIds).length
    ).toBe(6);

    spectator.service.removeAnnotations(['test-id1']);
    // annotation was removed, so we get a cache miss
    expect(
      spectator.service.getAnnotations(['test-id1'])
    ).toBeUndefined();
    
    expect(
      spectator.service.getAnnotations(['test-id0', 'test-id2', 'test-id3', 'test-id4', 'test-id5'])
    ).toStrictEqual([annotations[0], annotations[2], annotations[3], annotations[4], annotations[5]]);
  });
});