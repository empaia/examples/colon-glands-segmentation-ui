import { Injectable } from '@angular/core';
import { Annotation } from '../models/annotation';

@Injectable({
  providedIn: 'root'
})
export class AnnotationCachingService {

  private _cacheSizeSoftConstraint = 50000;

  private annotationCache: Map<string, Annotation>;

  constructor() { 
    this.annotationCache = new Map<string, Annotation>();
  }

  public set cacheSize(size: number) {
    this._cacheSizeSoftConstraint = size;
  }

  public get cacheSize(): number {
    return this._cacheSizeSoftConstraint;
  }

  public getUncachedAnnotationIds(annotationIds: string[]): string[] {
    // there is nothing cached yet
    if (!this.annotationCache || this.annotationCache.size < 1) {
      return annotationIds;
    }

    // we check out cache for uncached annotations
    const uncachedAnnotations = [];
    annotationIds.forEach((id: string) => {
      if (!this.annotationCache.has(id)) {
        uncachedAnnotations.push(id);
      }
    });

    // we need to make place for the newly requested annotations, so we clean up the cache if needed
    if (this.annotationCache.size + uncachedAnnotations.length > this._cacheSizeSoftConstraint) {
      const requestedCacheSpace = this.annotationCache.size + uncachedAnnotations.length - this._cacheSizeSoftConstraint;
      Array.from(this.annotationCache.keys())
        .splice(0, requestedCacheSpace)
        .forEach(key => this.annotationCache.delete(key));
    }

    return uncachedAnnotations;
  }

  public getAnnotations(annotationIds: string[]): Annotation[] | undefined {
    const cachedAnnotations = [];
    for (const id of annotationIds) {
      const annotation = this.annotationCache.get(id);
      if (annotation) {
        cachedAnnotations.push(annotation);
      } else {
        // cache miss
        return undefined;
      }
    }
    return cachedAnnotations;
  }

  public addAnnotations(annotations: Annotation[]): string[] {
    const addedAnnotations = [];
    for (const annotation of annotations) {
      const id = annotation.id;
      if (this.annotationCache.has(id)) {
        this.annotationCache.delete(id);
      }
      this.annotationCache.set(id, annotation);
      addedAnnotations.push(id);
    }
    return addedAnnotations;
  }

  public clearCache(): void {
    this.annotationCache.clear();
  }

  public removeAnnotations(annotationIds: string[]): void {
    for (const id of annotationIds) {
      this.annotationCache.delete(id);
    }
  }
}
