import { Injectable } from '@angular/core';
import {
  Circle as CircleStyle,
  Fill,
  Stroke,
  Style,
  Text
} from 'ol/style';

@Injectable({
  providedIn: 'root'
})
export class ClusterStyleService {
  private styleCache: Style[] = [];

  public getClusterStyle(size: number): Style | undefined {
    return this.styleCache[size];
  }

  public setClusterStyle(size: number): void {
    this.styleCache[size] = this.createCluster(size);
  }

  private createCluster(size: number): Style {
    return new Style({
      image: new CircleStyle({
        radius: 15,
        stroke: new Stroke({
          color: '#57d09a',
          width: 2,
        }),
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
      }),
      text: new Text({
        text: size.toString(),
        fill: new Fill({
          color: 'rgba(0, 0, 0, 1.0)',
        }),
      }),
    });
  }
}
