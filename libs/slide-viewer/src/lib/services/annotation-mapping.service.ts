import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnnotationMappingService {
  private annotationIdCache: Set<string> = new Set<string>();

  getAll(): Set<string> {
    return this.annotationIdCache;
  }

  add(id: string): void {
    this.annotationIdCache.delete(id);
    this.annotationIdCache.add(id);
  }

  addMany(ids: string[]): void {
    for (const id of ids) {
      this.add(id);
    }
  }

  discard(count: number): string[] {
    const discard = Array.from(this.annotationIdCache).splice(0, count);
    for (const id of discard) {
      this.annotationIdCache.delete(id);
    }
    return discard;
  }

  clear(): void {
    this.annotationIdCache.clear();
  }
}
