import { TestBed } from '@angular/core/testing';

import { AnnotationMappingService } from './annotation-mapping.service';

describe('AnnotationMappingService', () => {
  let service: AnnotationMappingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnnotationMappingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
