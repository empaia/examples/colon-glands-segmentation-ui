import { TestBed } from '@angular/core/testing';
import { Circle, Point, Polygon } from 'ol/geom';

import { ViewerCalculationsService } from './viewer-calculations.service';

describe('SlideCalculationsService', () => {
  let service: ViewerCalculationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewerCalculationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('calculate centroid for point (inverted)', () => {
    const point: Point = new Point([10, 20]);
    expect(service.centroidForGeometry(point)).toStrictEqual([10, -20]);
  });

  it('calculate centroid for circle (inverted)', () => {
    const circle: Circle = new Circle([10, 20], 10);
    expect(service.centroidForGeometry(circle)).toStrictEqual([10, -20]);
  });

  it('calculate centroid for triangle', () => {
    const triangle: number[][] = [[5, 3], [9, 6], [10, 6]];
    expect(service.centroidForCoordinates(triangle)).toStrictEqual([8, 5]);
  });

  it('calculate centroid for rectangle', () => {
    const rectangle: number[][] = [[4, 2], [10, 8], [8, 6], [6, 10]];
    expect(service.centroidForCoordinates(rectangle)).toStrictEqual([6, 6]);
  });

  it('calculate centroid for polygon', () => {
    const polygon: number[][] = [
      [4, 2], [12, 6], [2, 5], [7, 11], [9, 13], [6, 10],
    ];
    expect(service.centroidForCoordinates(polygon)).toStrictEqual([10, 2]);
  });

  it('calculate centroid for polygon float', () => {
    const polygon: number[][] = [
      [4.928, 3.285], [6.394, 5.292], [2.394, 7.944], [7.482, 2.049], [8.293, 8.280],
    ];
    expect(service.centroidForCoordinates(polygon)).toStrictEqual(
      [6, 5]
    );
  });

  it('check self intersecting polygon', () => {
    const selfIntersectionPolygon = new Polygon([[
      [10, 7], [11, 6], [16, 8], [16, 6], [10, 7],
    ]]);
    expect(service.polygonIsSelfIntersecting(selfIntersectionPolygon)).toBe(true);
  });

  it('check non self intersecting polygon', () => {
    const selfIntersectionPolygon = new Polygon([[
      [1.6, 4.5], [1.5, 2.3], [4.2, 0.8], [7.2, 0.5], 
      [7.8, 1.9], [7.7, 4.2], [7.0, 6.3], [3.7, 6.0],
    ]]);
    expect(service.polygonIsSelfIntersecting(selfIntersectionPolygon)).toBe(false);
  });

  it('check lines intersecting', () => {
    const point1 = [1, 6], point2 = [6, 1], point3 = [1, 1], point4 = [6, 6];
    expect(service.checkLineIntersection(point1, point2, point3, point4)).toBe(true);
  });

  it('check lines not intersecting', () => {
    const point1 = [1, 1], point2 = [1, 6], point3 = [1, 6], point4 = [6, 6];
    expect(service.checkLineIntersection(point1, point2, point3, point4)).toBe(false);
  });

});
