import { EventEmitter, Injectable } from '@angular/core';
import { Fill, Stroke, Style } from 'ol/style';
import { Circle as CircleStyle } from 'ol/style';
import {
  AnnotationPalette,
  ColorName,
  ColorSequenceRecord,
  FeatureStyle,
  FeatureStyleEntry,
  FeatureStylesList
} from '../models/feature-style';
import { AnnotationClass } from "../models/annotation-class";


class AnnotationPalettesCreator {
  private static colorSequences: ColorSequenceRecord = {
    blue: ['#2971AF', '#94B8D7', '#699cc7', '#1F5E9F', '#0F4286'],
    turquoise: ['#0696BB', '#83CBDD', '#51B6CF', '#0483AC', '#026896'],
    dark_green: ['#008E5A', '#80C7AD', '#4DB08C', '#007B48', '#005F2E'],
    light_green: ['#8DBB25', '#C6DD92', '#AFCF66', '#7AAC1B', '#5D960D'],
    red: ['#E22321', '#F19190', '#EB6564', '#E64442', '#CF0C0B'],
    magenta: ['#C5037D', '#FF9BC3', '#FF68A4', '#FF4E95', '#A3014A'],
    dark_orange: ['#E96220', '#F09163', '#E96220', '#E35018', '#D9340B'],
    orange: ['#F28E1C', '#F6B060', '#F28E1C', '#EE7B14', '#E85F09'],
    yellow: ['#FDC60A', '#FED754', '#FDC60A', '#FCB907', '#FCA403']
  };

  public static create(): AnnotationPalette[] {
    return [
      // this.createAnnotationPalette('blue', 'yellow', 'dark_green', 'light_green', 'turquoise'),  # OLD
      this.createAnnotationPalette('blue', 'yellow', 'dark_green', 'red', 'turquoise', 'light_green'), // # NEW
      this.createAnnotationPalette('magenta', 'yellow', 'red', 'light_green', 'dark_green', 'light_green'),
      this.createAnnotationPalette('magenta', 'yellow', 'red', 'dark_orange', 'orange', 'light_green')
    ];
  }

  private static createAnnotationPalette(hover: ColorName, roi: ColorName, drawing: ColorName, annotation1: ColorName, annotation2: ColorName, annotation3: ColorName): AnnotationPalette {
    const annotationClasses = [];
    for (let i = 0; i < this.colorSequences[annotation1].length; i++) {
      annotationClasses.push(this.colorSequences[annotation1][i]);
      annotationClasses.push(this.colorSequences[annotation2][i]);
      annotationClasses.push(this.colorSequences[annotation3][i]);

    }
    return {
      annotationClasses,
      drawing: this.colorSequences[drawing][0],
      hover: this.colorSequences[hover][0],
      roi: this.colorSequences[roi][0]
    };
  }
}

@Injectable({
  providedIn: 'root'
})
export class FeatureStyleService {
  private annotationPaletteIndex = 0;
  private annotationPalettes = AnnotationPalettesCreator.create();
  private featureStyle: FeatureStyle;
  private transparentFill = new Fill({ color: `rgba(255, 255, 255, 0)` });
  private strokeWidth = 1;
  private circleRadius = 5;
  private annotationClassNames: string[] = [];
  private roiName: string | undefined = undefined;

  public annotationStrokeWidthChange = new EventEmitter<never>();
  public annotationPaletteChange = new EventEmitter<never>();
  public annotationClassColorsMappingChange = new EventEmitter<{ [key: string]: string }>();

  constructor() {
    this.featureStyle = this.createFeatureStyle(this.annotationPaletteIndex);
  }

  public getAnnotationPalettes = () => this.annotationPalettes;
  public getAnnotationPalette = (index: number) => this.annotationPalettes[index];
  public getAnnotationPaletteIndex = () => this.annotationPaletteIndex;

  public getAnnotationClassColorIndex(annotationClassName: string): number {
    return this.annotationClassNames.indexOf(annotationClassName);
  }

  public addAnnotationClassToColorMapping(annotationClassName: string): number {
    const lastPossibleIndex = this.getCurrentAnnotationPalette().annotationClasses.length - 1;
    let index = this.annotationClassNames.length;
    if (index <= lastPossibleIndex) {
      this.annotationClassNames.push(annotationClassName);
      this.annotationClassColorsMappingChange.emit(this.getAnnotationClassColors());
    } else {
      // all colors are used up, return only the last color index:
      index = lastPossibleIndex;
    }
    return index;
  }

  public updateAnnotationClassToColorMapping(annotationClasses: AnnotationClass[]) {
    this.annotationClassNames = annotationClasses.map((ac) => ac.id).filter(id => id !== null && !id.endsWith('.roi'));
    // add null id in case annotations are available that have no classes
    this.annotationClassNames = this.annotationClassNames.concat(annotationClasses.map(ac => ac.id).find(id => id === null));
    this.roiName = annotationClasses.map(ac => ac.id).find(id => id !== null && id.endsWith('.roi'));
    this.annotationClassColorsMappingChange.emit(this.getAnnotationClassColors());
  }

  private getAnnotationClassColors(): { [key: string]: string } {
    const m = {};
    const ap = this.getCurrentAnnotationPalette();
    for (let i = 0; i < this.annotationClassNames.length; i++) {
      const color = (i < ap.annotationClasses.length)
        ? ap.annotationClasses[i]
        : ap.annotationClasses[ap.annotationClasses.length - 1];
      m[this.annotationClassNames[i]] = color;
    }
    if (this.roiName) {
      m[this.roiName] = ap.roi;
    }
    return m;
  }

  public setAnnotationPaletteIndex(annotationPaletteIndex: number) {
    if (this.annotationPaletteIndex != annotationPaletteIndex) {
      this.annotationPaletteIndex = annotationPaletteIndex;
      this.updateFeatureStyle();
      this.annotationPaletteChange.emit();
      this.annotationClassColorsMappingChange.emit(this.getAnnotationClassColors());
    }
  }

  private createStyle(color: string): Style {
    return new Style({
      fill: this.transparentFill,
      stroke: new Stroke({ color, width: this.strokeWidth }),
    });
  }

  private getCircleStrokeWidth(): number {
    return Math.max(2, this.strokeWidth);
  }

  private createCircleStyle(color: string): Style {
    return new Style({
      image: new CircleStyle({
        fill: this.transparentFill,
        radius: this.circleRadius,
        stroke: new Stroke({ color, width: this.getCircleStrokeWidth() }),
      }),
    });
  }

  private createStyleEntry(color: string): FeatureStyleEntry {
    return [this.createStyle(color), this.createCircleStyle(color)];
  }

  private createAnnotationClassesStyleEntries(annotationClassesColors: string[]): FeatureStylesList {
    return annotationClassesColors.map(color => this.createStyleEntry(color));
  }

  private createFeatureStyle(annotationPaletteIndex: number): FeatureStyle {
    const ap = this.annotationPalettes[annotationPaletteIndex];
    return {
      DEFAULT: this.createStyleEntry(ap.roi),
      DRAWING: this.createStyleEntry(ap.drawing),
      HOVER: this.createStyleEntry(ap.hover),
      ANNOTATION_CLASSES: this.createAnnotationClassesStyleEntries(ap.annotationClasses)
    };
  }

  public getFeatureStyle(): FeatureStyle {
    return this.featureStyle;
  }

  private updateStyleEntry(styleEntry: FeatureStyleEntry, color: string) {
    const style = styleEntry[0];
    style.getStroke().setWidth(this.strokeWidth);
    style.getStroke().setColor(color);
    const circleStyle = styleEntry[1];
    const imageStyle = (circleStyle.getImage() as CircleStyle);
    const imageStroke = imageStyle.getStroke();
    imageStroke.setColor(color);
    imageStroke.setWidth(this.getCircleStrokeWidth());
    // Clone the image and assign it again, because changing the stroke
    // does not cause an update otherwise.
    circleStyle.setImage(imageStyle.clone());
  }

  private getCurrentAnnotationPalette(): AnnotationPalette {
    return this.annotationPalettes[this.annotationPaletteIndex];
  }

  private updateFeatureStyle(): void {
    const ap = this.getCurrentAnnotationPalette();
    this.updateStyleEntry(this.featureStyle.DEFAULT as FeatureStyleEntry, ap.roi);
    this.updateStyleEntry(this.featureStyle.DRAWING as FeatureStyleEntry, ap.drawing);
    this.updateStyleEntry(this.featureStyle.HOVER as FeatureStyleEntry, ap.hover);
    let classIndex = 0;
    (this.featureStyle.ANNOTATION_CLASSES as FeatureStylesList).forEach(
      styleEntry => {
        this.updateStyleEntry(styleEntry, ap.annotationClasses[classIndex]);
        classIndex++;
      }
    );
  }

  public setStrokeWidth(width: number): void {
    if (this.strokeWidth !== width) {
      this.strokeWidth = width;
      this.updateFeatureStyle();
      this.annotationStrokeWidthChange.emit();
    }
  }

  public getStrokeWidth(): number {
    return this.strokeWidth;
  }
}
