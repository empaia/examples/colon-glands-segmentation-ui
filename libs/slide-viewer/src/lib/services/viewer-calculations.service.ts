import { Injectable } from '@angular/core';

import {
  PROJECTION_M_TO_NM
} from '../models/types';
import { Collection, View } from 'ol';
import { Circle, Geometry, Point, Polygon } from 'ol/geom';
import BaseLayer from 'ol/layer/Base';
import { Extent } from 'ol/extent';
import TileLayer from 'ol/layer/Tile';
import { Coordinate } from 'ol/coordinate';
import { AxisConverter } from '../tools/axis-converter';

@Injectable({
  providedIn: 'root'
})
export class ViewerCalculationsService {

  private calculateNppBase(view: View): number {
    return view.getProjection().getMetersPerUnit() * PROJECTION_M_TO_NM;
  }

  nppForView(view: View): number {
    const nppBase = this.calculateNppBase(view);
    const npp = nppBase * view.getResolution();
    return npp;
  }

  nppExtrema(view: View): number[] {
    const nppBase = this.calculateNppBase(view);
    const nppMin = view.getMinResolution() * nppBase;
    const nppMax = view.getMaxResolution() * nppBase;
    return [nppMin, nppMax];
  }

  getImageExtentFromSourceLayers(layerArray: Collection<BaseLayer>): Extent {
    const tileLayer = layerArray.getArray().find(elem => elem instanceof TileLayer);
    return tileLayer.getProperties()?.source.getTileGrid().getExtent();
  }

  isGeometryInExtent(geometry: Geometry, extent: Extent) {
    const featureExtent = geometry.getExtent();
    return featureExtent[0] > extent[0] && featureExtent[1] > extent[1]
      && featureExtent[2] < extent[2] && featureExtent[3] < extent[3];
  }

  centroidForGeometry(geometry: Geometry) : Coordinate {
    let result: Coordinate;
    if (geometry instanceof Circle) {
      result = AxisConverter.invertYAxis((geometry as Circle).getCenter().map(p => Math.round(p)));
    } else if (geometry instanceof Point) {
      result = AxisConverter.invertYAxis((geometry as Point).getCoordinates().map(p => Math.round(p)));
    } else if (geometry instanceof Polygon) {
      const invertedCoordinates = AxisConverter.invertYAxisOfArray(geometry.getCoordinates()[0]);
      result = this.centroidForCoordinates(invertedCoordinates);
    } else {
      throw new Error('Invalid geometry object.');
    }
    return result;
  }

  centroidForCoordinates(coordinates: number[][]): number[] {
    const centroid = [0, 0];
    let area = 0, x0 = 0, y0 = 0, x1 = 0, y1 = 0, tempArea = 0;

    for (let i = 0; i < coordinates.length - 1; ++i) {
      x0 = coordinates[i][0];
      y0 = coordinates[i][1];
      x1 = coordinates[i + 1][0];
      y1 = coordinates[i + 1][1];
      tempArea = x0 * y1 - x1 * y0;
      area += tempArea;
      centroid[0] = centroid[0] + (x0 + x1) * tempArea;
      centroid[1] = centroid[1] + (y0 + y1) * tempArea;
    }

    // we calculate the last partial area outside the loop
    // to avoid expensive modulo divisions
    x0 = coordinates[coordinates.length - 1][0];
    y0 = coordinates[coordinates.length - 1][1];
    x1 = coordinates[0][0];
    y1 = coordinates[0][1];
    tempArea = x0 * y1 - x1 * y0;
    area += tempArea;
    centroid[0] = centroid[0] + (x0 + x1) * tempArea;
    centroid[1] = centroid[1] + (y0 + y1) * tempArea;

    area = area * 0.5;
    centroid[0] = Math.round(centroid[0] / (6 * area));
    centroid[1] = Math.round(centroid[1] / (6 * area));

    return centroid;
  }

  polygonIsSelfIntersecting(polygon: Polygon): boolean {
    const coordinates = polygon.getCoordinates()[0];
    const partialSections = [];
    for (let i = 0; i < coordinates.length - 1; i++) {
      partialSections.push(coordinates[i], coordinates[i+1]);
    }

    for (let i = 0; i < partialSections.length - 1; i++) {
      for (let j = 1; j < partialSections.length - 2; j++) {
        const p1 = partialSections[i];
        const p2 = partialSections[i + 1];
        const p3 = partialSections[j];
        const p4 = partialSections[j + 1];
        if(this.checkLineIntersection(p1, p2, p3, p4)) {
          return true;
        }
      }
    }
    return false;
  }

  checkLineIntersection(p1c: Coordinate, p2c: Coordinate, p3c: Coordinate, p4c: Coordinate) : boolean {
    // see https://stackoverflow.com/questions/13999249/uibezierpath-intersect/14301701#14301701
    let denominator = (p4c[1] - p3c[1]) * (p2c[0] - p1c[0]) - (p4c[0] - p3c[0]) * (p2c[1] - p1c[1]);
    let ua = (p4c[0] - p3c[0]) * (p1c[1] - p3c[1]) - (p4c[1] - p3c[1]) * (p1c[0] - p3c[0]);
    let ub = (p2c[0] - p1c[0]) * (p1c[1] - p3c[1]) - (p2c[1] - p1c[1]) * (p1c[0] - p3c[0]);
    if (denominator < 0) {
      ua = -ua;
      ub = -ub;
      denominator = -denominator;
    }
    return (ua > 0.0 && ua <= denominator && ub > 0.0 && ub <= denominator);
  }
}
