export interface Token {
  value: string;
  type: 'token';
}

export interface Request {
  type: 'tokenRequest';
}

export interface TokenReady {
  type: 'tokenReady';
}

export const TOKEN_TYPE = 'token';
export const TOKEN_REQUEST_TYPE = 'tokenRequest';
export const TOKEN_READY_TYPE = 'tokenReady';
