export interface WbsUrl {
  url: string;
  type: 'wbsUrl';
}

export interface WbsUrlReady {
  type: 'wbsUrlReady';
}

export const WBS_URL_TYPE = 'wbsUrl';
export const WBS_URL_READY_TYPE = 'wbsUrlReady';
