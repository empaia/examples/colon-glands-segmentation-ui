export interface Scope {
  id: string;
  type: 'scope';
}

export interface ScopeReady {
  type: 'scopeReady';
}

export const SCOPE_TYPE = 'scope';
export const SCOPE_READY_TYPE = 'scopeReady';
