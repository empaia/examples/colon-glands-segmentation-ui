/* eslint-disable @typescript-eslint/no-explicit-any */
import { Scope, SCOPE_TYPE } from '../models/scope';
import { Listener } from './listener';

export class ScopeListener extends Listener<Scope> {
  constructor() {
    super();
  }

  protected isTypeOf(data: any): data is Scope {
    return 'type' in data && data['type'] === SCOPE_TYPE;
  }
}

export const scopeListener = new ScopeListener();
