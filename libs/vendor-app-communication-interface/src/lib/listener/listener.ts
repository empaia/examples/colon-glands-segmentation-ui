/* eslint-disable @typescript-eslint/no-explicit-any */
import { EventEmitter } from '../event-emitter/event-emitter';

export abstract class Listener<T> {
  private eventEmitter = new EventEmitter<T>();
  private _origin!: string;
  public set origin(val: string) {
    this._origin= val;
  }

  public get origin() {
    return this._origin;
  }

  constructor() {
    this.addListener();
  }

  private addListener(): void {
    window.addEventListener('message', this.receiveData.bind(this), false);
  }

  private receiveData(message: MessageEvent): void {
    if (this.isTypeOf(message.data)) {
      this.origin = message.origin;
      this.eventEmitter.emit(message.data);
    }
  }

  protected abstract isTypeOf(data: any): data is T;

  public setCallback(callback: (data: T) => void): number {
    return this.eventEmitter.on(callback);
  }

  public getCallbackCount(): number {
    return this.eventEmitter.getCallbackCount();
  }

  public removeCallbackByIndex(index: number): boolean {
    return this.eventEmitter.removeListenerOnIndex(index);
  }
}
