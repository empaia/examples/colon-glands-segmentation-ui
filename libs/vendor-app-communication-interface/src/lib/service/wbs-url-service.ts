import { WBS_URL_READY_TYPE, WbsUrl, WbsUrlReady } from '../models/wbs-url';
import { wbsUrlListener } from '../listener/wbs-url-listener';

function addWbsUrlListener(callback: (data: WbsUrl) => void): number {
  const index = wbsUrlListener.setCallback(callback);

  if (wbsUrlListener.getCallbackCount() <= 1) {
    sendWbsUrl();
  }

  return index;
}

function sendWbsUrl(): void {
  if (window.top) {
    const origin = wbsUrlListener.origin ? wbsUrlListener.origin : '*';
    const request: WbsUrlReady = {
      type: WBS_URL_READY_TYPE
    };
    // use '*' because the origin is not known yet
    // this function is just called once
    window.top.postMessage(request, origin);
  } else {
    console.error('App is not embedded');
  }
}

function removeWbsUrlListener(index: number): boolean {
  return wbsUrlListener.removeCallbackByIndex(index);
}

export {
  addWbsUrlListener,
  removeWbsUrlListener,
};
