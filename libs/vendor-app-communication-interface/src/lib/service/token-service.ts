import { Token, Request, TOKEN_REQUEST_TYPE, TokenReady, TOKEN_READY_TYPE } from '../models/token';
import { tokenListener } from '../listener/token-listener';

function addTokenListener(callback: (data: Token) => void): number {
  const index = tokenListener.setCallback(callback);

  if (tokenListener.getCallbackCount() <= 1) {
    sendToken();
  }

  return index;
}

function requestNewToken(): void {
  if (window.top) {
    const origin = tokenListener.origin;
    const request: Request = {
      type: TOKEN_REQUEST_TYPE
    };
    window.top.postMessage(request, origin);
  } else {
    console.error('App is not embedded');
  }
}

function sendToken(): void {
  if (window.top) {
    const origin = tokenListener.origin ? tokenListener.origin : '*';
    const requst: TokenReady = {
      type: TOKEN_READY_TYPE
    };
    // use '*' because the origin is not known yet
    // this function is just called once
    window.top.postMessage(requst, origin);
  } else {
    console.error('App is not embedded');
  }
}

function removeTokenListener(index: number): boolean {
  return tokenListener.removeCallbackByIndex(index);
}

export {
  addTokenListener,
  requestNewToken,
  removeTokenListener,
};
