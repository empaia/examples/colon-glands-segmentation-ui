# Colon Glands Segmentation UI App

This is a simple UI app for [colon glands segmentation app](https://gitlab.com/empaia/examples/colon-glands-segmentation-app), which serves as it's frontend.

## Requirements

- [Docker](https://docs.docker.com/install/) :whale:

## How to run

1. Clone this repository

```
git clone https://gitlab.com/empaia/examples/colon-glands-segmentation-ui-app.git
```

2. Run the script file `script.sh` to build the docker image and run the app over the port `3010`

```
./script.sh
```