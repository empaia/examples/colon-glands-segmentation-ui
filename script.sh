#! /bin/bash
docker build -t my-app-ui -f ./tools/docker/Dockerfile .
echo "Running docker container on port 3010..."
docker run -p 3010:80 -it my-app-ui
