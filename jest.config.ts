const { getJestProjects } = require('@nrwl/jest');

export default {
  projects: [
    ...getJestProjects(),
    '<rootDir>/apps/slide-viewer-demo/',
    '<rootDir>/apps/organization-landing-page/',
    '<rootDir>/apps/sample-app/',
    '<rootDir>/apps/workbench-client/',
    '<rootDir>/apps/workbench-client-v2/',
    '<rootDir>/libs/app-integration/',
    '<rootDir>/libs/slide-viewer/',
  ],
};
